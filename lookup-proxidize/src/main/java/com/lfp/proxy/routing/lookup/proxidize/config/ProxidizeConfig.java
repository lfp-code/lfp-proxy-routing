package com.lfp.proxy.routing.lookup.proxidize.config;

import java.net.URI;
import java.time.Duration;
import java.util.List;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.core.properties.converter.URIConverter;
import com.lfp.joe.location.Location;
import com.lfp.joe.properties.converter.DurationConverter;
import com.lfp.joe.properties.converter.JsonConverter;
import com.lfp.proxy.routing.lookup.proxidize.rotation.IPRotationService;

public interface ProxidizeConfig extends Config {

	@ConverterClass(URIConverter.class)
	URI uri();

	String apiToken();

	String proxyUsername();

	String proxyPassword();

	@ConverterClass(JsonConverter.class)
	Location location();

	@DefaultValue("2 min")
	@ConverterClass(DurationConverter.class)
	Duration rotateSuccessRand1();

	@DefaultValue("3 min")
	@ConverterClass(DurationConverter.class)
	Duration rotateSuccessRand2();

	@DefaultValue("2 min")
	@ConverterClass(DurationConverter.class)
	Duration rotateFailureRand1();

	@DefaultValue("3 min")
	@ConverterClass(DurationConverter.class)
	Duration rotateFailureRand2();

	@DefaultValue("4 min")
	@ConverterClass(DurationConverter.class)
	Duration apiRotationServiceTimeout();

	@DefaultValue("4 min")
	@ConverterClass(DurationConverter.class)
	Duration modemRotationServiceTimeout();

	List<String> disabledCarriers();

	// @DefaultValue("com.lfp.proxy.routing.lookup.proxidize.rotation.NoOpIPRotationService")
	@DefaultValue("com.lfp.proxy.routing.lookup.proxidize.rotation.ModemIPRotationService")
	// @DefaultValue("com.lfp.proxy.routing.lookup.proxidize.rotation.APIIPRotationService")
	Class<? extends IPRotationService> ipRotationServiceType();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.properties());
		Configs.printProperties(PrintOptions.jsonBuilder().withIncludeValues(true).build());
	}
}
