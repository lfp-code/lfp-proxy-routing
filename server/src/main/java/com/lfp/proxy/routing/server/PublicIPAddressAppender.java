package com.lfp.proxy.routing.server;

import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.data.redisson.tools.cache.RedissonWriterLoader;
import com.lfp.data.tendis.client.TendisClients;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.proxy.routing.service.ProxyExt;

public class PublicIPAddressAppender implements BiFunction<Proxy, String, ProxyExt> {

	public static final PublicIPAddressAppender INSTANCE = new PublicIPAddressAppender();

	private static final Duration STORAGE_TIME_TO_LIVE = Duration.ofMinutes(1);
	private static final long MEM_CACHE_SIZE = 25_000;
	private static final Duration MEM_CACHE_EXPAW = null;
	private static final Duration MEM_CACHE_EXPAA = Duration.ofSeconds(10);
	private static final int VERSION = 2;
	private final LoadingCache<Request, Optional<String>> cache;

	private PublicIPAddressAppender() {
		var writerLoader = new RedissonWriterLoader<Request, Optional<String>>(TendisClients.getDefault(),
				this.getClass(), VERSION) {

			@Override
			protected @Nullable Iterable<Object> getKeyParts(@NonNull Request key) {
				return List.of(key.id);
			}

			@Override
			protected @Nullable ThrowingFunction<ThrowingSupplier<Optional<String>, Exception>, Optional<String>, Exception> getStorageLockAccessor(
					@NonNull Request key, String lockKey) {
				return createStorageLockAccessorDefault(key, lockKey);
			}

			@Override
			protected @Nullable Optional<String> loadFresh(@NonNull Request key) throws Exception {
				var ipAddress = Ok.Clients.getIPAddress(Ok.Clients.get(key.proxy));
				if (!IPs.isValidIpAddress(ipAddress))
					return Optional.empty();
				return Optional.of(ipAddress);
			}

			@Override
			protected @Nullable Duration getStorageTTL(@NonNull Request key, @NonNull Optional<String> result) {
				return STORAGE_TIME_TO_LIVE;
			}
		};
		this.cache = writerLoader
				.buildCache(Caches.newCaffeineBuilder(MEM_CACHE_SIZE, MEM_CACHE_EXPAW, MEM_CACHE_EXPAA));
	}

	public ProxyExt apply(Proxy proxy) {
		return apply(proxy, null);
	}

	@Override
	public ProxyExt apply(Proxy proxy, String uuid) {
		if (proxy == null)
			return null;
		ProxyExt proxyExt;
		if (proxy instanceof ProxyExt)
			proxyExt = (ProxyExt) proxy;
		else
			proxyExt = ProxyExt.build(proxy, null);
		if (proxyExt.getPublicIPAddress().isPresent())
			return proxyExt;
		if (Utils.Strings.isBlank(uuid))
			uuid = proxy.uuid();
		var publicIPAddressOp = this.cache.get(new Request(uuid, proxyExt));
		return proxyExt.toBuilder().publicIPAddress(publicIPAddressOp.orElse(null)).build();
	}

	private static class Request {

		private final String id;
		public final Proxy proxy;

		public Request(String id, Proxy proxy) {
			super();
			this.id = id;
			this.proxy = proxy;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Request other = (Request) obj;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			return true;
		}

	}

}