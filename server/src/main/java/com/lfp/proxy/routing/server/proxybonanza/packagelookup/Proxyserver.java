
package com.lfp.proxy.routing.server.proxybonanza.packagelookup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Proxyserver {

    @SerializedName("georegion_id")
    @Expose
    private Long georegionId;
    @SerializedName("georegion")
    @Expose
    private Georegion georegion;

    /**
     * 
     * @return
     *     The georegionId
     */
    public Long getGeoregionId() {
        return georegionId;
    }

    /**
     * 
     * @param georegionId
     *     The georegion_id
     */
    public void setGeoregionId(Long georegionId) {
        this.georegionId = georegionId;
    }

    /**
     * 
     * @return
     *     The georegion
     */
    public Georegion getGeoregion() {
        return georegion;
    }

    /**
     * 
     * @param georegion
     *     The georegion
     */
    public void setGeoregion(Georegion georegion) {
        this.georegion = georegion;
    }

}
