package com.lfp.proxy.routing.lookup.proxidize.rotation;

import java.io.IOException;

import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.function.Throws.ThrowingRunnable;

public interface IPRotationService extends ThrowingRunnable<IOException>, Scrapable {

}
