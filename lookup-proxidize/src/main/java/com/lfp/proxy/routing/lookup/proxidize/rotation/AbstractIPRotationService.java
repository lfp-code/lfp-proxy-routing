package com.lfp.proxy.routing.lookup.proxidize.rotation;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;

import com.github.throwable.beanref.BeanProperty;
import com.github.throwable.beanref.BeanRef;
import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.tools.concurrent.KeepAliveSemaphore;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.log.Logging;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.threads.ImmutableFutures.ScheduleWhileRequest;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.threads.executor.SubmitterSchedulerLFP;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.lookup.proxidize.api.GetInfoResult;
import com.lfp.proxy.routing.lookup.proxidize.api.GetInfoResultFunction;
import com.lfp.proxy.routing.lookup.proxidize.config.ProxidizeConfig;

import ch.qos.logback.classic.Level;
import okhttp3.OkHttpClient;
import one.util.streamex.EntryStream;

public abstract class AbstractIPRotationService extends Scrapable.Impl implements IPRotationService {

	private static final Duration IP_MODIFY_CHECK_DELAY = Duration.ofSeconds(5);
	private static final Duration IP_MODIFY_CHECK_POLL = Duration.ofSeconds(1);

	private final Logger logger = Logging.logger();
	private final Map<String, ListenableFuture<Nada>> futureMap = new ConcurrentHashMap<>();
	private final SubmitterSchedulerLFP pool = Threads.Pools.centralPool()
			.limit(Math.max(8, Utils.Machine.logicalProcessorCount() * 2));
	private final Duration rotationTimeout;
	private final String serviceId;
	private ListenableFuture<Nada> _pollFuture;

	protected AbstractIPRotationService(Duration rotationTimeout, String serviceId, boolean disablePollStart) {
		this.rotationTimeout = Objects.requireNonNull(rotationTimeout);
		this.serviceId = Utils.Strings.trimToNullOptional(serviceId)
				.orElseGet(() -> IPRotationService.class.getSimpleName());
		if (!disablePollStart)
			this.startPolling();
	}

	public ListenableFuture<Nada> startPolling() {
		if (_pollFuture != null)
			return _pollFuture;
		synchronized (this) {
			if (_pollFuture == null) {
				var future = ScheduleWhileRequest.builder(Duration.ofSeconds(5), () -> {
					try {
						run();
					} catch (Throwable t) {
						if (!Utils.Exceptions.isCancelException(t))
							org.slf4j.LoggerFactory.getLogger(this.getClass()).warn("poll error", t);
					}
					return Nada.get();
				}, nil -> {
					return !this.isScrapped();
				}).initialDelay(Duration.ofSeconds(5)).build().schedule();
				this.onScrap(() -> future.cancel(true));
				future.listener(this::scrap);
				this._pollFuture = future;
			}
		}
		return this._pollFuture;
	}

	@Override
	public void run() throws IOException {
		processRotations();
	}

	protected void processRotations() throws IOException {
		for (var proxyInfo : GetInfoResultFunction.INSTANCE.get()) {
			if (proxyInfo == null)
				continue;
			var httpProxy = proxyInfo.streamProxies(Proxy.Type.HTTP).findFirst().orElse(null);
			if (httpProxy == null)
				continue;
			processRotation(proxyInfo);
		}
	}

	protected void processRotation(GetInfoResult proxyInfo) {
		processRotation(proxyInfo, false);
	}

	protected void processRotation(GetInfoResult proxyInfo, boolean force) {
		var futureKey = proxyInfo.getImei();
		if (futureKey == null) {
			// logger.warn("imei not found:{}", proxyInfo);
			return;
		}
		var rotateFutureCreated = new AtomicBoolean();
		var rotateFuture = futureMap.computeIfAbsent(futureKey, nil -> {
			var future = scheduleRotation(proxyInfo, force);
			rotateFutureCreated.set(true);
			return future;
		});
		if (!rotateFutureCreated.get())
			return;
		rotateFuture.listener(() -> this.futureMap.remove(futureKey));
	}

	private ListenableFuture<Nada> scheduleRotation(GetInfoResult proxyInfo, boolean force) {
		RotationContext rotationContext = RotationContext.builder().proxyInfo(proxyInfo).keyParts(this.serviceId)
				.build();
		var semaphore = new KeepAliveSemaphore(RedissonClients.getDefault(), 1, rotationContext.getStorageKey("lock"));
		if (MachineConfig.isDeveloper())
			semaphore.setVerboseLogging(true);
		ThrowingSupplier<ListenableFuture<Nada>, ?> onAcquiredFlatMap = () -> {
			var rotationFuture = createRotationFuture(rotationContext, force);
			rotationFuture = Threads.Futures.callbackTimeout(rotationFuture, this.rotationTimeout, f -> {
				f.cancel(true);
			}, Threads.Pools.centralPool());
			return rotationFuture;
		};
		var future = semaphore.acquireAsyncKeepAliveFlatSupply(onAcquiredFlatMap);
		future = future.flatMap(proceed -> {
			onComplete(rotationContext, null);
			return FutureUtils.immediateResultFuture(proceed);
		});
		future = future.flatMapFailure(Throwable.class, t -> {
			onComplete(rotationContext, t);
			return FutureUtils.immediateFailureFuture(t);
		});
		future.listener(() -> {
			Utils.Exceptions.closeQuietly(Level.WARN, rotationContext);
		});
		return future;
	}

	private ListenableFuture<Nada> createRotationFuture(RotationContext rotationContext, boolean force) {
		var rotationFuture = this.pool.submit(() -> rotate(rotationContext, force));
		rotationFuture = rotationFuture.flatMap(proceed -> {
			if (!proceed)
				return FutureUtils.immediateResultFuture(false);
			return this.pool.submitScheduled(() -> true, IP_MODIFY_CHECK_DELAY.toMillis());
		});
		rotationFuture = rotationFuture.flatMap(proceed -> {
			if (!proceed)
				return FutureUtils.immediateResultFuture(false);
			return pollForIpModification(rotationContext);
		});
		return rotationFuture.map(nil -> Nada.get());

	}

	private boolean rotate(RotationContext rotationContext, boolean force) throws IOException {
		var rotateAt = rotationContext.getRotateAtBucket().get();
		if (!force && rotateAt != null && new Date().before(rotateAt))
			return false;
		logger.info("ip rotation requested. {}", getLogDataMessage(rotationContext, null));
		String ipAddressPre = getIPAddress(rotationContext, true, Duration.ofSeconds(15));
		if (!IPs.isValidIpAddress(ipAddressPre))
			ipAddressPre = null;
		rotationContext.setIpAddressPre(ipAddressPre);
		rotationContext.markStarted();
		logger.info("ip rotation started. {}", getLogDataMessage(rotationContext, null));
		this.processRotation(rotationContext.getProxyInfo(), rotationContext.getClient());
		logger.info("ip rotation processed. {}", getLogDataMessage(rotationContext, null));
		return true;
	}

	private ListenableFuture<Boolean> pollForIpModification(RotationContext rotationContext) {
		return ScheduleWhileRequest.builder(IP_MODIFY_CHECK_POLL, () -> {
			var ipAddressPost = Utils.Functions.catching(() -> Ok.Clients.getIPAddress(rotationContext.getClient()),
					t -> null);
			if (!IPs.isValidIpAddress(ipAddressPost))
				return false;
			rotationContext.setIpAddressPost(ipAddressPost);
			return true;
		}, ipAddressPostFound -> {
			if (ipAddressPostFound)
				return false;
			return !Thread.currentThread().isInterrupted();
		}).scheduler(pool).build().schedule();
	}

	private void onComplete(RotationContext rotationContext, Throwable failure) {
		if (!rotationContext.isStarted())
			return;
		String logDataMessage = getLogDataMessage(rotationContext, failure);
		boolean success;
		if (failure != null) {
			logger.warn("ip rotation failed. {}", logDataMessage);
			success = false;
		} else if (rotationContext.getIpAddressPost().isEmpty()) {
			logger.warn("post rotation ip lookup failed {}", logDataMessage);
			success = false;
		} else if (Objects.equals(rotationContext.getIpAddressPre().orElse(null),
				rotationContext.getIpAddressPost().orElse(null))) {
			logger.warn("post rotation ip modification failed. {}", logDataMessage);
			success = false;
		} else {
			logger.info("ip rotation success. {}", logDataMessage);
			success = true;
		}
		updateRotateAtBucket(rotationContext, success);
		return;
	}

	private void updateRotateAtBucket(RotationContext rotationContext, boolean success) {
		Date rotateAt = new Date(System.currentTimeMillis() + getDelay(success).toMillis());
		Duration ttl;
		{
			var ttlMillis = rotateAt.getTime() - System.currentTimeMillis();
			ttlMillis = Math.max(0, ttlMillis);
			ttlMillis = ttlMillis * 2;
			ttl = Duration.ofMillis(ttlMillis);
		}
		rotationContext.getRotateAtBucket().set(rotateAt, ttl.toMillis(), TimeUnit.MILLISECONDS);
		logger.info("ip rotation scheduled for {}. {}", rotateAt, getLogDataMessage(rotationContext, null));
	}

	protected static String getLogDataMessage(RotationContext rotationContext, Throwable failure) {
		return getLogDataStream(rotationContext, failure)
				.map(ent -> String.format("%s:%s", ent.getKey(), ent.getValue())).joining(" ");
	}

	protected static EntryStream<String, String> getLogDataStream(RotationContext rotationContext, Throwable failure) {
		Map<String, Object> logDataMap = new LinkedHashMap<>();
		{// RotationContext props
			List<BeanProperty<RotationContext, Object>> props = new ArrayList<>();
			props.add(BeanRef.$(RotationContext::getIpAddressPre));
			props.add(BeanRef.$(RotationContext::getIpAddressPost));
			props.forEach(prop -> logDataMap.put(prop.getName(), prop.get(rotationContext)));
		}
		{// error message
			List<Entry<Throwable, String>> errorEntries;
			{
				var estream = Utils.Exceptions.streamCauses(failure).mapToEntry(Throwable::getMessage);
				estream = estream.filterValues(v -> Utils.Strings.isNotBlank(v));
				errorEntries = estream.toList();
			}
			if (!errorEntries.isEmpty()) {
				String errorMessage = Utils.Lots.streamEntries(errorEntries)
						.filterKeys(v -> !(v instanceof RuntimeException)).values().findFirst()
						.orElse(errorEntries.get(0).getValue());
				logDataMap.put("errorMessage", errorMessage);
			}
		}
		{// GetInfoResult props
			List<BeanProperty<GetInfoResult, Object>> props = new ArrayList<>();
			props.add(BeanRef.$(GetInfoResult::getIndex));
			props.add(BeanRef.$(GetInfoResult::getImei));
			props.add(BeanRef.$(GetInfoResult::getUsbId));
			props.add(BeanRef.$(GetInfoResult::getCarrier));
			props.forEach(prop -> logDataMap.put(prop.getName(), prop.get(rotationContext.getProxyInfo())));
		}
		var logDataStream = Utils.Lots.stream(logDataMap).nonNullValues().mapValues(v -> {
			if (v instanceof Optional)
				v = ((Optional<?>) v).orElse(null);
			return v == null ? null : Utils.Strings.trimToNull(v.toString());
		});
		logDataStream = logDataStream.filterValues(Utils.Strings::isNotBlank);
		return logDataStream;
	}

	protected static String getIPAddress(RotationContext rotationContext, boolean sourceProxyInfo, Duration timeout)
			throws IOException {
		if (sourceProxyInfo) {
			var ip = rotationContext.getProxyInfo().getPublicIp();
			if (IPs.isValidIpAddress(ip))
				return ip;
		}
		Callable<String> task = () -> {
			var ip = Utils.Functions.catching(() -> Ok.Clients.getIPAddress(rotationContext.getClient()), t -> null);
			if (IPs.isValidIpAddress(ip))
				return ip;
			return null;
		};
		Predicate<String> loopTest = ip -> {
			if (IPs.isValidIpAddress(ip))
				return false;
			return true;
		};
		var future = ScheduleWhileRequest.builder(Duration.ofSeconds(1), task, loopTest).build().schedule();
		try {
			return future.get(timeout.toMillis(), TimeUnit.MILLISECONDS);
		} catch (Exception e) {
			if (e instanceof TimeoutException)
				return null;
			throw Utils.Exceptions.as(e, IOException.class);
		} finally {
			future.cancel(true);
		}
	}

	private static Duration getDelay(boolean success) {
		var cfg = Configs.get(ProxidizeConfig.class);
		var rand1 = success ? cfg.rotateSuccessRand1() : cfg.rotateFailureRand1();
		var rand2 = success ? cfg.rotateSuccessRand2() : cfg.rotateFailureRand2();
		long millis = Utils.Crypto.getRandomInclusive(rand1.toMillis(), rand2.toMillis());
		return Duration.ofMillis(millis);
	}

	protected abstract void processRotation(GetInfoResult proxyInfo, OkHttpClient client) throws IOException;

}
