package com.lfp.proxy.routing.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.lfp.proxy.routing.impl.TunnelService;
import com.lfp.proxy.routing.lookup.LookupService;
import com.lfp.proxy.routing.lookup.proxidize.ProxidizeLookupService;
import com.lfp.proxy.routing.server.iproyal.IPRoyalLookupService;
import com.lfp.proxy.routing.server.localproxies.LocalProxiesLookupService;
import com.lfp.proxy.routing.server.proxybonanza.ProxyBonanzaLookupService;
import com.lfp.proxy.routing.service.RoutingService;
import com.lfp.rsocket.server.RSocketServerContext;
import com.lfp.rsocket.server.RSocketServers;

public class App {

	public static void main(String[] args) throws IOException {
		List<LookupService> lookupServices = new ArrayList<>();
		{
			lookupServices.add(ProxyBonanzaLookupService.INSTANCE);
			//lookupServices.add(SmartProxyLookupService.INSTANCE);
			lookupServices.add(ProxidizeLookupService.INSTANCE);
			lookupServices.add(LocalProxiesLookupService.INSTANCE);
			//lookupServices.add(AirProxyLookupService.INSTANCE);
			//lookupServices.add(IPRoyalLookupService.INSTANCE);
		}
		RoutingAndTunnelServiceImpl handler = new RoutingAndTunnelServiceImpl(lookupServices.toArray(LookupService[]::new));
		RSocketServerContext serverContext = RSocketServers.create();
		RSocketServers.attach(serverContext.getRequestHandlingRSocket(), RoutingService.class, handler);
		RSocketServers.attach(serverContext.getRequestHandlingRSocket(), TunnelService.class, handler);
		serverContext.getServer().onDispose().block();
	}
}
