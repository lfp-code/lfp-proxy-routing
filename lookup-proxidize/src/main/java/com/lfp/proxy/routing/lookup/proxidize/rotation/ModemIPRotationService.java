package com.lfp.proxy.routing.lookup.proxidize.rotation;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonPrimitive;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.lookup.proxidize.api.GetInfoResult;
import com.lfp.proxy.routing.lookup.proxidize.api.GetInfoResultFunction;
import com.lfp.proxy.routing.lookup.proxidize.config.ProxidizeConfig;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class ModemIPRotationService extends AbstractIPRotationService {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final URI MODEM_ENDPOINT = URI.create("http://192.168.8.1/reqproc/proc_post");

	public ModemIPRotationService() {
		this(null, false);
	}

	public ModemIPRotationService(String serviceId, boolean disablePollStart) {
		super(Configs.get(ProxidizeConfig.class).modemRotationServiceTimeout(), serviceId, disablePollStart);
	}

	protected void processRotation(GetInfoResult proxyResult, OkHttpClient client) throws IOException {
		List<Entry<String, JsonElement>> resultTracker = new ArrayList<>();
		{
			String cmd = "LOGIN";
			if (!modemPost(client, "LOGIN", Map.of("password", Utils.Bits.from("admin").encodeBase64()), false,
					resultTracker))
				throw new IOException(cmd + " command failed");
		}
		{
			String cmd = "DISCONNECT_NETWORK";
			if (!modemPost(client, cmd, Map.of("notCallback", true), false, resultTracker))
				throw new IOException(cmd + " command failed");
		}
		modemPost(client, "SET_BEARER_PREFERENCE", Map.of("BearerPreference", "Only_LTE"), false, resultTracker);
		modemPost(client, "SET_BEARER_PREFERENCE", Map.of("BearerPreference", "NETWORK_auto"), false, resultTracker);
		{
			String cmd = "CONNECT_NETWORK";
			if (!modemPost(client, "CONNECT_NETWORK", Map.of("notCallback", true), true, resultTracker))
				throw new IOException(cmd + " command failed");
		}
	}

	@SuppressWarnings("deprecation")
	private static boolean isSuccess(String body) throws IOException {
		var je = Serials.Gsons.getJsonParser().parse(body);
		var result = Serials.Gsons.tryGetAsString(je, "result").orElse(null);
		if ("success".equalsIgnoreCase(result))
			return true;
		var code = Utils.Strings.parseNumber(result).map(Number::intValue).orElse(null);
		if (Objects.equals(0, code))
			return true;
		return false;
	}

	@SuppressWarnings("deprecation")
	private static boolean modemPost(OkHttpClient client, String goFormId, Map<String, Object> data, boolean close,
			List<Entry<String, JsonElement>> resultTracker) {
		var rb = new Request.Builder().url(MODEM_ENDPOINT.toString());
		if (close) {
			rb = rb.addHeader(Headers.CONNECTION, "close");
			rb = rb.addHeader(Headers.PROXY_CONNECTION, "close");
		}
		var fbb = new FormBody.Builder();
		Utils.Lots.stream(data).prepend("goformId", goFormId).prepend("isTest", false).forEach(ent -> {
			var key = Optional.ofNullable(ent.getKey()).orElse("");
			var value = Optional.ofNullable(ent.getValue()).map(Object::toString).orElse("");
			fbb.add(key, value);
		});
		rb = rb.post(fbb.build());
		String body = null;
		boolean success;
		try (var response = client.newCall(rb.build()).execute()) {
			body = response.body().string();
			Ok.Calls.validateStatusCode(response, 2);
			success = isSuccess(body);
		} catch (Exception e) {
			success = false;
			if (Utils.Strings.isBlank(body))
				body = e.getMessage();
		}
		JsonElement bodyJe;
		if (Utils.Strings.isBlank(body))
			bodyJe = JsonNull.INSTANCE;
		else
			try {
				bodyJe = Serials.Gsons.getJsonParser().parse(body);
			} catch (Exception e) {
				bodyJe = new JsonPrimitive(body);
			}
		resultTracker.add(Utils.Lots.entry(goFormId, bodyJe));
		return success;
	}

	public static void main(String[] args) throws InterruptedException, IOException {
		long port = 1001;
		var json = "{\"ID\":1509,\"Index\":1,\"Model\":\"UNKNOWN DEVICE\",\"Imei\":\"352906118180761\",\"Public_ip\":\"None\",\"Phone_number\":\"#\",\"Carrier\":\"UNKNOWN\",\"USB_ID\":\"1-1.3.3.3\",\"Dev_name\":\"eth7\",\"Internal_ip\":\"192.168.8.100\",\"Gateway\":\"192.168.8.1\",\"Proxy_ip\":\"192.168.8.220\",\"Driver\":\"CDC_Ether\",\"TTY\":\"0\",\"Time_added\":\"2021-10-29 14:34:39\",\"rotation_interval\":\"None\",\"Port\":1001,\"HTTP\":\"192.168.1.168:1000\",\"SOCKS\":\"192.168.1.168:1001\",\"Workmode\":\"UNKNOWN\",\"Status\":\"UNKNOWN\"}";
		json = "";
		try (var service = new ModemIPRotationService(null, true);) {
			GetInfoResult getInfoResult;
			if (Utils.Strings.isNotBlank(json)) {
				getInfoResult = Serials.Gsons.get().fromJson(json, GetInfoResult.class);
				getInfoResult.setDisableHostModification(true);
			} else
				getInfoResult = GetInfoResultFunction.INSTANCE.get(true).filter(v -> {
					return Objects.equals(port, v.getPort());
				}).findFirst().get();
			System.out.println(Serials.Gsons.getPretty().toJson(getInfoResult));
			service.processRotation(getInfoResult, true);
		}
		Thread.currentThread().join();
	}
}
