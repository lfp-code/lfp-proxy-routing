package com.lfp.proxy.routing.client.test;

import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.okhttp.interceptor.UserAgentConnectInterceptor;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Executors;
import com.lfp.proxy.routing.client.ProxyRoutingClient;
import com.lfp.proxy.routing.service.ProxyExt;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Dispatcher;
import okhttp3.Interceptor;
import okhttp3.Interceptor.Chain;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class App {

	private static final MemoizedSupplier<X509TrustManager> TRUST_ALL_MANAGER_S = MemoizedSupplier.create(() -> {
		// Create a trust manager that does not validate certificate chains
		return new X509TrustManager() {
			@Override
			public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)
					throws CertificateException {}

			@Override
			public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType)
					throws CertificateException {}

			@Override
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[0];
			}
		};
	});
	private static final MemoizedSupplier<SSLContext> TRUST_ALL_CONTEXT_S = MemoizedSupplier.create(() -> {
		// Install the all-trusting trust manager
		final SSLContext sslContext;
		try {
			sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, new TrustManager[] { TRUST_ALL_MANAGER_S.get() }, new java.security.SecureRandom());
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			throw Throws.unchecked(e);
		}
		return sslContext;
	});

	private static OkHttpClient createClient(com.lfp.joe.net.proxy.Proxy proxy) {
		var cb = new OkHttpClient.Builder();
		interceptUserAgent(cb.interceptors());
		cb = cb.addInterceptor(new UserAgentConnectInterceptor());
		cb = cb.proxySelector(new ProxySelector() {

			@Override
			public List<Proxy> select(URI uri) {
				return List.of(proxy.asJdkProxy());
			}

			@Override
			public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {}

		});
		cb = cb.proxyAuthenticator((route, response) -> {
			if (!proxy.isAuthenticationEnabled())
				return response.request();
			var basicAuth = "Basic " + Base64.getEncoder().encodeToString(
					String.format("%s:%s", proxy.getUsername().orElse(""), proxy.getPassword().orElse(""))
							.getBytes(MachineConfig.getDefaultCharset()));
			return response.request().newBuilder().header("Proxy-Authorization", basicAuth).build();
		});

		cb = cb.sslSocketFactory(TRUST_ALL_CONTEXT_S.get().getSocketFactory(), TRUST_ALL_MANAGER_S.get())
				.hostnameVerifier((hostname, session) -> true);
		cb = cb.dispatcher(new Dispatcher(Executors.asExecutorService(CoreTasks.executor())));
		var client = cb.build();
		return client;
	}

	private static void interceptUserAgent(List<Interceptor> interceptors) {
		Function<Request, Request> requestModifier = request -> {
			return request.newBuilder().header("User-Agent",
					"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36")
					.build();
		};
		if (interceptors.isEmpty()) {
			interceptors.add(chain -> {
				return chain.proceed(requestModifier.apply(chain.request()));
			});
			return;
		}
		for (int i = 0; i < interceptors.size(); i++) {
			var interceptor = interceptors.get(i);
			Interceptor interceptorWrapper = chain -> {
				var request = requestModifier.apply(chain.request());
				InvocationHandler invocationHandler = (proxy, method, args) -> {
					if (method.getParameterCount() == 0 && "request".equals(method.getName()))
						return request;
					return method.invoke(interceptor, args);
				};
				Chain chainWrapper = (Chain) java.lang.reflect.Proxy.newProxyInstance(
						CoreReflections.getDefaultClassLoader(), new Class<?>[] { Chain.class }, invocationHandler);
				return interceptor.intercept(chainWrapper);
			};
			interceptors.set(i, interceptorWrapper);
		}
	}

	public static void main(String[] args) throws Exception {
		Map<String, ProxyExt> proxyMap = new HashMap<>();
		for (int i = 0; i < 1; i++) {
			Set<String> unique = new HashSet<>();
			while (true) {
				ProxyExt proxy = ProxyRoutingClient.get().lookupProxyBlocking();
				if (!unique.add(proxy.getPublicIPAddress().orElse(null)))
					break;
				if (proxyMap.size() % 50 == 0)
					System.out.println("proxies foudn:" + proxyMap.size());
				proxyMap.putIfAbsent(proxy.getPublicIPAddress().orElse(null), proxy);
			}
		}

		System.out.println("starting:" + proxyMap.size());
		var counter = new AtomicInteger();
		var clientList = Streams.of(proxyMap.values()).map(proxy -> {
			var client = createClient(proxy);
			return client;
		}).peek(v -> {
			var count = counter.incrementAndGet();
			if (count % 25 == 0)
				System.out.println(count);
		}).toList();
		System.gc();
		System.out.println(clientList.size());
		var latch = new CountDownLatch(clientList.size());
		var index = -1;
		for (var client : clientList) {
			var clientIndex = index++;
			var request = new Request.Builder().url("https://api.ipify.org").build();
			client.newCall(request).enqueue(new Callback() {

				@Override
				public void onResponse(Call arg0, Response response) throws IOException {
					try (response) {
						System.out.println(clientIndex + " - " + response.code() + " - " + response.body().string());
					} finally {
						latch.countDown();
					}

				}

				@Override
				public void onFailure(Call arg0, IOException e) {
					try {
						System.err.println(clientIndex + " - " + e.getMessage());
					} finally {
						latch.countDown();
					}

				}
			});
		}
		latch.await();
		System.gc();
		System.exit(0);
	}
}
