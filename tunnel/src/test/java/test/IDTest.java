package test;

import java.util.Random;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;

public class IDTest {

	public static void main(String[] args) {
		Random random = new Random();
		String alphabet = "abcdefghijklmnopqrstuvwxyz0123456789";
		String id = NanoIdUtils.randomNanoId(random, alphabet.toCharArray(), 16);
		System.out.println(id);
	}

}
