package test;

import java.util.List;

import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.service.ProxyAttributes;
import com.lfp.proxy.routing.service.RoutingService;
import com.lfp.rsocket.client.RSocketClients;
import com.neovisionaries.i18n.CountryCode;

public class ClientTest2 {

	@SuppressWarnings("unused")
	public static void main(String[] args) throws InterruptedException {
		RoutingService client = RSocketClients.get(RoutingService.class);
		if (false)
			while (true) {
				List<ProxyAttributes> locs = client.availableProxyAttributes().collectList().block();
				System.out.println(Serials.Gsons.get().toJson(locs));
				Thread.sleep(5_000);
				if (false)
					break;
			}
		for (int i = 0; i < 1; i++) {
			new Thread(() -> {
				while (true) {
					var pa = ProxyAttributes.builder();
					pa.countryCode(CountryCode.US);
					Proxy proxy = client.lookupProxy(Proxy.Type.HTTP, pa.build()).block();
					System.out.println(Serials.Gsons.get().toJson(proxy));
					var ipAddress = Ok.Clients.getIPAddress(Ok.Clients.get(proxy));
					System.out.println(ipAddress);
					try {
						Thread.sleep(Utils.Crypto.getRandomInclusive(100, 1000));
					} catch (InterruptedException e) {
						throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
								? java.lang.RuntimeException.class.cast(e)
								: new java.lang.RuntimeException(e);
					}
				}
			}).start();
		}
		while (true)
			Thread.sleep(1000);
	}

}
