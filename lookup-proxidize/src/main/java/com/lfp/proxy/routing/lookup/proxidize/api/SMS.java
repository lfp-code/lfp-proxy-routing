package com.lfp.proxy.routing.lookup.proxidize.api;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class SMS {

	@SerializedName("phone")
	@Expose
	private String phone;
	@SerializedName("content")
	@Expose
	private String content;
	@SerializedName("date")
	@Expose
	private String date;

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}