package com.lfp.proxy.routing.server.iproyal;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.location.Locations;
import com.lfp.joe.net.cookie.CookieStoreLFP;
import com.lfp.joe.net.cookie.Cookies;
import com.lfp.joe.net.html.Jsoups;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.okhttp.cookie.CookieJars;
import com.lfp.joe.okhttp.interceptor.BrowserHeaderInterceptor;
import com.lfp.joe.reactor.Scheduli;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.proxy.routing.lookup.LookupService;
import com.lfp.proxy.routing.lookup.ProxyRegistration;
import com.lfp.proxy.routing.lookup.ProxySupplier;
import com.lfp.proxy.routing.server.PublicIPAddressAppender;
import com.lfp.proxy.routing.service.ProxyAttributes;
import com.lfp.proxy.routing.service.ProxyAttributes.RotationType;
import com.lfp.proxy.routing.service.ProxyAttributes.ServerType;
import com.lfp.proxy.routing.service.ProxyExt;

import org.apache.commons.lang3.Validate;
import org.jsoup.nodes.Document;

import com.google.gson.JsonElement;

import okhttp3.OkHttpClient;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;
import reactor.core.Disposable;
import reactor.core.Disposables;
import reactor.core.publisher.Mono;

public enum IPRoyalLookupService implements LookupService {
	INSTANCE;

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 0;
	private static final Duration RESET_IP_DURATION_1 = MachineConfig.isDeveloper() ? Duration.ofSeconds(10)
			: Duration.ofSeconds(60);
	private static final Duration RESET_IP_DURATION_2 = MachineConfig.isDeveloper() ? Duration.ofSeconds(20)
			: Duration.ofSeconds(90);
	private final OkHttpClient client;
	{
		CookieStoreLFP cookieStore = !MachineConfig.isDeveloper() ? Cookies.createCookieStore()
				: Cookies.createCookieStore(Utils.Files.tempFile(this.getClass(), VERSION));
		Proxy proxy = null;
		this.client = Ok.Clients.get(proxy).newBuilder().cookieJar(CookieJars.create(cookieStore))
				.addInterceptor(BrowserHeaderInterceptor.getBrowserDefault())
				.addInterceptor(new IPRoyalLoginInterceptor()).build();
	}
	private final Map<IPRoyalProxyContext, ResetIPAddressSubscription> resetIPSubscriptions = new ConcurrentHashMap<>();

	@Override
	public StreamEx<ProxyRegistration> lookup() throws IOException {
		Document doc;
		try (var rctx = Ok.Calls.execute(client, "https://dashboard.iproyal.com/products/4g-mobile-proxies")
				.validateSuccess().parseHtml()) {
			doc = rctx.getParsedBody();
		}
		var uriStream = Jsoups.select(doc, "td.text-right a[href]").map(v -> URIs.parse(v.absUrl("href")).orElse(null));
		uriStream = uriStream.nonNull().distinct();
		uriStream = uriStream.filter(v -> URIs.pathPrefixMatch(v.getPath(), "/orders"));
		uriStream = uriStream.filter(v -> {
			var pathSegments = URIs.streamPathSegments(v).limit(3).toList();
			if (pathSegments.size() != 2)
				return false;
			return Utils.Strings.parseNumber(pathSegments.get(1)).isPresent();
		});
		var proxyContextList = uriStream.map(v -> {
			return Utils.Functions.unchecked(() -> getIPRoyalProxyContext(v));
		}).nonNull().toList();
		updateSchedules(proxyContextList);
		return Utils.Lots.stream(proxyContextList).map(v -> toProxyRegistration(v));
	}

	private void updateSchedules(List<IPRoyalProxyContext> proxyContextList) {
		{// remove
			var kstream = Utils.Lots.stream(resetIPSubscriptions).keys();
			kstream.filter(v -> !proxyContextList.contains(v)).toList().forEach(v -> {
				logger.info("removing subscription:{}", v);
				var subscription = resetIPSubscriptions.remove(v);
				if (subscription != null)
					subscription.dispose();
			});
		}
		{// add
			proxyContextList.forEach(v -> {
				resetIPSubscriptions.computeIfAbsent(v, nil -> {
					logger.info("adding subscription:{}", v);
					return createResetIPSubscription(v);
				});
			});
		}
	}

	private ProxyRegistration toProxyRegistration(IPRoyalProxyContext iprCtx) {
		ProxyAttributes proxyAttributes;
		{
			var blder = ProxyAttributes.builder();
			blder.countryCode(iprCtx.getCountryCode());
			blder.serverTypes(ServerType.MOBILE);
			blder.rotationType(RotationType.RANDOM);
			proxyAttributes = blder.build();
		}
		ProxyRegistration proxyRegistration;
		{
			var blder = ProxyRegistration.builder();
			blder.lookupServiceType(this.getClass());
			blder.proxyAttributes(proxyAttributes);
			var proxy = iprCtx.getProxy();
			blder.proxySupplier(new ProxySupplier(proxy.hash()) {

				@Override
				public ProxyExt get() {
					var resetIPSubscription = resetIPSubscriptions.get(iprCtx);
					if (resetIPSubscription == null)
						return PublicIPAddressAppender.INSTANCE.apply(proxy);
					return ProxyExt.build(proxy, resetIPSubscription.getIpAddress());
				}
			});
			proxyRegistration = blder.build();
		}
		return proxyRegistration;
	}

	private ResetIPAddressSubscription createResetIPSubscription(IPRoyalProxyContext iprCtx) {
		var resetIPSubscription = new ResetIPAddressSubscription(() -> getIPAddress(iprCtx));
		var flux = Mono.just(Nada.get()).repeat().delayUntil(nil -> {
			var delayMillis = Utils.Crypto.getRandomInclusive(RESET_IP_DURATION_1.toMillis(),
					RESET_IP_DURATION_2.toMillis());
			return Mono.just(Nada.get()).delayElement(Duration.ofMillis(delayMillis), Scheduli.unlimited());
		});
		var delegate = flux.map(nil -> {
			resetIPSubscription.setIpAddress(() -> {
				return resetIP(iprCtx);
			});
			return Nada.get();
		}).onErrorResume(t -> {
			logger.warn("error during reset ip rest call:" + iprCtx, t);
			return Mono.just(Nada.get());
		}).subscribe();
		resetIPSubscription.setDelegate(delegate);
		return resetIPSubscription;
	}

	private IPRoyalProxyContext getIPRoyalProxyContext(URI uri) throws IOException {
		Document doc;
		try (var rctx = Ok.Calls.execute(client, uri).validateSuccess().parseHtml()) {
			doc = rctx.getParsedBody();
		}
		return getIPRoyalProxyContext(doc);
	}

	private IPRoyalProxyContext getIPRoyalProxyContext(Document doc) {
		BiFunction<String, Boolean, Optional<String>> valueGetter = (filter, contains) -> {
			var stream = streamProperties(doc);
			if (contains)
				stream = stream.filterKeys(v -> Utils.Strings.containsIgnoreCase(v, filter));
			else
				stream = stream.filterKeys(v -> Utils.Strings.equalsIgnoreCase(v, filter));
			return stream.values().findFirst();
		};
		var confirmed = valueGetter.apply("status", false).filter("confirmed"::equalsIgnoreCase).isPresent();
		if (!confirmed)
			return null;
		var resetURI = Jsoups.select(doc, "#reset_key").map(v -> v.attr("value")).map(v -> URIs.parse(v).orElse(null))
				.nonNull().findFirst().orElse(null);
		var apiKey = valueGetter.apply("api", true).get();
		var countryCode = valueGetter.apply("location", false).flatMap(Locations::parseCountryCode).orElse(null);
		var hostname = valueGetter.apply("ip", false).get();
		var port = valueGetter.apply("port", true).flatMap(Utils.Strings::parseNumber).map(Number::intValue).get();
		var username = valueGetter.apply("username", false).get();
		var password = valueGetter.apply("password", false).get();
		var proxy = Proxy.builder().type(Proxy.Type.HTTP).hostname(hostname).port(port).username(username)
				.password(password).build();
		return IPRoyalProxyContext.builder().apiKey(apiKey).resetURI(resetURI).countryCode(countryCode).proxy(proxy)
				.build();
	}

	private EntryStream<String, String> streamProperties(Document doc) {
		EntryStream<String, String> estream = EntryStream.empty();
		{
			var append = Jsoups.select(doc, "table tr").map(v -> {
				var tds = Jsoups.select(v, "td").limit(3).toList();
				if (tds.size() != 2)
					return null;
				return Utils.Lots.entry(tds.get(0).text(), tds.get(1).text());
			}).nonNull();
			estream = estream.append(append);
		}
		{
			var lineStreams = Jsoups.select(doc, "#product_info").map(v -> v.text())
					.map(v -> Utils.Strings.streamLines(v));
			var lineStream = Utils.Lots.flatMap(lineStreams).filter(Utils.Strings::isNotBlank).distinct();
			var append = lineStream.map(v -> {
				var key = Utils.Strings.substringBefore(v, ":");
				var value = Utils.Strings.substringAfter(v, ":");
				return Utils.Lots.entry(key, value);
			});
			estream = estream.append(append);
		}
		estream = estream.mapKeys(Utils.Strings::trimToNull).nonNullKeys();
		estream = estream.mapValues(Utils.Strings::trimToNull).nonNullValues();
		return estream;
	}

	private String resetIP(IPRoyalProxyContext iprCtx) throws IOException {
		if (MachineConfig.isDeveloper())
			logger.info("resetting ip:" + Serials.Gsons.get().toJson(iprCtx));
		var uri = iprCtx.getResetURI();
		JsonElement je;
		try (var rctx = Ok.Calls.execute(client, uri).validateSuccess().parseJson()) {
			je = rctx.getParsedBody();
		}
		var status = Serials.Gsons.tryGetAsString(je, "status").orElse(null);
		Validate.isTrue("success".equalsIgnoreCase(status), "invalid reset ip response:%s", je);
		return getIPAddress(iprCtx);
	}

	private static String getIPAddress(IPRoyalProxyContext iprCtx) {
		var client = Ok.Clients.get(iprCtx.getProxy());
		return Ok.Clients.getIPAddress(client);
	}

	private static class ResetIPAddressSubscription implements Disposable {

		private Disposable delegate = Disposables.disposed();
		private String _ipAddress;

		public ResetIPAddressSubscription(ThrowingSupplier<String, IOException> ipAddressSupplier) {
			this.setIpAddress(ipAddressSupplier);
		}

		public void setDelegate(Disposable disposable) {
			this.delegate = Objects.requireNonNull(disposable);
		}

		public void dispose() {
			delegate.dispose();
		}

		public boolean isDisposed() {
			return delegate.isDisposed();
		}

		public void setIpAddress(ThrowingSupplier<String, IOException> sup) {
			synchronized (this) {
				var ip = Utils.Functions.unchecked(sup::get);
				Validate.isTrue(IPs.isValidIpAddress(ip));
				this._ipAddress = ip;
			}
		}

		public String getIpAddress() {
			synchronized (this) {
				return _ipAddress;
			}
		}
	}

	public static void main(String[] args) throws IOException {
		for (int i = 0;; i++) {
			if (i > 0)
				Threads.sleepUnchecked(Duration.ofSeconds(1));
			var stream = IPRoyalLookupService.INSTANCE.lookup();
			Utils.Lots.stream(stream).forEach(v -> {
				System.out.println(Serials.Gsons.getPretty().toJson(v.getProxy()));
			});
		}
	}

}
