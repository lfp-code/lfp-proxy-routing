package com.lfp.unirest;

import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.headers.UserAgents;
import com.lfp.joe.reactor.Monos;
import com.lfp.proxy.routing.client.ProxyRoutingClient;
import com.lfp.proxy.routing.service.ProxyExt;

import kong.unirest.Unirest;
import one.util.streamex.IntStreamEx;
import reactor.core.publisher.Flux;

public class Unirests {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		var responses = Flux.range(0, 10).flatMap(index -> {
			return ProxyRoutingClient.get().lookupProxy().flatMapMany(proxy -> {
				return Flux.fromStream(process(proxy)).flatMap(Monos::fromFuture);
			});
		});
		var total = responses.count().block();
		System.err.println("done:" + total);

	}

	private static Stream<CompletableFuture<String>> process(ProxyExt proxy) {
		System.out.println(proxy);
		var unirest = Unirest.spawnInstance();
		unirest.config()
				.proxy(proxy.getHostname(), proxy.getPort(), proxy.getUsername().orElse(""),
						proxy.getPassword().orElse(""));
		unirest.config().concurrency(1_000, 100);
		// unirest.config().executor(executor);
		var futures = IntStreamEx.range(50).mapToObj(index -> {
			var request = unirest.get("https://api.ipify.org/");
			var response = request.header(Headers.USER_AGENT, UserAgents.CHROME_LATEST.get())
					.asStringAsync()
					.thenApplyAsync(v -> v.getBody(), CoreTasks.executor());
			response = response.handle((v, t) -> {
				if (t != null) {
					LOGGER.error("error. index:{} proxy:{}", index, proxy);
					return null;
				}
				return v;
			});
			response.whenComplete((v, t) -> {
				if (t == null)
					System.out.println(String.format("%s - %s - %s", index, v, proxy));
			});
			return response;
		}).toList();
		CompletableFuture.allOf(futures.toArray(CompletableFuture[]::new)).whenCompleteAsync((v, t) -> {
			unirest.close();
		}, CoreTasks.executor());
		return futures.stream();
	}

}
