package test;

import java.security.interfaces.RSAPrivateKey;
import java.text.ParseException;
import java.time.Duration;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Base58;
import com.lfp.proxy.routing.impl.config.ProxyRoutingImplConfig;
import com.lfp.proxy.routing.server.config.ProxyRoutingServerConfig;
import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.RSADecrypter;
import com.nimbusds.jose.crypto.RSAEncrypter;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

public class Test {

	public static void main(String[] args) throws JOSEException, ParseException {
		Map<String, Object> data = Map.of("username", "reggiepierce", "password", Utils.Crypto.getSecureRandomString());
		System.out.println("input:" + Serials.Gsons.getPretty().toJson(data));
		RSAKey senderPublicKey = new RSAKey.Builder(Configs.get(ProxyRoutingImplConfig.class).serverPublicKey())
				.build();
		RSAKey senderPrivateKey = new RSAKey.Builder(senderPublicKey)
				.privateKey(Configs.get(ProxyRoutingServerConfig.class).serverPrivateKey()).build();
		RSAKey recipientPublicKey = new RSAKey.Builder(Configs.get(ProxyRoutingImplConfig.class).tunnelPublicKey())
				.build();
		RSAKey recipientPrivateKey = new RSAKey.Builder(recipientPublicKey).privateKey(Serials.Gsons.get().fromJson(
				"{\"p\":\"x6GAqT0v2he-grTUoyXseBRMfPe4I9uu8oOL07hPx1Ppmn20YpnoU0nS3LzDtagVthoL9LurI1pHqCCekYzLKviE7Tl6WsvwFizuBnizb25AkHb_SlPfdJPj_E8R-dQHLlef3VVxy62VLPRyVZtw0aIQPLCZwFkxpjJN5wqJJ9k\",\"kty\":\"RSA\",\"q\":\"pL7YrKZXc4PmUkx9npljwiUpqZ7kcHR65dFY8YFCjC6HvENNwJ-hGr8ltW9ReIFUV7EnFV7fM_ahzV-8xJCdPFB1bPR7RKYRw2umV4kJpp2uYEuGJ0-i8JU0GrDmbBioYLCUNu1xuXTnuB4D6nKxXYufE-7Cq39w8dFwxiYUZxk\",\"d\":\"TpzMfntIewjjwe_3rJi-qD0OE9bOyV46zCiAJdN9BQlrB39QVNMIRfVRlV_kC3-DpS8h5a0ceKvhpv_sI8VW9bufybjU75TcSksoEf2gRNY02n29BEcZf-MLu6g_68auR40h86ncmO1p7kwbimPGOc2ankZEoimb8eBEOKVbl64iMnA0FTHLBmhSWTl138SQBJTkBkDyHqTJVOTGRkOumm7iufcAzOyfHg620wjU6OmhjAS1QPeMvssQWYoKamuBEZO_mlBrgp8a1M8Stx79WzRRMkDSP9QwkPY2gCEau48t5LexBlMveUG1F_WuuSrck495fI-P6Gq4SDDb-fYlwQ\",\"e\":\"AQAB\",\"qi\":\"onijLye1RUtFWCkwkUpBN8hKhCzaQJikbT0k1XxFrjEDd1N7ZMGp-gRvkE6IuSOFvaH2ZfDMMq33UHLFU9ferUz78iyrtfCW82Deg9qz3iGnZW5FKJpaPuF-gyadY1EwynrJaR8qS8IlinBIRRLtvnOzfC374yiKiJMAFycIino\",\"dp\":\"ecOSnKfLljVBaKwvvsuU7rFC7j6SvJx0m0uNcDhNwQD3zdm6uyGxSYwP57_jh9vcUlSU0lPd0RRx_KTY02KTUcSAH8odTWBzMrN5A81_dQpEq1lJ4eZCt_K07uKzzjR7pNKhmflSDa_0-5SmYH0NVXCKgtodDevZksvAJJqSihE\",\"dq\":\"Uyc9CE_hfh3KSCnxkfbMQnctgz7_AL_aNFupcCSVKVpnCqBSA3LPB0gJyX1JevCUWLrDBmdfc8P9wUKcbf6sToAx9O-ltG26M5AHYtF5fI7EMDiOmxL4JES7CunVNCRyAHPAK8VHjJs0RltoEJMZcSUDAzWVLqjhojzbljgsPik\",\"n\":\"gHhJQM-WOfVvEqhLM7QBXA42Okmehi0beI_WHPdeUgtZs4bw51ydBDREA9BjP2r6BedDo5XRF5ovKtmiu_RMFg10F_sJrHnmUeXHVYTF-5XqfenE8Ht4LUaUo6ynTPHmKSuT0QQK3q8roTrZWcfKT8FQ7HPrJCoYCYaPSpBO7tnm5LIOvXmumeJclS67oVA9JIXaDoUxPne2KizuZ861HW0M1nSUonj3haw8T8pJuxJLrHu5dUMn-Ku2Mj5eE_95PHVnKF-99yWQAXlA0OVmv7SFSjKWpfuLgExs3GlNpvA7xUTl75WokPnI-dbOUx_aR58ewRx5RvDSi4FEiWEzMQ\",\"createdAt\":1611674295583}",
				RSAPrivateKey.class)).build();
		String encoded = encode(senderPrivateKey, recipientPublicKey, data);
		System.out.println("result:" + encoded);
		var decoded = decode(senderPublicKey, recipientPrivateKey, encoded);
		System.out.println("output:" + Serials.Gsons.getPretty().toJson(decoded));
	}

	private static String encode(RSAKey senderJWK, RSAKey recipientPublicJWK, Map<String, Object> data)
			throws JOSEException {
		var expiresAt = new Date(System.currentTimeMillis() + Duration.ofDays(1).toMillis());
		// expiresAt = new Date();
		var claimBuilder = new JWTClaimsSet.Builder().expirationTime(expiresAt);
		for (var ent : data.entrySet())
			claimBuilder.claim(ent.getKey(), ent.getValue());
		// Create JWT
		SignedJWT signedJWT = new SignedJWT(
				new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(senderJWK.getKeyID()).build(), claimBuilder.build());
		// Sign the JWT
		signedJWT.sign(new RSASSASigner(senderJWK));
		// Create JWE object with signed JWT as payload
		JWEObject jweObject = new JWEObject(
				new JWEHeader.Builder(JWEAlgorithm.RSA_OAEP_256, EncryptionMethod.A256GCM).contentType("JWT").build(),
				new Payload(signedJWT));
		// Encrypt with the recipient's public key
		jweObject.encrypt(new RSAEncrypter(recipientPublicJWK));
		// Serialise to JWE compact form
		String jweString = jweObject.serialize();
		System.out.println("jweString encrypt:" + jweString);
		var partsIter = Utils.Lots.stream(Utils.Strings.split(jweString, '.')).map(Utils.Bits::parseBase64Url)
				.iterator();
		var result = Utils.Lots.stream(partsIter).map(Base58::encodeBytes).joining("-");
		return result;
	}

	private static Map<String, Object> decode(RSAKey senderPublicJWK, RSAKey recipientJWK, String encoded)
			throws JOSEException, ParseException {
		var jweString = Utils.Lots.stream(Utils.Strings.split(encoded, '-')).map(Base58::decodeToBytes)
				.map(v -> v.encodeBase64(true, false)).joining(".");
		System.out.println("jweString decrypt:" + jweString);
		JWEObject jweObject = JWEObject.parse(jweString);
		// Decrypt with private key
		jweObject.decrypt(new RSADecrypter(recipientJWK));
		// Extract payload
		SignedJWT signedJWT = jweObject.getPayload().toSignedJWT();
		Objects.requireNonNull(signedJWT, "payload not a signed JWT");
		// Check the signature
		Validate.isTrue(signedJWT.verify(new RSASSAVerifier(senderPublicJWK)));
		var expirationTime = signedJWT.getJWTClaimsSet().getExpirationTime();
		Validate.isTrue(expirationTime == null || expirationTime.after(new Date()), "expired JWT");
		return signedJWT.getJWTClaimsSet().getClaims();
	}
}
