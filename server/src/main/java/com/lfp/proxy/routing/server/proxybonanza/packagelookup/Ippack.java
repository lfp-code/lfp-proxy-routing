
package com.lfp.proxy.routing.server.proxybonanza.packagelookup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Ippack {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("ip")
    @Expose
    private String ip;
    @SerializedName("port_http")
    @Expose
    private Long portHttp;
    @SerializedName("port_socks")
    @Expose
    private Long portSocks;
    @SerializedName("active")
    @Expose
    private Boolean active;
    @SerializedName("proxyserver")
    @Expose
    private Proxyserver proxyserver;

    /**
     * 
     * @return
     *     The id
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * 
     * @param ip
     *     The ip
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * 
     * @return
     *     The portHttp
     */
    public Long getPortHttp() {
        return portHttp;
    }

    /**
     * 
     * @param portHttp
     *     The port_http
     */
    public void setPortHttp(Long portHttp) {
        this.portHttp = portHttp;
    }

    /**
     * 
     * @return
     *     The portSocks
     */
    public Long getPortSocks() {
        return portSocks;
    }

    /**
     * 
     * @param portSocks
     *     The port_socks
     */
    public void setPortSocks(Long portSocks) {
        this.portSocks = portSocks;
    }

    /**
     * 
     * @return
     *     The active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * 
     * @param active
     *     The active
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * 
     * @return
     *     The proxyserver
     */
    public Proxyserver getProxyserver() {
        return proxyserver;
    }

    /**
     * 
     * @param proxyserver
     *     The proxyserver
     */
    public void setProxyserver(Proxyserver proxyserver) {
        this.proxyserver = proxyserver;
    }

}
