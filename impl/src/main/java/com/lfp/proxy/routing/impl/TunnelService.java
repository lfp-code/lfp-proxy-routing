package com.lfp.proxy.routing.impl;

import org.reactivestreams.Publisher;

import com.lfp.joe.core.properties.Configs;
import com.lfp.proxy.routing.service.config.ProxyRoutingServiceConfig;

import reactor.core.publisher.Flux;

public interface TunnelService {

	static ProxyRoutingServiceConfig clientConfig() {
		return Configs.get(ProxyRoutingServiceConfig.class);
	}

	Flux<TunnelAddress> register(Publisher<TunnelAddress> addressPublisher);

}
