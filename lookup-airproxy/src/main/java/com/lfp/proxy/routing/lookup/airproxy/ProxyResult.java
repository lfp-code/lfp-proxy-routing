
package com.lfp.proxy.routing.lookup.airproxy;

import java.util.List;

import com.lfp.joe.net.proxy.Proxy;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class ProxyResult {

	@SerializedName("id")
	@Expose
	private Long id;
	@SerializedName("ip")
	@Expose
	private String ip;
	@SerializedName("port")
	@Expose
	private int port;
	@SerializedName("username")
	@Expose
	private String username;
	@SerializedName("password")
	@Expose
	private String password;
	@SerializedName("src_whitelist")
	@Expose
	private List<Object> srcWhitelist = null;
	@SerializedName("isp")
	@Expose
	private String isp;
	@SerializedName("comment")
	@Expose
	private String comment;

	public Proxy toProxy() {
		return Proxy.builder().type(Proxy.Type.HTTP).username(getUsername()).password(getPassword()).hostname(getIp())
				.port(getPort()).build();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Object> getSrcWhitelist() {
		return srcWhitelist;
	}

	public void setSrcWhitelist(List<Object> srcWhitelist) {
		this.srcWhitelist = srcWhitelist;
	}

	public String getIsp() {
		return isp;
	}

	public void setIsp(String isp) {
		this.isp = isp;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
