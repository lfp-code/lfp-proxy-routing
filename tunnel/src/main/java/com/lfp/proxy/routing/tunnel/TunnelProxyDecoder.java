package com.lfp.proxy.routing.tunnel;

import java.io.IOException;
import java.time.Duration;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.proxy.routing.impl.ProxyCodec;
import com.lfp.proxy.routing.impl.ProxyDecoder;
import com.lfp.proxy.routing.tunnel.config.ProxyRoutingTunnelConfig;

public class TunnelProxyDecoder implements ProxyCodec<Iterable<String>, Proxy> {

	public static final TunnelProxyDecoder INSTANCE = new TunnelProxyDecoder();

	private final ProxyCodec<Iterable<String>, Proxy> delegate;

	protected TunnelProxyDecoder() {
		var cfg = Configs.get(ProxyRoutingTunnelConfig.class);
		ProxyCodec<Iterable<String>, Proxy> delegate = new ProxyDecoder(cfg.tunnelPrivateKey());
		delegate = delegate.cached(cfg.decodedProxyCacheMaxiumumSize(), cfg.decodedProxyCacheExpireAfterWrite(),
				cfg.decodedProxyCacheExpireAfterAccess(), cfg.decodedProxyCacheRefreshAfterWrite());
		this.delegate = delegate;
	}

	@Override
	public Proxy apply(Iterable<String> input) throws IOException {
		return delegate.apply(input);
	}

	@Override
	public ProxyCodec<Iterable<String>, Proxy> cached(long maximumSize, Duration expireAfterWrite,
			Duration expireAfterAccess, Duration refreshAfterWrite) {
		return delegate.cached(maximumSize, expireAfterWrite, expireAfterAccess, refreshAfterWrite);
	}

}
