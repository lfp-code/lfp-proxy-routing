package com.lfp.proxy.routing.impl;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.SignatureException;
import java.time.Duration;
import java.util.Date;
import java.util.Map.Entry;
import java.util.Objects;

import com.lfp.joe.core.function.FNV;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Base58;
import com.lfp.proxy.routing.impl.config.ProxyRoutingImplConfig;
import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.RSAEncrypter;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

import one.util.streamex.StreamEx;

public class ProxyEncoder extends ProxyCodec.Abs<Proxy, Entry<String, String>> {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 0;
	private final Duration ttl;

	public ProxyEncoder(Duration ttl, PrivateKey serverKey) {
		this(ttl,
				new RSAKey.Builder(Configs.get(ProxyRoutingImplConfig.class).serverPublicKey())
						.privateKey(Objects.requireNonNull(serverKey))
						.build(),
				new RSAKey.Builder(Configs.get(ProxyRoutingImplConfig.class).tunnelPublicKey()).build());
	}

	public ProxyEncoder(Duration ttl, RSAKey serverKey, RSAKey tunnelPublicKey) {
		super(serverKey, tunnelPublicKey);
		ttl = roundTTL(ttl);
		this.ttl = Objects.requireNonNull(ttl);
	}

	@Override
	public Entry<String, String> applyInternal(Proxy proxy) throws IOException {
		try {
			if (Proxy.Type.SOCKS_5.equals(proxy.getType())) {
				return encodeSocks(proxy);
			} else if (Proxy.Type.HTTP.equals(proxy.getType()))
				return encodeHttp(proxy);
		} catch (InvalidKeyException | NoSuchAlgorithmException | SignatureException | JOSEException e) {
			throw Utils.Exceptions.as(e, IOException.class);
		}
		throw new IllegalArgumentException("unable to handle proxy type:" + proxy.getType());
	}

	@Override
	protected Long getCacheKey(Proxy input) {
		ByteBuffer bb = ByteBuffer.allocate(4);
		bb.putInt(VERSION);
		return FNV.hash64(THIS_CLASS.getName().getBytes(Utils.Bits.getDefaultCharset()), bb.array(),
				input.hash().array());

	}

	protected Entry<String, String> encodeSocks(Proxy proxy)
			throws InvalidKeyException, JOSEException, NoSuchAlgorithmException, SignatureException {
		var cacheKeyBytes = Utils.Crypto.hashMD5(THIS_CLASS, VERSION, proxy);
		var expiresAtBytes = CompactDateCodec.INSTANCE.encode(getExpiresAt());
		var signatureBytes = Utils.Crypto.sign(getSignature(), this.getSigningKey().toPrivateKey(),
				expiresAtBytes.append(cacheKeyBytes));
		var token = StreamEx.of(signatureBytes, expiresAtBytes, cacheKeyBytes).map(Base58::encodeBytes).joining("-");
		var ssdbProxyCacheTTLSeconds = Math.max(1,
				Configs.get(ProxyRoutingImplConfig.class).ssdbProxyCacheTTL().getSeconds());
		var ssdbClient = getSSDBClient();
		var cacheKey = cacheKeyBytes.encodeHex();
		if (!ssdbClient.expire(cacheKey, ssdbProxyCacheTTLSeconds))
			ssdbClient.setex(cacheKeyBytes.encodeHex(), ssdbProxyCacheTTLSeconds, proxy);
		return toCredentials(token);
	}

	protected Entry<String, String> encodeHttp(Proxy proxy) throws IOException, JOSEException {
		var claimBuilder = new JWTClaimsSet.Builder();
		claimBuilder.expirationTime(getExpiresAt());
		for (var mp : Proxy.meta().metaPropertyIterable())
			claimBuilder.claim(mp.name(), mp.get(proxy));
		// Create JWT
		SignedJWT signedJWT = new SignedJWT(
				new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(this.getSigningKey().getKeyID()).build(),
				claimBuilder.build());
		// Sign the JWT
		signedJWT.sign(new RSASSASigner(this.getSigningKey()));
		// Create JWE object with signed JWT as payload
		JWEObject jweObject = new JWEObject(
				new JWEHeader.Builder(JWEAlgorithm.RSA_OAEP_256, EncryptionMethod.A256GCM).contentType("JWT").build(),
				new Payload(signedJWT));
		// Encrypt with the recipient's public key
		jweObject.encrypt(new RSAEncrypter(this.getPublicKey()));
		// Serialise to JWE compact form
		String jweString = jweObject.serialize();
		var partsIter = Utils.Lots.stream(Utils.Strings.split(jweString, '.'))
				.map(Utils.Bits::parseBase64Url)
				.iterator();
		var result = Utils.Lots.stream(partsIter).map(Base58::encodeBytes).joining("-");
		return toCredentials(result);
	}

	private Date getExpiresAt() {
		var result = new Date(System.currentTimeMillis() + this.ttl.toMillis());
		return result;
	}

	private static Entry<String, String> toCredentials(String result) {
		if (result == null)
			return null;
		int splitAt = result.length() / 2;
		return Utils.Lots.entry(result.substring(0, splitAt), result.substring(splitAt, result.length()));
	}

	private static Duration roundTTL(Duration ttl) {
		if (ttl == null)
			return null;
		var ttlMillis = ttl.toMillis();
		if (ttlMillis <= 0)
			return null;
		var sec = 1_000;
		var ttlMillisRounded = ((ttlMillis + (sec / 2)) / sec) * sec;
		if (ttlMillisRounded < ttlMillis)
			ttlMillis = ttlMillisRounded + sec;
		else
			ttlMillis = ttlMillisRounded;
		return Duration.ofMillis(ttlMillis);
	}
}
