package test;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;

import com.lfp.joe.core.function.ByteArrayHashers;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Base58;
import com.lfp.proxy.routing.impl.config.ProxyRoutingImplConfig;
import com.lfp.proxy.routing.tunnel.config.ProxyRoutingTunnelConfig;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.StreamEx;

public class SignTest {

	public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
		Bytes messageBytes = Utils.Crypto.hashMD5(Utils.Crypto.getSecureRandomString());
		var tunnelPrivateKey = Configs.get(ProxyRoutingTunnelConfig.class).tunnelPrivateKey();
		var tunnelPublicKey = Configs.get(ProxyRoutingImplConfig.class).tunnelPublicKey();
		Signature sign = Signature.getInstance("MD5WithRSA");
		sign.initSign(tunnelPrivateKey);
		sign.update(messageBytes.array());
		var signatureBytes = Bytes.from(sign.sign());
		String token = StreamEx.of(signatureBytes, messageBytes).map(v -> Base58.encode(v.array())).joining("_");
		System.out.println(token);
		System.out.println(token.substring(0, token.length() / 2));
		System.out.println(token.substring(token.length() / 2, token.length()));
		var signVerify = Signature.getInstance("MD5WithRSA");
		signVerify.initVerify(tunnelPublicKey);
		signVerify.update(messageBytes.array());
		// Verifying the signature
		boolean bool = signVerify.verify(signatureBytes.array());
		System.out.println(bool);
	}

}
