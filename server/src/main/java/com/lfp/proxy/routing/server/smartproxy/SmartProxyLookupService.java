package com.lfp.proxy.routing.server.smartproxy;

import java.io.IOException;
import java.util.List;

import com.github.throwable.beanref.BeanRef;
import com.google.common.reflect.TypeToken;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.location.Locations;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.lookup.LookupService;
import com.lfp.proxy.routing.lookup.ProxyRegistration;
import com.lfp.proxy.routing.lookup.ProxySupplier;
import com.lfp.proxy.routing.server.smartproxy.config.SmartProxyConfig;
import com.lfp.proxy.routing.service.ProxyAttributes;
import com.lfp.proxy.routing.service.ProxyAttributes.RotationType;
import com.lfp.proxy.routing.service.ProxyAttributes.ServerType;
import com.neovisionaries.i18n.CountryCode;

import okhttp3.Request;
import one.util.streamex.StreamEx;

public enum SmartProxyLookupService implements LookupService {
	INSTANCE;

	@SuppressWarnings("serial")
	private static final TypeToken<List<Endpoint>> TYPE_TOKEN = new TypeToken<List<Endpoint>>() {
	};

	private final boolean disabled;

	private SmartProxyLookupService() {
		var cfg = Configs.get(SmartProxyConfig.class);
		var disabled = false;
		for (var bp : BeanRef.$(SmartProxyConfig.class).all()) {
			var value = bp.get(cfg);
			if (value == null || Utils.Strings.isBlank(value.toString())) {
				disabled = true;
				org.slf4j.LoggerFactory.getLogger(this.getClass()).info("disabling. property empty:{}", bp.getPath());
				break;
			}
		}
		this.disabled = disabled;
	}

	@Override
	public StreamEx<ProxyRegistration> lookup() throws IOException {
		if (disabled)
			return StreamEx.empty();
		var streams = StreamEx.of(true, false).map(v -> {
			return Utils.Functions.unchecked(() -> lookup(v));
		});
		return Utils.Lots.flatMap(streams);
	}

	private StreamEx<ProxyRegistration> lookup(boolean random) throws IOException {
		String type = random ? "random" : "sticky";
		Request req = requestBuilder("https://api.smartproxy.com/v1/endpoints/" + type).build();
		List<Endpoint> endpoints = Ok.Calls.execute(Ok.Clients.get(), req).validateStatusCode(2).parseJson(TYPE_TOKEN)
				.getParsedBody();
		SmartProxyConfig cfg = Configs.get(SmartProxyConfig.class);
		StreamEx<Endpoint> endpointStream = Utils.Lots.stream(endpoints).filter(v -> v.getLocale().isPresent());
		var resultStream = endpointStream.map(v -> {
			var countryCode = Locations.toCountryCode(v.getLocale().orElse(null)).orElse(CountryCode.US);
			ProxyAttributes.Builder proxyAttributesB = ProxyAttributes.builder();
			proxyAttributesB.countryCode(countryCode);
			proxyAttributesB.serverTypes(ServerType.RESIDENTIAL);
			proxyAttributesB.rotationType(random ? RotationType.RANDOM : RotationType.STICKY);
			ProxySupplier proxySupplier = ProxySupplier.create(Proxy.Type.HTTP, v.getHostname(), cfg.proxyUsername(),
					cfg.proxyPassword(), v.getPortStart(), v.getPortEnd(), null);
			return ProxyRegistration.builder().lookupServiceType(this.getClass())
					.proxyAttributes(proxyAttributesB.build()).proxySupplier(proxySupplier).build();
		});
		return resultStream;

	}

	@Override
	public boolean skipValidation() {
		return true;
	}

	private static Request.Builder requestBuilder(String url) {
		return new Request.Builder().url(url).header(Headers.AUTHORIZATION,
				"Token " + SmartProxyAuthenticator.INSTANCE.get());
	}

	public static void main(String[] args) throws IOException {
		StreamEx<ProxyRegistration> lookup = SmartProxyLookupService.INSTANCE.lookup();
		System.out.println(Serials.Gsons.getPretty().toJson(lookup.toList()));
	}

}
