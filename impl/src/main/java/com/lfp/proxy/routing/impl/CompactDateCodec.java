package com.lfp.proxy.routing.impl;

import java.nio.ByteBuffer;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalField;
import java.util.Date;
import java.util.List;

import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Base58;

import at.favre.lib.bytes.Bytes;

public enum CompactDateCodec {
	INSTANCE;

	private static final List<TemporalField> TEMPORAL_FIELDS = List.of(ChronoField.YEAR, ChronoField.MONTH_OF_YEAR,
			ChronoField.DAY_OF_MONTH, ChronoField.HOUR_OF_DAY, ChronoField.MINUTE_OF_HOUR,
			ChronoField.SECOND_OF_MINUTE);

	public Bytes encode() {
		return encode(Instant.now());
	}

	public Bytes encode(Date date) {
		return encode(date == null ? null : date.toInstant());
	}

	public Bytes encode(Instant instant) {
		if (instant == null)
			return Utils.Bits.empty();
		var zdt = ZonedDateTime.ofInstant(instant, ZoneOffset.UTC);
		var byteBuffer = ByteBuffer.allocate(TEMPORAL_FIELDS.size() * 4);
		for (var tf : TEMPORAL_FIELDS) {
			var value = zdt.get(tf);
			byteBuffer.putInt(value);
		}
		var encoded = Utils.Bits.from(byteBuffer.array());
		return encoded;
	}

	public Instant decode(Bytes bytes) {
		if (bytes == null || bytes.length() == 0)
			return null;
		var zdt = ZonedDateTime.now(ZoneOffset.UTC);
		var temporalFieldsIter = TEMPORAL_FIELDS.iterator();
		var byteBuffer = bytes.buffer();
		while (temporalFieldsIter.hasNext() && byteBuffer.hasRemaining()) {
			var value = byteBuffer.getInt();
			var temporalField = temporalFieldsIter.next();
			zdt = zdt.with(temporalField, value);
		}
		return zdt.toInstant();
	}

	public Date decodeDate(Bytes bytes) {
		var instant = decode(bytes);
		return instant == null ? null : new Date(instant.toEpochMilli());
	}

	public static void main(String[] args) {
		Date date = new Date();
		System.out.println(date);
		var encoded = CompactDateCodec.INSTANCE.encode(date);
		System.out.println(Base58.encodeBytes(encoded));
		var decoded = CompactDateCodec.INSTANCE.decodeDate(encoded);
		System.out.println(decoded);
	}

}
