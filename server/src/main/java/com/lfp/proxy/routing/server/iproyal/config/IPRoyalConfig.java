package com.lfp.proxy.routing.server.iproyal.config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;

import org.aeonbits.owner.Config;

public interface IPRoyalConfig extends Config {

	String email();

	String password();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.json());
	}
}
