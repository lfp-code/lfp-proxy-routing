package com.lfp.proxy.routing.lookup.proxidize.rotation;

import java.io.IOException;

import com.lfp.joe.core.function.Scrapable;

public class NoOpIPRotationService extends Scrapable.Impl implements IPRotationService {

	@Override
	public void run() throws IOException {		
	}

}
