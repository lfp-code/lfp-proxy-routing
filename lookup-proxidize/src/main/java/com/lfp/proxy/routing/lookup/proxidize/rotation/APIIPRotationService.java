package com.lfp.proxy.routing.lookup.proxidize.rotation;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.common.base.Objects;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.retry.Retrys;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.lookup.proxidize.api.APIFunction;
import com.lfp.proxy.routing.lookup.proxidize.api.GetInfoResult;
import com.lfp.proxy.routing.lookup.proxidize.api.GetInfoResultFunction;
import com.lfp.proxy.routing.lookup.proxidize.config.ProxidizeConfig;

import okhttp3.OkHttpClient;

public class APIIPRotationService extends AbstractIPRotationService {

	public APIIPRotationService() {
		this(null, false);
	}

	protected APIIPRotationService(String serviceId, boolean disablePollStart) {
		super(Configs.get(ProxidizeConfig.class).apiRotationServiceTimeout(), serviceId, disablePollStart);
	}

	@Override
	protected void processRotation(GetInfoResult proxyResult, OkHttpClient client) throws IOException {
		Retrys.execute(6, Duration.ofSeconds(5), i -> {
			processRotationAttempt(proxyResult, client, i);
			return Nada.get();
		});
	}

	private void processRotationAttempt(GetInfoResult proxyResult, OkHttpClient client, int attempt)
			throws IOException {
		var index = proxyResult.getIndex();
		Map<String, List<Integer>> qp = index == null ? null : Map.of("index", List.of(index));
		APIFunction.INSTANCE.get(List.of("rotate"), qp, is -> {
			Utils.Functions.unchecked(() -> parseResponse(index, is));
			return Nada.get();
		});
	}

	private void parseResponse(int index, InputStream is) throws Exception {
		Exception error = null;
		List<String> lines = new ArrayList<String>();
		try (var br = Utils.Bits.bufferedReader(is);) {
			br.lines().forEach(lines::add);
		} catch (Exception e) {
			error = e;
		}
		var body = Utils.Lots.stream(lines).joining(Utils.Strings.newLine() + "");
		var successMessage = String.format("Rotating modem with index: %s", index);
		if (Utils.Strings.containsIgnoreCase(body, successMessage))
			return;
		if (Utils.Strings.isBlank(body))
			throw error;
		throw new IOException("invalid response body:" + body);
	}

	public static void main(String[] args) throws InterruptedException, IOException {
		long port = 1014;
		try (var service = new APIIPRotationService(null, true);) {
			var getInfoResult = GetInfoResultFunction.INSTANCE.get(true).filter(v -> {
				return Objects.equal(port, v.getPort());
			}).findFirst().get();
			service.processRotation(getInfoResult, true);
		}
		Thread.currentThread().join();
	}
}
