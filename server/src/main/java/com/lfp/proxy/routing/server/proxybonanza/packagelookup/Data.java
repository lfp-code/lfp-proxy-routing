
package com.lfp.proxy.routing.server.proxybonanza.packagelookup;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Data {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("login")
    @Expose
    private String login;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("expires")
    @Expose
    private String expires;
    @SerializedName("bandwidth")
    @Expose
    private Long bandwidth;
    @SerializedName("last_ip_change")
    @Expose
    private String lastIpChange;
    @SerializedName("ippacks")
    @Expose
    private List<Ippack> ippacks = new ArrayList<Ippack>();
    @SerializedName("authips")
    @Expose
    private List<Authip> authips = new ArrayList<Authip>();
    @SerializedName("package")
    @Expose
    private Package _package;

    /**
     * 
     * @return
     *     The id
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The login
     */
    public String getLogin() {
        return login;
    }

    /**
     * 
     * @param login
     *     The login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * 
     * @return
     *     The password
     */
    public String getPassword() {
        return password;
    }

    /**
     * 
     * @param password
     *     The password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 
     * @return
     *     The expires
     */
    public String getExpires() {
        return expires;
    }

    /**
     * 
     * @param expires
     *     The expires
     */
    public void setExpires(String expires) {
        this.expires = expires;
    }

    /**
     * 
     * @return
     *     The bandwidth
     */
    public Long getBandwidth() {
        return bandwidth;
    }

    /**
     * 
     * @param bandwidth
     *     The bandwidth
     */
    public void setBandwidth(Long bandwidth) {
        this.bandwidth = bandwidth;
    }

    /**
     * 
     * @return
     *     The lastIpChange
     */
    public String getLastIpChange() {
        return lastIpChange;
    }

    /**
     * 
     * @param lastIpChange
     *     The last_ip_change
     */
    public void setLastIpChange(String lastIpChange) {
        this.lastIpChange = lastIpChange;
    }

    /**
     * 
     * @return
     *     The ippacks
     */
    public List<Ippack> getIppacks() {
        return ippacks;
    }

    /**
     * 
     * @param ippacks
     *     The ippacks
     */
    public void setIppacks(List<Ippack> ippacks) {
        this.ippacks = ippacks;
    }

    /**
     * 
     * @return
     *     The authips
     */
    public List<Authip> getAuthips() {
        return authips;
    }

    /**
     * 
     * @param authips
     *     The authips
     */
    public void setAuthips(List<Authip> authips) {
        this.authips = authips;
    }

    /**
     * 
     * @return
     *     The _package
     */
    public Package getPackage() {
        return _package;
    }

    /**
     * 
     * @param _package
     *     The package
     */
    public void setPackage(Package _package) {
        this._package = _package;
    }

}
