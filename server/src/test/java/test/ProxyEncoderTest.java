package test;

import java.io.IOException;
import java.time.Duration;
import java.util.function.Predicate;

import com.google.common.base.Stopwatch;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.server.ServerProxyEncoder;
import com.lfp.proxy.routing.server.proxybonanza.ProxyBonanzaLookupService;

public class ProxyEncoderTest {

	public static void main(String[] args) throws IOException {
		var url = "http://FOIbPhewyqBZtqTk3WE7TCvwaROS:QqIAPmhbt0JHOpGEyd8cT7yCSS0N@proxy00.iplasso.com:1001";
		var uri = URIs.parse(url).orElse(null);
		Proxy proxy;
		if (uri != null)
			proxy = Proxy.builder(uri).build();
		else {
			Predicate<Proxy> lookupPredicate = v -> v.getType().equals(Proxy.Type.HTTP);
			var registrations = ProxyBonanzaLookupService.INSTANCE.lookup().filter(v -> {
				return lookupPredicate.test(v.getProxy());
			}).toList();
			proxy = registrations.get(Utils.Crypto.getRandomInclusive(0, registrations.size() - 1)).getProxy();
		}
		for (int i = 0; i < 10; i++) {
			if (i > 0)
				Threads.sleepUnchecked(Duration.ofSeconds(5));
			var sw = Stopwatch.createStarted();
			var credentials = ServerProxyEncoder.getInstance().apply(proxy);
			var elapsed = sw.elapsed();
			var tempProxy = proxy.toBuilder().username(credentials.getKey()).password(credentials.getValue()).build();
			System.out.println(Serials.Gsons.getPretty().toJson(tempProxy));
			System.out.println(elapsed.toMillis());
		}
	}
}
