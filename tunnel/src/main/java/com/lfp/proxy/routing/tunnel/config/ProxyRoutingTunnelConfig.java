package com.lfp.proxy.routing.tunnel.config;

import java.security.interfaces.RSAPrivateKey;
import java.time.Duration;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.properties.converter.DurationConverter;
import com.lfp.joe.properties.converter.JsonConverter;

public interface ProxyRoutingTunnelConfig extends Config {

	@DefaultValue("30000")
	int proxyPortStart();

	@DefaultValue("8080")
	int serverPort();

	@DefaultValue("300000") // 5 minutes
	long machineIPAddressCacheDurationMillis();

	@DefaultValue("2")
	int ioThreadCoreMultiplier();

	@DefaultValue("16")
	int workerThreadIOThreadMultiplier();

	@ConverterClass(JsonConverter.class)
	RSAPrivateKey tunnelPrivateKey();

	@ConverterClass(DurationConverter.class)
	Duration decodedProxyCacheExpireAfterWrite();

	@DefaultValue("15 second")
	@ConverterClass(DurationConverter.class)
	Duration decodedProxyCacheExpireAfterAccess();

	@DefaultValue("5 second")
	@ConverterClass(DurationConverter.class)
	Duration decodedProxyCacheRefreshAfterWrite();

	@DefaultValue("100000")
	long decodedProxyCacheMaxiumumSize();

	@DefaultValue("false")
	boolean disableAddressPublishDeveloper();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.json());
	}

}
