(
rm -f app*.jar*
rm -f run.sh
rm -f app.properties
ufw allow 30000:40000
nano +999999 app.env
bootscript=/root/boot.sh 
sudo tee $bootscript << END
#!/bin/bash
echo "boot script ran at at \$(date)" > /tmp/boot.log
set -e
source app.env
curl -L \$script_url | bash
END
chmod +x $bootscript
shutdown -rf now
)
