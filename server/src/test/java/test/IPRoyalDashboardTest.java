package test;
import java.io.OutputStream;

import java.io.IOException;
import java.net.URI;

import com.lfp.joe.net.cookie.Cookies;
import com.lfp.joe.net.html.FormData;
import com.lfp.joe.net.html.Jsoups;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.okhttp.cookie.CookieJars;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import org.jsoup.nodes.Document;

public class IPRoyalDashboardTest {

	public static void main(String[] args) throws IOException {
		var url = "https://dashboard.iproyal.com/login";
		var cookieStore = Cookies.createCookieStore();
		var proxy = Proxy.builder(URI.create("http://localhost:8888")).build();
		var client = Ok.Clients.get(proxy).newBuilder().cookieJar(CookieJars.create(cookieStore)).build();
		FormData formData;
		{
			Document doc;
			try (var rctx = Ok.Calls.execute(client, url).validateSuccess().parseHtml()) {
				doc = rctx.getParsedBody();
			}
			//System.out.println(doc);
			var form = Jsoups.select(doc, "form [name=\"password\"]").map(v -> {
				while (v != null) {
					if ("form".equalsIgnoreCase(v.tagName()))
						return v;
					v = v.parent();
				}
				return null;
			}).nonNull().findFirst().get();
			formData = Jsoups.parseFormData(form);
		}
		{
			formData.set("email", "xx");
			formData.set("password", "xx");
			var rb = Ok.Calls.toRequest(formData).newBuilder();
			rb.header(Headers.REFERER, "https://dashboard.iproyal.com/login");
			rb.header(Headers.ORIGIN, "https://dashboard.iproyal.com");
			try (var rctx = Ok.Calls.execute(client, rb.build()).validateSuccess().parseHtml()) {
				Utils.Bits.copy(rctx.getResponse().body().byteStream(), OutputStream.nullOutputStream());
			}
		}
		{
			Document doc;
			try (var rctx = Ok.Calls.execute(client, "https://dashboard.iproyal.com/products/4g-mobile-proxies")
					.validateSuccess().parseHtml()) {
				doc = rctx.getParsedBody();
			}
			System.out.println(doc);
		}
		System.out.println(Serials.Gsons.getPretty().toJson(cookieStore));
	}
}