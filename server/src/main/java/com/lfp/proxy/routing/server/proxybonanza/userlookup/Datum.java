
package com.lfp.proxy.routing.server.proxybonanza.userlookup;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Datum {

	@SerializedName("id")
	@Expose
	private Long id;
	@SerializedName("login")
	@Expose
	private String login;
	@SerializedName("password")
	@Expose
	private String password;
	@SerializedName("expires")
	@Expose
	private String expires;
	@SerializedName("bandwidth")
	@Expose
	private Long bandwidth;
	@SerializedName("last_ip_change")
	@Expose
	private String lastIpChange;

	/**
	 * 
	 * @return The id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id The id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * 
	 * @param login The login
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * 
	 * @return The password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * 
	 * @param password The password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * 
	 * @return The expires
	 */
	public String getExpires() {
		return expires;
	}

	/**
	 * 
	 * @param expires The expires
	 */
	public void setExpires(String expires) {
		this.expires = expires;
	}

	/**
	 * 
	 * @return The bandwidth
	 */
	public Long getBandwidth() {
		return bandwidth;
	}

	/**
	 * 
	 * @param bandwidth The bandwidth
	 */
	public void setBandwidth(Long bandwidth) {
		this.bandwidth = bandwidth;
	}

	/**
	 * 
	 * @return The lastIpChange
	 */
	public String getLastIpChange() {
		return lastIpChange;
	}

	/**
	 * 
	 * @param lastIpChange The last_ip_change
	 */
	public void setLastIpChange(String lastIpChange) {
		this.lastIpChange = lastIpChange;
	}

}
