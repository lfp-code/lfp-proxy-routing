package test;

import java.net.URI;
import java.time.Duration;

import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.threads.Threads;

import one.util.streamex.StreamEx;

public class IPRoyalTest {

	public static void main(String[] args) {
		String url = "http://oxy371232:Ttqfgjhxqqx@4g.iproyal.com:4027";
		var proxy = Proxy.builder(URI.create(url)).build();
		var client = Ok.Clients.get(proxy);
		for (int i = 0;; i++) {
			if (i > 0)
				Threads.sleepUnchecked(Duration.ofMillis(250));
			var ipAddress = Ok.Clients.getIPAddress(client);
			System.out.println(StreamEx.of(i, ipAddress).joining("\t"));
		}
	}

}
