package com.lfp.proxy.routing.server.smartproxy;

import java.util.Iterator;
import java.util.Locale;
import java.util.Optional;

import org.jsoup.helper.Validate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.lfp.joe.utils.Utils;

public class Endpoint {

	private transient Optional<Locale> _localeOp;

	public Optional<Locale> getLocale() {
		if (_localeOp != null)
			return _localeOp;

		synchronized (this) {
			if (_localeOp == null) {
				String lookup = this.getLocation();
				if (Utils.Strings.equalsIgnoreCase(lookup, "USA"))
					lookup = "us";
				if (Utils.Strings.equalsIgnoreCase(lookup, "UAE"))
					lookup = "ae";
				String[] countries = Locale.getISOCountries();
				Iterable<Locale> localeIble = () -> Utils.Lots.stream(countries).map(v -> new Locale("", v)).iterator();
				for (Iterator<Locale> iter = localeIble.iterator(); _localeOp == null && iter.hasNext();) {
					Locale locale = iter.next();
					if (Utils.Strings.equalsIgnoreCase(locale.getDisplayCountry(), lookup))
						_localeOp = Optional.of(locale);
				}
				for (Iterator<Locale> iter = localeIble.iterator(); _localeOp == null && iter.hasNext();) {
					Locale locale = iter.next();
					if (Utils.Strings.equalsIgnoreCase(locale.getCountry(), lookup))
						_localeOp = Optional.of(locale);

				}
				if (_localeOp == null)
					_localeOp = Optional.empty();
			}
		}
		return _localeOp;
	}

	public int getPortStart() {
		String range = this.getPortRange();
		return parseInteger(Utils.Strings.substringBefore(range, "-"));
	}

	public int getPortEnd() {
		String range = this.getPortRange();
		String str = Utils.Strings.substringAfter(range, "-");
		if (Utils.Strings.isBlank(str))
			return getPortStart();
		return parseInteger(str);
	}

	private static int parseInteger(String str) {
		str = Utils.Strings.trim(str);
		Validate.isTrue(Utils.Strings.isNumeric(str));
		return Integer.valueOf(str);
	}

	@SerializedName("location")
	@Expose
	private String location;
	@SerializedName("hostname")
	@Expose
	private String hostname;
	@SerializedName("port_range")
	@Expose
	private String portRange;

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getPortRange() {
		return portRange;
	}

	public void setPortRange(String portRange) {
		this.portRange = portRange;
	}

}