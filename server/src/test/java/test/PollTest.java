package test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import com.lfp.joe.core.function.Nada;
import com.lfp.joe.reactor.Scheduli;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class PollTest {

	public static void main(String[] args) throws InterruptedException {
//		test1();
		test2();
		Thread.currentThread().join();
	}

	private static void test1() {
		List<Disposable> subscriptions = new ArrayList<>();
		for (int i = 0; i < 25; i++) {
			var scheduler = Scheduli.unlimited();
			var flux = Flux
					.concat(Mono.just(true), Mono.just(false).repeat().delayElements(Duration.ofSeconds(2), scheduler))
					.subscribeOn(scheduler);
			var subscription = flux.subscribe(nil -> {
				System.out.println("starting:" + Thread.currentThread().getName());
				Threads.sleepUnchecked(Duration.ofSeconds(2));
				System.out.println("finished:" + Thread.currentThread().getName());
			});
			subscriptions.add(subscription);
		}
		Threads.sleepUnchecked(Duration.ofSeconds(10));
		subscriptions.forEach(v -> v.dispose());

	}

	private static void test2() {
		Mono.just(Nada.get()).repeat().delayUntil(nil -> {
			var delay = Duration.ofSeconds(Utils.Crypto.getRandomInclusive(1, 10));
			System.out.println("next delay:" + delay.toSeconds());
			return Mono.just(Nada.get()).delayElement(delay, Scheduli.unlimited());
		}).subscribe(nil -> {
			System.out.println("finished:" + Thread.currentThread().getName());
		});
	}

}
