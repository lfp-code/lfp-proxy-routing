package com.lfp.proxy.routing.tunnel;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.time.Duration;
import java.util.Arrays;
import java.util.Optional;

import com.lfp.connect.socks5.server.Socks5ProxyServer;
import com.lfp.connect.socks5.server.UsernamePasswordAuthenticatorLFP;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.impl.TunnelAddress;
import com.lfp.proxy.routing.impl.TunnelService;
import com.lfp.proxy.routing.tunnel.config.ProxyRoutingTunnelConfig;
import com.lfp.rsocket.client.RSocketClients;

import org.apache.commons.lang3.Validate;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;

import reactor.core.publisher.Flux;
import sockslib.common.methods.UsernamePasswordMethod;
import sockslib.server.manager.User;

public class App {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final Duration RECONNECT_DURATION = Duration.ofSeconds(15);

	public static void main(String[] args) throws InterruptedException, IOException {
		run();
	}

	public static void run() throws IOException, InterruptedException {
		run("0.0.0.0", -1);
	}

	public static void run(String host, int maxInstances) throws IOException, InterruptedException {
		Validate.isTrue(maxInstances == -1 || maxInstances > 0);
		if (!MachineConfig.isDeveloper()) {
			var tempDirectory = Utils.Files.tempDirectory();
			Utils.Files.deleteDirectoryContents(tempDirectory, v -> true);
		}
		ProxyRoutingTunnelConfig cfg = Configs.get(ProxyRoutingTunnelConfig.class);
		int servers = Math.max(1, Utils.Machine.logicalProcessorCount() - 1);
		if (maxInstances != -1)
			servers = Math.min(maxInstances, servers);
		int serverIndex = 0;
		for (; serverIndex < servers; serverIndex++) {
			int port = cfg.proxyPortStart() + serverIndex;
			var proxyChainFuture = ProxyChain.INSTANCE.start(new InetSocketAddress(host, port));
			var publishFuture = publishTunnelAddress(Proxy.Type.HTTP, port);
			proxyChainFuture.listener(() -> {
				publishFuture.cancel(true);
				logger.error("proxy chain exited, shutting down:{}", port);
				System.exit(1);
			});
		}
		{// socks server
			int port = cfg.proxyPortStart() + serverIndex;
			var server = new Socks5ProxyServer(port);
			server.setSupportMethods(new UsernamePasswordMethod(new UsernamePasswordAuthenticatorLFP() {

				@Override
				protected User getValidUser(String username, String password) {
					var user = asUser(username, password);
					var proxyOp = getUpstream(user);
					return proxyOp.isEmpty() ? null : user;
				}
			}));
			server.setProxySelector(userOp -> {
				return getUpstream(userOp.orElse(null)).orElse(null);
			});
			server.start();
			var publishFuture = publishTunnelAddress(Proxy.Type.SOCKS_5, port);
			Threads.Futures.logFailureError(publishFuture, false, "socks5Server failed on port:{}", port);
		}
		Thread.currentThread().join();

	}

	private static Optional<Proxy> getUpstream(User user) {
		if (user == null)
			return Optional.empty();
		try {
			var result = TunnelProxyDecoder.INSTANCE.apply(Arrays.asList(user.getUsername(), user.getPassword()));
			return Optional.ofNullable(result);
		} catch (Exception e) {
			logger.error("invalid token. user:{}", user, e);
			return Optional.empty();
		}
	}

	private static ListenableFuture<Nada> publishTunnelAddress(Proxy.Type proxyType, int proxyPort) {
		if (Boolean.TRUE.equals(Configs.get(ProxyRoutingTunnelConfig.class).disableAddressPublishDeveloper()))
			return FutureUtils.immediateResultFuture(Nada.get());
		return Threads.Pools.centralPool().submit(() -> {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					publishTunnelAddressAttempt(proxyType, proxyPort);
				} catch (Throwable t) {
					logger.warn("unexpected exception in tunnel address publisher, reconnecting in {}ms",
							RECONNECT_DURATION.toMillis(), t);
					Threads.sleepUnchecked(RECONNECT_DURATION);
				}
			}
			return Nada.get();
		});
	}

	private static void publishTunnelAddressAttempt(Proxy.Type proxyType, int proxyPort) {
		Flux<TunnelAddress> registerFlux = Flux.create(fluxSink -> {
			var publisher = new TunnelAddressPublisher(proxyType, proxyPort, fluxSink);
			publisher.start();
		});
		TunnelService client = RSocketClients.get(TunnelService.class);
		client.register(registerFlux).map(tunnelAddress -> {
			logger.info("proxy registration updated:{}", tunnelAddress);
			return tunnelAddress;
		}).blockLast();
	}

}
