package com.lfp.proxy.routing.lookup.airproxy;

import java.io.IOException;
import java.net.URI;
import java.util.Optional;

import org.apache.commons.lang3.Validate;

import com.google.gson.JsonElement;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.lookup.LookupService;
import com.lfp.proxy.routing.lookup.ProxyRegistration;
import com.lfp.proxy.routing.lookup.ProxySupplier;
import com.lfp.proxy.routing.lookup.airproxy.config.AirProxyConfig;
import com.lfp.proxy.routing.service.ProxyAttributes;
import com.lfp.proxy.routing.service.ProxyAttributes.RotationType;
import com.lfp.proxy.routing.service.ProxyAttributes.ServerType;
import com.lfp.proxy.routing.service.ProxyExt;
import com.neovisionaries.i18n.CountryCode;

import io.mikael.urlbuilder.UrlBuilder;
import one.util.streamex.StreamEx;

public enum AirProxyLookupService implements LookupService {
	INSTANCE;

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String API_URL = "https://airproxy.io/api/proxy/";

	@Override
	public StreamEx<ProxyRegistration> lookup() throws IOException {
		var uri = getUrlBuilder("list").toUri();
		var request = HttpRequests.request().uri(uri);
		var response = HttpClients.send(request);
		ListResponse listResponse;
		try (var is = response.body()) {
			Validate.isTrue(StatusCodes.isSuccess(response.statusCode(), 2));
			listResponse = Serials.Gsons.fromStream(is, ListResponse.class);
		}
		var proxyResultStream = Utils.Lots.stream(listResponse.getProxies()).nonNull();
		return proxyResultStream.map(v -> toProxyRegistration(v)).nonNull();
	}

	private ProxyRegistration toProxyRegistration(ProxyResult proxyResult) {
		var proxy = Optional.ofNullable(proxyResult).map(ProxyResult::toProxy).orElse(null);
		if (proxy == null)
			return null;
		ProxyAttributes proxyAttributes;
		{
			var blder = ProxyAttributes.builder();
			blder.countryCode(CountryCode.IT);
			blder.serverTypes(ServerType.MOBILE);
			blder.rotationType(RotationType.RANDOM);
			proxyAttributes = blder.build();
		}
		ProxyRegistration proxyRegistration;
		{
			var blder = ProxyRegistration.builder();
			blder.lookupServiceType(this.getClass());
			blder.proxyAttributes(proxyAttributes);
			blder.proxySupplier(new ProxySupplier(proxy.hash()) {

				@Override
				public ProxyExt get() {
					String ipAddress = Utils.Functions.unchecked(() -> getExternalIPAddress(proxyResult));
					return ProxyExt.build(proxy, ipAddress);
				}

			});
			proxyRegistration = blder.build();
		}
		return proxyRegistration;
	}

	private static String getExternalIPAddress(ProxyResult proxyResult) throws IOException {
		var uri = getUrlBuilder("ext_ip").addParameter("id", proxyResult.getId() + "").toUri();
		var request = HttpRequests.request().uri(uri);
		var response = HttpClients.send(request);
		JsonElement je;
		try (var is = response.body()) {
			if (!StatusCodes.isSuccess(response.statusCode(), 2)) {
				var bodyBytes = Utils.Bits.from(is);
				throw new IOException(
						String.format("invalid status code:%s body:%s", response.statusCode(), bodyBytes.encodeUtf8()));
			}
			je = Serials.Gsons.fromStream(is, JsonElement.class);
		}
		var ip = Serials.Gsons.tryGetAsString(je, "ip").orElse(null);
		if (!IPs.isValidIpAddress(ip)) {
			var client = Ok.Clients.get(proxyResult.toProxy());
			try (var rctx = Ok.Calls.execute(client, "https://icanhazip.com/").validateSuccess()) {
				ip = rctx.parseText().getParsedBody();
			}
		}
		if (!IPs.isValidIpAddress(ip))
			throw new IOException(String.format("ip not found. body:%s", je));
		return ip;
	}

	private static UrlBuilder getUrlBuilder(String... pathAppend) {
		var uri = URI.create(API_URL);
		var path = URIs.normalizePath(uri.getPath());
		var pathAppendJoined = Utils.Lots.stream(pathAppend).filter(Utils.Strings::isNotBlank).joining("/");
		if (!pathAppendJoined.isEmpty())
			path += "/" + pathAppendJoined;
		var urlb = UrlBuilder.fromString(API_URL).withPath(path).addParameter("key",
				Configs.get(AirProxyConfig.class).key());
		return urlb;
	}

	public static void main(String[] args) throws IOException {
		AirProxyLookupService.INSTANCE.lookup().forEach(v -> {
			System.out.println(v.getProxy().getPublicIPAddress());
		});
	}
}
