package com.lfp.proxy.routing.server;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang3.Validate;
import org.reactivestreams.Publisher;
import org.threadly.concurrent.SubmitterExecutor;
import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;

import com.google.common.util.concurrent.RateLimiter;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.net.dns.DNSs;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.net.proxy.Proxy.Type;
import com.lfp.joe.okhttp.proxy.OkHttpClientProxyTester;
import com.lfp.joe.reactor.Scheduli;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.impl.TunnelAddress;
import com.lfp.proxy.routing.impl.TunnelService;
import com.lfp.proxy.routing.lookup.LookupService;
import com.lfp.proxy.routing.lookup.LookupService.IPAuthorization;
import com.lfp.proxy.routing.lookup.ProxyRegistration;
import com.lfp.proxy.routing.lookup.proxidize.ProxidizeLookupService;
import com.lfp.proxy.routing.service.ProxyAttributes;
import com.lfp.proxy.routing.service.ProxyAttributes.ServerType;
import com.lfp.proxy.routing.service.ProxyExt;
import com.lfp.proxy.routing.service.RoutingService;

import one.util.streamex.StreamEx;
import reactor.core.Disposable;
import reactor.core.Disposables;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.SignalType;

public class RoutingAndTunnelServiceImpl implements TunnelService, RoutingService, Disposable {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final Duration LOOKUP_INTERVAL = MachineConfig.isDeveloper() ? Duration.ofSeconds(10)
			: Duration.ofSeconds(60);
	private static final int PROXY_TEST_THREADS = Utils.Machine.logicalProcessorCount() * 10;

	private final RateLimiter noTunnnelLogRateLimiter = RateLimiter.create(1);
	private final Executor asyncExecutor = Threads.Pools.centralPool();
	private final List<LookupService> lookupServices = new ArrayList<>();
	private final Registry<TunnelAddress> tunnelRegistry = new Registry<>();
	private final Registry<ProxyRegistration> proxyRegistry = new Registry<>();
	private final Disposable disposableDelegate = Disposables.single();

	public RoutingAndTunnelServiceImpl(LookupService... lookupServices) throws IOException {
		Utils.Lots.stream(lookupServices).nonNull().distinct().forEach(v -> this.lookupServices.add(v));
		Validate.isTrue(!this.lookupServices.isEmpty(), "at least one lookup service is required");
		for (IPAuthorization ipAuthorization : Utils.Lots.stream(this.lookupServices)
				.select(LookupService.IPAuthorization.class))
			ipAuthorization.authorizeIPs(IPs.getIPAddress());
		var testExecutor = Threads.Pools.centralPool().limit(PROXY_TEST_THREADS);
		for (LookupService service : lookupServices)
			FutureUtils.scheduleWhile(Threads.Pools.centralPool(), LOOKUP_INTERVAL.toMillis(), true,
					createLookupServiceLoader(testExecutor, service), nil -> !this.isDisposed());
	}

	private Callable<Void> createLookupServiceLoader(SubmitterExecutor submitterExecutor, LookupService service) {
		return () -> {
			try {
				logger.info("{} - starting proxy lookup", service.getClass().getName());
				runLookupServiceLoader(submitterExecutor, service);
			} catch (Throwable t) {
				logger.error("{} - uncaught error during lookup", service.getClass().getName(), t);
			} finally {
				logger.info("{} - finished proxy lookup", service.getClass().getName());
			}
			return null;
		};
	}

	private void runLookupServiceLoader(SubmitterExecutor submitterExecutor, LookupService service) throws IOException {
		Objects.requireNonNull(submitterExecutor);
		Objects.requireNonNull(service);
		Set<ProxyRegistration> removeProxyRegistrations = Utils.Lots.stream(this.proxyRegistry.getAll())
				.filter(v -> service.getClass().equals(v.getLookupServiceType())).toSet();
		StreamEx<ProxyRegistration> lookupProxyRegistration = service.lookup();
		lookupProxyRegistration = lookupProxyRegistration.nonNull();
		lookupProxyRegistration = lookupProxyRegistration.distinct(v -> v.uuid());
		AtomicLong passing = new AtomicLong();
		AtomicLong failing = new AtomicLong();
		BiConsumer<Proxy, IOException> errorConsumer = (proxy, failure) -> {
			List<String> parts = new ArrayList<>(2);
			parts.add(failure.getMessage());
			if (failure.getCause() != null)
				parts.add(failure.getCause().getMessage());
			logger.warn(Utils.Lots.stream(parts).filter(Utils.Strings::isNotBlank).joining(" "));
		};
		logger.info("{} - starting proxy testing", service.getClass().getName());
		StreamEx<ListenableFuture<ProxyRegistration>> futureStream = lookupProxyRegistration.map(v -> {
			if (v.isDisableTesting()) {
				passing.incrementAndGet();
				return FutureUtils.immediateResultFuture(v);
			}
			ListenableFuture<ProxyRegistration> future = submitterExecutor.submit(() -> {
				boolean passes;
				if (service.skipValidation())
					passes = true;
				else if (OkHttpClientProxyTester.getDefault().isValid(v.getProxy(), errorConsumer))
					passes = true;
				else
					passes = false;
				if (passes) {
					passing.incrementAndGet();
					return v;
				}
				failing.incrementAndGet();
				return null;
			});
			Threads.Futures.logFailureError(future, true, "unexpected error");
			return future;
		});
		var proxyRegistrations = Threads.Futures.join(FutureUtils.makeResultListFuture(futureStream.toList(), true));
		{
			long total = failing.get() + passing.get();
			float perc = total == 0 ? -1f : (float) passing.get() / total;
			logger.info("{} - completed proxy testing. passPercentage:{}% passing:{} failing:{} total:{}",
					service.getClass().getName(), perc * 100, passing.get(), failing.get(), total);
		}
		for (ProxyRegistration pr : proxyRegistrations) {
			if (pr == null)
				continue;
			removeProxyRegistrations.remove(pr);
			this.proxyRegistry.add(pr);
		}
		if (!removeProxyRegistrations.isEmpty())
			this.proxyRegistry.removeIf(v -> removeProxyRegistrations.contains(v));
	}

	@Override
	public Flux<TunnelAddress> register(Publisher<TunnelAddress> addressPublisher) {
		Flux<TunnelAddress> addressFlux = Flux.from(addressPublisher).subscribeOn(Scheduli.unlimited());
		Predicate<Optional<TunnelAddress>> updateTunnelAddress = new Predicate<Optional<TunnelAddress>>() {

			private TunnelAddress currentTunnelAddress;
			private Map<LookupService, Optional<Void>> serviceInitTracker;

			@Override
			public synchronized boolean test(Optional<TunnelAddress> tunnelAddressOp) {
				var tunnelAddress = tunnelAddressOp.orElse(null);
				if (Objects.equals(currentTunnelAddress, tunnelAddress)) {
					ensureInitializeTunnelAddress(serviceInitTracker, tunnelAddress);
					return false;
				}
				List<TunnelAddress> removed = currentTunnelAddress == null ? List.of()
						: RoutingAndTunnelServiceImpl.this.tunnelRegistry.removeIf(v -> {
							if (Objects.equals(currentTunnelAddress, v))
								return true;
							return false;
						});
				for (var ta : removed)
					logger.info("tunnel address removed:{}", ta);
				this.currentTunnelAddress = tunnelAddress;
				if (this.currentTunnelAddress == null) {
					this.serviceInitTracker = null;
					return true;
				}
				this.serviceInitTracker = new ConcurrentHashMap<>();
				ensureInitializeTunnelAddress(serviceInitTracker, this.currentTunnelAddress);
				if (!tunnelRegistry.add(this.currentTunnelAddress))
					logger.warn("tunnel address registration failed, duplicate:{}", this.currentTunnelAddress);
				else
					logger.info("tunnel address registered:{}", this.currentTunnelAddress);
				return true;
			}
		};
		Runnable destroyTunnelAddress = () -> updateTunnelAddress.test(Optional.empty());
		addressFlux = addressFlux.doOnCancel(destroyTunnelAddress).doOnTerminate(destroyTunnelAddress);
		addressFlux = addressFlux.filter(tunnelAddress -> {
			try {
				return updateTunnelAddress.test(Optional.of(tunnelAddress));
			} catch (Throwable t) {
				destroyTunnelAddress.run();
				throw Utils.Exceptions.asRuntimeException(t);
			}
		});
		return addressFlux;
	}

	@Override
	public Mono<ProxyExt> lookupProxy(Type typeRequest, ProxyAttributes proxyAttributes) {
		if (typeRequest == null)
			typeRequest = Proxy.Type.HTTP;
		proxyAttributes = proxyAttributes != null ? proxyAttributes.toBuilder().build()
				: ProxyAttributes.builder().build();
		return lookupProxyInternal(typeRequest, proxyAttributes);
	}

	protected Mono<ProxyExt> lookupProxyInternal(Type typeRequest, ProxyAttributes proxyAttributes) {
		CompletableFuture<Optional<ProxyExt>> future = CompletableFuture.supplyAsync(() -> {
			Optional<ProxyRegistration> proxyRegistrationOp = this.proxyRegistry.nextIf(v -> {
				if (!v.isCompatible(typeRequest, proxyAttributes))
					return false;
				return true;
			});
			if (!proxyRegistrationOp.isPresent()) {
				logger.warn("proxy for request not found. type:{} proxyAttributes:{}", typeRequest, proxyAttributes);
				return Optional.empty();
			}
			Optional<TunnelAddress> tunnelAddressOp = this.tunnelRegistry.nextIf(v -> {
				if (!Objects.equals(v.getProxy().getType(), typeRequest))
					return false;
				var restrictTunnelIPAddress = proxyRegistrationOp.get().getRestrictTunnelIPAddress();
				if (!restrictTunnelIPAddress.isEmpty() && !restrictTunnelIPAddress.contains(v.getMachineIPAddress()))
					return false;
				return true;
			});
			if (!tunnelAddressOp.isPresent()) {
				if (noTunnnelLogRateLimiter.tryAcquire())
					logger.warn("tunnel registration not found. type:{} proxyAttributes:{}", typeRequest,
							proxyAttributes);
				return Optional.empty();
			}
			var upstreamProxy = proxyRegistrationOp.get().getProxy();
			TunnelAddress tunnelAddress = tunnelAddressOp.get();
			var credentials = Utils.Functions.unchecked(() -> ServerProxyEncoder.getInstance().apply(upstreamProxy));
			var proxyExtB = ProxyExt.builder(tunnelAddress.getProxy(), upstreamProxy.getPublicIPAddress().orElse(null));
			proxyExtB.username(credentials.getKey());
			proxyExtB.password(credentials.getValue());
			var proxy = proxyExtB.build();
			if (IPs.isValidIpAddress(proxy.getHostname())) {
				var hostname = DNSs.toWildcardDNSHostname(proxy.getHostname(), upstreamProxy.uuid(10));
				proxy = (ProxyExt) proxy.toBuilder().hostname(hostname).build();
			}
			if (proxyRegistrationOp.get().isLookupPublicIPAddressWithTunnel())
				proxy = PublicIPAddressAppender.INSTANCE.apply(proxy, upstreamProxy.uuid());
			return Optional.of(proxy);
		}, asyncExecutor);
		future.whenComplete((v, t) -> {
			if (t == null || Utils.Exceptions.isCancelException(t))
				return;
			logger.error("error during proxy lookup", t);
		});
		return Mono.fromFuture(future).doFinally(st -> {
			if (Arrays.asList(SignalType.CANCEL, SignalType.ON_ERROR).contains(st))
				Threads.Futures.cancel(future, true);
		}).filter(Optional::isPresent).map(Optional::get).subscribeOn(Scheduli.unlimited())
				.publishOn(Scheduli.unlimited());
	}

	@Override
	public Flux<ProxyAttributes> availableProxyAttributes() {
		List<ProxyAttributes> attributes = this.proxyRegistry.getAll().stream()
				.map(ProxyRegistration::getProxyAttributes).distinct().collect(Collectors.toList());
		return Flux.fromIterable(attributes);
	}

	private void ensureInitializeTunnelAddress(Map<LookupService, Optional<Void>> serviceTracker,
			TunnelAddress tunnelAddress) {
		if (serviceTracker == null || tunnelAddress == null)
			return;
		for (LookupService service : Utils.Lots.stream(this.lookupServices).nonNull()) {
			serviceTracker.computeIfAbsent(service, nil -> {
				boolean success = initializeTunnelAddress(service, tunnelAddress, t -> {
					logger.warn("tunnel initialize failed. service:{} tunnelAddress:{}", service, tunnelAddress);
				});
				if (!success)
					return null;
				return Optional.empty();
			});
		}
	}

	private boolean initializeTunnelAddress(LookupService service, TunnelAddress tunnelAddress,
			Consumer<IOException> onError) {
		if (service instanceof IPAuthorization)
			try {
				((IPAuthorization) service).authorizeIPs(tunnelAddress.getMachineIPAddress());
			} catch (IOException e) {
				onError.accept(e);
				return false;
			}
		return true;
	}

	@Override
	public void dispose() {
		disposableDelegate.dispose();
	}

	@Override
	public boolean isDisposed() {
		return disposableDelegate.isDisposed();
	}

	public static void main(String[] args) throws InterruptedException, IOException {
		RoutingAndTunnelServiceImpl routingAndTunnelServiceImpl = new RoutingAndTunnelServiceImpl(
				ProxidizeLookupService.INSTANCE);
		for (var type : Proxy.Type.values()) {
			routingAndTunnelServiceImpl.register(Flux.create(fs -> {
				new Thread(() -> {
					while (true) {
						var proxy = Proxy.builder().type(type).hostname(IPs.getIPAddress()).port(1234).build();
						fs.next(TunnelAddress.builder().proxy(proxy).build());
						Utils.Functions.unchecked(() -> Thread.sleep(1000));
					}
				}).start();
			})).subscribe(v -> {
				System.out.println("registered:" + v);
			});
		}
		while (true) {
			Thread.sleep(1_000);
			Proxy proxy;
			try {
				proxy = routingAndTunnelServiceImpl
						.lookupProxy(Proxy.Type.HTTP, ProxyAttributes.builder().serverTypes(ServerType.MOBILE).build())
						.block();
			} catch (Exception e) {
				logger.warn("could not get proxy:{}", e.getMessage());
				Thread.sleep(1000);
				continue;
			}

			System.out.println(Serials.Gsons.getPretty().toJson(proxy));
			// var pa = routingAndTunnelServiceImpl.availableProxyAttributesBlocking();
			// System.out.println(Serials.Gsons.getPretty().toJson(pa));
		}

	}

}
