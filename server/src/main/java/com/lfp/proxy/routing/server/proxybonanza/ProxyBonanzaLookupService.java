package com.lfp.proxy.routing.server.proxybonanza;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import com.goebl.david.Webb;
import com.google.common.reflect.TypeToken;
import com.google.common.util.concurrent.RateLimiter;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.location.Location;
import com.lfp.joe.location.Locations;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.net.proxy.Proxy.Type;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.lookup.LookupService;
import com.lfp.proxy.routing.lookup.ProxyRegistration;
import com.lfp.proxy.routing.lookup.ProxySupplier;
import com.lfp.proxy.routing.server.proxybonanza.authips.AuthenticatedIPs;
import com.lfp.proxy.routing.server.proxybonanza.authipupdate.AuthResponse;
import com.lfp.proxy.routing.server.proxybonanza.authipupdate.Data;
import com.lfp.proxy.routing.server.proxybonanza.config.ProxyBonanzaConfig;
import com.lfp.proxy.routing.server.proxybonanza.packagelookup.Ippack;
import com.lfp.proxy.routing.server.proxybonanza.packagelookup.PackageLookup;
import com.lfp.proxy.routing.server.proxybonanza.userlookup.Datum;
import com.lfp.proxy.routing.server.proxybonanza.userlookup.UserLookup;
import com.lfp.proxy.routing.service.ProxyAttributes;
import com.lfp.proxy.routing.service.ProxyAttributes.RotationType;
import com.lfp.proxy.routing.service.ProxyAttributes.ServerType;
import com.lfp.proxy.routing.service.ProxyExt;
import com.neovisionaries.i18n.CountryCode;

import at.favre.lib.bytes.Bytes;
import okhttp3.FormBody;
import okhttp3.Request;
import one.util.streamex.StreamEx;

public enum ProxyBonanzaLookupService implements LookupService, LookupService.IPAuthorization {
	INSTANCE;

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int MAX_RETRY = 3;
	private static final boolean DISABLE_SOCKS = false;
	private static final RateLimiter RATE_LIMITER = RateLimiter.create(1.5);

	@SuppressWarnings("resource")
	@Override
	public StreamEx<ProxyRegistration> lookup() throws IOException {
		var streams = streamPackageLookups().map(v -> toProxyRegistrations(v));
		return Utils.Lots.flatMap(streams);
	}

	@Override
	public void authorizeIPs(String... ipAddresses) throws IOException {
		authenticateIPOnAllPackages(ipAddresses == null ? null : () -> Arrays.asList(ipAddresses).iterator());
	}

	private StreamEx<ProxyRegistration> toProxyRegistrations(PackageLookup plup) {
		if (plup == null)
			return StreamEx.empty();
		Map<Location, Set<List<Proxy>>> result = new LinkedHashMap<>();
		var listStream = Utils.Lots.stream(plup.getData().getIppacks()).map(ippack -> {
			return mapToProxyRegistrations(plup, ippack);
		});
		return Utils.Lots.flatMapIterables(listStream);
	}

	private StreamEx<ProxyRegistration> mapToProxyRegistrations(PackageLookup plup, Ippack ippack) {
		Map<Proxy.Type, Integer> typeToPort = new LinkedHashMap<>();
		BiConsumer<Proxy.Type, Long> typeToPortRegister = (type, port) -> {
			if (type == null || port == null || port < 0)
				return;
			typeToPort.put(type, port.intValue());
		};
		typeToPortRegister.accept(Proxy.Type.SOCKS_5, ippack.getPortSocks());
		typeToPortRegister.accept(Proxy.Type.HTTP, ippack.getPortHttp());
		return Utils.Lots.stream(typeToPort).map(ent -> {
			return mapToProxyRegistration(plup, ippack, ent.getKey(), ent.getValue());
		});
	}

	private ProxyRegistration mapToProxyRegistration(PackageLookup plup, Ippack ippack, Proxy.Type proxyType,
			int port) {
		var proxyRegistrationB = ProxyRegistration.builder().lookupServiceType(this.getClass());
		{
			ProxyAttributes.Builder proxyAttributesB = ProxyAttributes.builder().serverTypes(ServerType.DATA_CENTER)
					.rotationType(RotationType.STICKY);
			String cc = ippack.getProxyserver().getGeoregion().getCountry().getIsocode();
			cc = Utils.Strings.trimToNull(cc);
			proxyAttributesB.countryCode(Locations.parseCountryCode(cc).orElse(CountryCode.US));
			proxyRegistrationB.proxyAttributes(proxyAttributesB.build());
		}
		ProxySupplier proxySupplier;
		{
			ProxyExt.Builder bld = ProxyExt.builder();
			bld.type(proxyType);
			bld.hostname(ippack.getIp());
			bld.port(port);
			bld.username(plup.getData().getLogin());
			bld.password(plup.getData().getPassword());
			bld.publicIPAddress(ippack.getIp());
			proxySupplier = ProxySupplier.create(bld.build());
		}
		return proxyRegistrationB.proxySupplier(proxySupplier).build();
	}

	public static void authenticateIPOnAllPackages(Iterable<String> ipAddresses) throws IOException {
		List<String> validIps = Utils.Lots.stream(ipAddresses).filter(ip -> IPs.isValidIpAddress(ip)).distinct()
				.toList();
		if (validIps.isEmpty())
			return;
		AuthenticatedIPs authed = getAuthenticatedIPs();
		StreamEx<PackageLookup> proxyCollectionStream = streamPackageLookups();
		for (PackageLookup plup : proxyCollectionStream) {
			long plupId = plup.getData().getId();
			for (String ipAddress : validIps) {
				if (packageNeedsIPAuth(authed, ipAddress, plupId)) {
					authroizeIP(ipAddress, plupId);
				}
			}
		}
	}

	private static StreamEx<PackageLookup> streamPackageLookups() throws IOException {
		return streamDatums().map(datum -> {
			PackageLookup plup;
			try {
				plup = doPackageLookup(datum.getId());
			} catch (IOException e) {
				throw new java.lang.RuntimeException(e);
			}
			return plup;
		});
	}

	private static StreamEx<Datum> streamDatums() throws IOException {
		UserLookup userLookup;
		{
			var uri = URI.create("https://api.proxybonanza.com/v1/userpackages.json");
			userLookup = execute(uri, null, TypeToken.of(UserLookup.class), 2);
		}
		if (userLookup == null || !Boolean.TRUE.equals(userLookup.getSuccess()))
			return StreamEx.of();
		return Utils.Lots.stream(userLookup.getData()).nonNull().filter(d -> d.getId() != null)
				.filter(d -> isPackageIdAcceptable(d.getId()));
	}

	private static PackageLookup doPackageLookup(Long id) throws IOException {
		String url = "https://api.proxybonanza.com/v1/userpackages/" + id + ".json";
		PackageLookup pl = execute(URI.create(url), null, TypeToken.of(PackageLookup.class), 2);
		return pl == null || !Boolean.TRUE.equals(pl.getSuccess()) ? null : pl;
	}

	private static AuthenticatedIPs getAuthenticatedIPs() throws IOException {
		String url = "https://api.proxybonanza.com/v1/authips.json";
		AuthenticatedIPs response = execute(URI.create(url), null, TypeToken.of(AuthenticatedIPs.class), 2);
		if (response == null || !Boolean.TRUE.equals(response.getSuccess()))
			throw new IOException("could not get authorized ips from proxy bonanza");
		return response;
	}

	private static boolean isPackageIdAcceptable(Long id) {
		if (id == null)
			return false;
		List<Long> banned = Configs.get(ProxyBonanzaConfig.class).disabledPackages();
		boolean banMatch = Utils.Lots.stream(banned).nonNull().filter(v -> v.equals(id)).findFirst().isPresent();
		return !banMatch;
	}

	private static boolean packageNeedsIPAuth(AuthenticatedIPs authed, String ipAddress, Long id) {
		for (com.lfp.proxy.routing.server.proxybonanza.authips.Datum data : authed.getData())
			if (StringUtils.equalsIgnoreCase(ipAddress, data.getIp()) && id.equals(data.getUserpackageId()))
				return false;
		return true;
	}

	private static void authroizeIP(String ipAddress, Long id) throws IOException {
		logger.info("authenticating ip address: " + ipAddress);
		String url = "https://api.proxybonanza.com/v1/authips.json";
		AuthResponse response = execute(URI.create(url), requestb -> {
			var fbb = new FormBody.Builder();
			fbb.add("userpackage_id", String.valueOf(id));
			fbb.add("ip", ipAddress);
			return requestb.post(fbb.build());
		}, TypeToken.of(AuthResponse.class), 2);
		if (!Boolean.TRUE.equals(response.getSuccess())) {
			Data data = response.getData();
			String msg = data != null ? Serials.Gsons.get().toJson(data) : String.valueOf(response);
			throw new IOException("could not get authorized ips from proxy bonanza: " + msg);
		}
	}

	private static <X> X execute(URI uri, Function<Request.Builder, Request.Builder> requestModifier,
			TypeToken<X> typeToken, Integer... okCodes) throws IOException {
		Objects.requireNonNull(typeToken);
		Bytes bytes = execute(uri, requestModifier, okCodes);
		if (MachineConfig.isDeveloper())
			logger.info(bytes.encodeUtf8());
		return Serials.Gsons.fromBytes(bytes, typeToken);
	}

	@SuppressWarnings("resource")
	private static Bytes execute(URI uri, Function<Request.Builder, Request.Builder> requestModifier,
			Integer... okCodes) throws IOException {
		Objects.requireNonNull(uri);
		Set<Integer> codes = Utils.Lots.stream(okCodes).nonNull().filter(v -> v > 0).toSet();
		if (codes.isEmpty())
			codes.add(2);
		String apiKey = Configs.get(ProxyBonanzaConfig.class).apiKey();
		Validate.isTrue(StringUtils.isNotBlank(apiKey), "proxy bonanza api key may not be blank");
		for (int i = 0; i < MAX_RETRY; i++) {
			RATE_LIMITER.acquire();
			synchronized (RATE_LIMITER) {
				var requestb = new okhttp3.Request.Builder().url(uri.toString()).header(Webb.HDR_AUTHORIZATION, apiKey);
				if (requestModifier != null)
					requestb = requestModifier.apply(requestb);
				var response = Ok.Clients.get().newCall(requestb.build()).execute();
				Long sleepMillis;
				Bytes bytes;
				try (var body = response.body().byteStream()) {
					if (response.code() == 429) {
						sleepMillis = Utils.Crypto.getRandomInclusive(Duration.ofSeconds(30).toMillis(),
								Duration.ofSeconds(45).toMillis());
						bytes = Utils.Bits.empty();
					} else {
						StatusCodes.validate(response.code(), Streams.of(codes).mapToInt(v -> v).toArray());
						sleepMillis = null;
						bytes = Utils.Bits.from(body);
					}
				} finally {
					Ok.Calls.close(response);
				}
				if (sleepMillis != null) {
					String msg = String.format("throttling proxy bonanza connection. uri:%s", uri);
					logger.warn(msg);
					Threads.sleepUnchecked(sleepMillis);
					continue;
				}
				Validate.isTrue(bytes.length() > 0, "response did not contain a body. requestURI:%s", uri);
				return bytes;
			}
		}
		throw new IOException(String.format("unexpected error while processing request to uri:%s", uri));

	}

	public static void main(String[] args) throws IOException {
		var json = "{\"success\":true,\"data\":[{\"id\":39309,\"custom_name\":null,\"login\":\"reggiepierce\",\"password\":\"NreXzJSczVnPdumhNgPirHeru3te\",\"expires\":\"2022-06-06T23:59:00+02:00\",\"bandwidth\":107374182400,\"last_ip_change\":\"2017-08-29T00:00:00+00:00\",\"low_banwidth_notification_percent\":10,\"package\":{\"name\":\"International 22\",\"bandwidth\":107374182400,\"price\":{\"amount\":\"7700\",\"currency\":\"USD\"},\"howmany_ips\":22,\"price_per_gig\":{\"amount\":\"50\",\"currency\":\"USD\"},\"package_type\":\"geo\",\"howmany_authips\":70,\"ip_type\":4,\"price_user_formatted\":\"$77.00\"},\"bandwidth_gb\":107.37,\"additional_bandwidth_gb\":0,\"bandwidth_percent_left_human\":\"100%\",\"expiration_date_human\":\"27 days\",\"name\":\"International 22\",\"subaccount_limit\":0},{\"id\":46549,\"custom_name\":null,\"login\":\"reggiepierce\",\"password\":\"Cl3aDfVc3DWeEUNLRf3CTydm5arz\",\"expires\":\"2022-05-13T23:59:00+02:00\",\"bandwidth\":21471269820305,\"last_ip_change\":\"2019-01-18T00:00:00+00:00\",\"low_banwidth_notification_percent\":10,\"package\":{\"name\":\"Shared VIP\",\"bandwidth\":21474836480000,\"price\":{\"amount\":\"80000\",\"currency\":\"USD\"},\"howmany_ips\":1800,\"price_per_gig\":{\"amount\":\"100\",\"currency\":\"USD\"},\"package_type\":\"shared\",\"howmany_authips\":70,\"ip_type\":4,\"price_user_formatted\":\"$800.00\"},\"bandwidth_gb\":21471.27,\"additional_bandwidth_gb\":0,\"bandwidth_percent_left_human\":\"99.98%\",\"expiration_date_human\":\"3 days\",\"name\":\"Shared VIP\",\"subaccount_limit\":0}],\"pagination\":{\"page_count\":1,\"current_page\":1,\"has_next_page\":false,\"has_prev_page\":false,\"count\":2,\"limit\":null}}";
		var userLookup = Serials.Gsons.get().fromJson(json, UserLookup.class);
		System.out.println(userLookup);
		StreamEx<ProxyRegistration> stream = ProxyBonanzaLookupService.INSTANCE.lookup();
		stream.forEach(v -> {
			if (v.isCompatible(Type.HTTP, ProxyAttributes.builder().build()))
				System.out.println("RESIDENTIAL:" + v);
			if (v.isCompatible(Type.HTTP, ProxyAttributes.builder().rotationType(RotationType.STICKY).build()))
				System.out.println("STICKY:" + v);
			System.out.println(Serials.Gsons.getPretty().toJson(v.getProxy()));
		});
	}

}
