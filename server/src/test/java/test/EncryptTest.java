package test;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import com.github.throwable.beanref.BeanRef;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Base58;
import com.lfp.proxy.routing.impl.config.ProxyRoutingImplConfig;
import com.lfp.proxy.routing.server.config.ProxyRoutingServerConfig;
import com.lfp.proxy.routing.server.proxybonanza.ProxyBonanzaLookupService;
import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.RSAEncrypter;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

public class EncryptTest {

	public static void main(String[] args) throws IOException, InvalidKeyException, IllegalBlockSizeException,
			BadPaddingException, NoSuchAlgorithmException, JOSEException {
		var registrations = ProxyBonanzaLookupService.INSTANCE.lookup()
				.filter(v -> v.getProxy().getType().equals(Proxy.Type.HTTP)).toList();
		Collections.shuffle(registrations);
		RSAKey serverRSAKey = new RSAKey.Builder(Configs.get(ProxyRoutingImplConfig.class).serverPublicKey())
				.privateKey(Configs.get(ProxyRoutingServerConfig.class).serverPrivateKey()).build();
		RSAKey tunnelPublicKey = new RSAKey.Builder(Configs.get(ProxyRoutingImplConfig.class).tunnelPublicKey())
				.build();
		var token = encode(serverRSAKey, tunnelPublicKey, registrations.get(0).getProxy());
		System.out.println(token);
		int split = token.length() / 2;
		System.out.println(token.substring(0, split));
		System.out.println(token.substring(split, token.length()));
	}

	private static String encode(RSAKey senderJWK, RSAKey recipientPublicJWK, Proxy proxy) throws JOSEException {
		var expiresAt = new Date(System.currentTimeMillis() + Duration.ofDays(1).toMillis());
		// expiresAt = new Date();
		var claimBuilder = new JWTClaimsSet.Builder().expirationTime(expiresAt);
		for (var mp : Proxy.meta().metaPropertyIterable())
			claimBuilder.claim(mp.name(), mp.get(proxy));
		// Create JWT
		SignedJWT signedJWT = new SignedJWT(
				new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(senderJWK.getKeyID()).build(), claimBuilder.build());
		// Sign the JWT
		signedJWT.sign(new RSASSASigner(senderJWK));
		// Create JWE object with signed JWT as payload
		JWEObject jweObject = new JWEObject(
				new JWEHeader.Builder(JWEAlgorithm.RSA_OAEP_256, EncryptionMethod.A256GCM).contentType("JWT").build(),
				new Payload(signedJWT));
		// Encrypt with the recipient's public key
		jweObject.encrypt(new RSAEncrypter(recipientPublicJWK));
		// Serialise to JWE compact form
		String jweString = jweObject.serialize();
		var partsIter = Utils.Lots.stream(Utils.Strings.split(jweString, '.')).map(Utils.Bits::parseBase64Url)
				.iterator();
		var result = Utils.Lots.stream(partsIter).map(Base58::encodeBytes).joining("-");
		return result;
	}
}
