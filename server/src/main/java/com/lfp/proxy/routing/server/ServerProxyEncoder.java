package com.lfp.proxy.routing.server;

import java.io.IOException;
import java.net.URI;
import java.security.interfaces.RSAPrivateKey;
import java.time.Duration;
import java.util.Map.Entry;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.jwk.serialize.RSAKeySerializer;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.impl.ProxyCodec;
import com.lfp.proxy.routing.impl.ProxyEncoder;
import com.lfp.proxy.routing.server.config.ProxyRoutingServerConfig;

public class ServerProxyEncoder implements ProxyCodec<Proxy, Entry<String, String>> {

	public static final MemoizedSupplier<ServerProxyEncoder> INSTANCE_S = Utils.Functions
			.memoize(ServerProxyEncoder::new);

	public static ServerProxyEncoder getInstance() {
		return INSTANCE_S.get();
	}

	private final ProxyCodec<Proxy, Entry<String, String>> delegate;

	protected ServerProxyEncoder() {
		var cfg = Configs.get(ProxyRoutingServerConfig.class);
		ProxyCodec<Proxy, Entry<String, String>> delegate = new ProxyEncoder(cfg.encodedProxyTTL(),
				cfg.serverPrivateKey());
		delegate = delegate.cached(cfg.encodedProxyCacheMaxiumumSize(), cfg.encodedProxyCacheExpireAfterWrite(),
				cfg.encodedProxyCacheExpireAfterAccess(), cfg.encodedProxyCacheRefreshAfterWrite());
		this.delegate = delegate;
	}

	@Override
	public Entry<String, String> apply(Proxy input) throws IOException {
		return delegate.apply(input);
	}

	@Override
	public ProxyCodec<Proxy, Entry<String, String>> cached(long maximumSize, Duration expireAfterWrite,
			Duration expireAfterAccess, Duration refreshAfterWrite) {
		return delegate.cached(maximumSize, expireAfterWrite, expireAfterAccess, refreshAfterWrite);
	}

	public static void main(String[] args) throws IOException {
		var encoder = ServerProxyEncoder.getInstance();
		var encoded = encoder.apply(Proxy.builder(URI.create("http://localhost:8888")).build());
		System.out.println(encoded);
		var cfg = Configs.get(ProxyRoutingServerConfig.class);
		var json = RSAKeySerializer.INSTANCE.serialize(cfg.serverPrivateKey(), RSAPrivateKey.class,
				Serials.Gsons.serializationContext(Serials.Gsons.get()));
		System.out.println(json);
	}

}
