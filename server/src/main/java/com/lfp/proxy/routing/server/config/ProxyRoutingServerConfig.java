package com.lfp.proxy.routing.server.config;

import java.security.interfaces.RSAPrivateKey;
import java.time.Duration;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.properties.converter.DurationConverter;
import com.lfp.joe.properties.converter.JsonConverter;

public interface ProxyRoutingServerConfig extends Config {

	@ConverterClass(JsonConverter.class)
	RSAPrivateKey serverPrivateKey();

	@DefaultValue("1 day")
	@ConverterClass(DurationConverter.class)
	Duration encodedProxyTTL();

	@ConverterClass(DurationConverter.class)
	Duration encodedProxyCacheExpireAfterWrite();

	@DefaultValue("15 seconds")
	@ConverterClass(DurationConverter.class)
	Duration encodedProxyCacheExpireAfterAccess();

	@DefaultValue("5 seconds")
	@ConverterClass(DurationConverter.class)
	Duration encodedProxyCacheRefreshAfterWrite();

	@DefaultValue("100000")
	long encodedProxyCacheMaxiumumSize();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.json());
	}
}
