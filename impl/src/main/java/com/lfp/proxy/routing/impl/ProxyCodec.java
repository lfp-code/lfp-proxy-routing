package com.lfp.proxy.routing.impl;

import java.io.IOException;
import java.security.Signature;
import java.time.Duration;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.Validate;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.google.common.reflect.TypeToken;
import com.lfp.data.redis.client.codec.GsonCodec;
import com.lfp.data.redis.client.commands.SyncCommands;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.ssdb.client.SSDBClients;
import com.nimbusds.jose.jwk.RSAKey;

public interface ProxyCodec<X, Y> extends ThrowingFunction<X, Y, IOException> {

	ProxyCodec<X, Y> cached(long maximumSize, Duration expireAfterWrite, Duration expireAfterAccess,
			Duration refreshAfterWrite);

	public static abstract class Abs<X, Y> implements ProxyCodec<X, Y> {

		protected static Signature getSignature() {
			return Utils.Crypto.getSignatureMD5WithRSA();
		}

		private static final MemoizedSupplier<SyncCommands<String, Proxy>> SSDB_CLIENT_S = Utils.Functions
				.memoize(() -> {
					var codec = new GsonCodec<>(Serials.Gsons.get(), TypeToken.of(String.class),
							TypeToken.of(Proxy.class));
					return SSDBClients.createSync(codec);
				});

		private final RSAKey signingKey;
		private final RSAKey publicKey;

		public Abs(RSAKey signingKey, RSAKey publicKey) {
			super();
			Validate.isTrue(signingKey != null && signingKey.isPrivate(), "invalid signingKey:%s", signingKey);
			this.signingKey = signingKey;
			this.publicKey = Objects.requireNonNull(publicKey);
		}

		private Abs() {
			super();
			this.signingKey = null;
			this.publicKey = null;
		}

		@Override
		public Y apply(X input) throws IOException {
			if (input == null)
				return null;
			return applyInternal(input);
		}

		@Override
		public ProxyCodec<X, Y> cached(long maximumSize, Duration expireAfterWrite, Duration expireAfterAccess,
				Duration refreshAfterWrite) {
			Validate.isTrue(maximumSize >= -1);
			if (maximumSize == 0)
				return this;
			var self = this;
			Caffeine<CacheKey<X>, Optional<Y>> cbuilder = Caches.newCaffeineBuilder(maximumSize, expireAfterWrite,
					expireAfterAccess);
			cbuilder = cbuilder.executor(Threads.Pools.centralPool());
			if (refreshAfterWrite != null)
				cbuilder = cbuilder.refreshAfterWrite(refreshAfterWrite);
			var loadingCache = cbuilder.build(ck -> {
				Y result = self.apply(ck.request);
				return Optional.ofNullable(result);
			});
			return new ProxyCodec.Abs<X, Y>() {

				@Override
				public Y applyInternal(X input) throws IOException {
					var cacheKey = getCacheKey(input);
					if (cacheKey == null)
						return null;
					return loadingCache.get(new CacheKey<>(cacheKey, input)).orElse(null);
				}

				@Override
				protected RSAKey getSigningKey() {
					return self.getSigningKey();
				}

				@Override
				protected RSAKey getPublicKey() {
					return self.getPublicKey();
				}

				@Override
				protected Long getCacheKey(X input) {
					return self.getCacheKey(input);
				}
			};
		}

		protected RSAKey getSigningKey() {
			return signingKey;
		}

		protected RSAKey getPublicKey() {
			return publicKey;
		}

		protected SyncCommands<String, Proxy> getSSDBClient() {
			return SSDB_CLIENT_S.get();
		}

		protected abstract Y applyInternal(X input) throws IOException;

		protected abstract Long getCacheKey(X input);

		private static class CacheKey<X> {

			public final long key;

			public final X request;

			public CacheKey(long key, X request) {
				super();
				this.key = key;
				this.request = request;
			}

			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result + (int) (key ^ (key >>> 32));
				return result;
			}

			@Override
			public boolean equals(Object obj) {
				if (this == obj)
					return true;
				if (obj == null)
					return false;
				if (getClass() != obj.getClass())
					return false;
				CacheKey other = (CacheKey) obj;
				if (key != other.key)
					return false;
				return true;
			}

		}
	}

}
