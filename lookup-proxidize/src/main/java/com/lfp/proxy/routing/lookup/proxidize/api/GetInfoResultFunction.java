package com.lfp.proxy.routing.lookup.proxidize.api;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.common.reflect.TypeToken;
import com.google.gson.JsonArray;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.log.Logging;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.retry.Retrys;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.proxy.routing.lookup.proxidize.config.ProxidizeConfig;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.StreamEx;

public enum GetInfoResultFunction implements ThrowingSupplier<StreamEx<GetInfoResult>, IOException> {
	INSTANCE;

	@SuppressWarnings("serial")
	private static final TypeToken<List<GetInfoResult>> GET_INFO_TT = new TypeToken<List<GetInfoResult>>() {
	};
	private static final boolean LOG_RESPONSE_BODY = MachineConfig.isDeveloper() && true;
	private final org.slf4j.Logger logger = Logging.logger();

	private final Cache<Integer, GetInfoResult> proxyInfoFallbackCache = Caches
			.newCaffeineBuilder(-1, Duration.ofMinutes(5), null).build();

	private final LoadingCache<Nada, List<GetInfoResult>> proxyInfoFreshCache = Caches
			.newCaffeineBuilder(-1, Duration.ofMillis(500), null).build(nil -> {
				return Retrys.execute(20, Duration.ofSeconds(1), attempt -> {
					var proxyInfos = APIFunction.INSTANCE.get(Arrays.asList("getinfo"), null, is -> {
						return deserializeGetInfoResults(attempt, is);
					});
					for (var proxyInfo : proxyInfos) {
						var current = proxyInfoFallbackCache.getIfPresent(proxyInfo.getIndex());
						boolean putValue;
						if (current == null)
							putValue = true;
						else if (Utils.Strings.isNotBlank(proxyInfo.getImei()))
							putValue = true;
						else if (Utils.Strings.isBlank(current.getImei()))
							putValue = true;
						else
							putValue = false;
						if (putValue)
							proxyInfoFallbackCache.put(proxyInfo.getIndex(), proxyInfo);
					}
					return proxyInfos;
				});
			});

	@Override
	public StreamEx<GetInfoResult> get() throws IOException {
		return get(false);
	}

	public StreamEx<GetInfoResult> get(boolean enableAllCarriers) throws IOException {
		StreamEx<GetInfoResult> proxyInfoStream = StreamEx.empty();
		{
			var append = Utils.Lots.stream(proxyInfoFreshCache.get(Nada.get()));
			proxyInfoStream = proxyInfoStream.append(append);
		}
		{
			var append = Utils.Lots.stream(this.proxyInfoFallbackCache.asMap()).values();
			proxyInfoStream = proxyInfoStream.append(append);
		}
		proxyInfoStream = normalizeStream(enableAllCarriers, proxyInfoStream);
		return proxyInfoStream;
	}

	private List<GetInfoResult> deserializeGetInfoResults(int attempt, InputStream is) throws IOException {
		if (attempt > 0 && LOG_RESPONSE_BODY) {
			Bytes body;
			try {
				body = Utils.Bits.from(is);
			} finally {
				is.close();
			}
			logger.info("body:{}", body.encodeUtf8());
			is = body.inputStream();
		}
		var jarr = Serials.Gsons.fromStream(is, JsonArray.class);
		for (var je : jarr) {
			var jo = je.getAsJsonObject();
			for (var key : List.copyOf(jo.keySet())) {
				var value = Serials.Gsons.tryGetAsString(jo.get(key)).orElse(null);
				if (value == null)
					continue;
				if (!Utils.Strings.isBlank(value))
					continue;
				jo.remove(key);
			}
		}
		List<GetInfoResult> result = Serials.Gsons.get().fromJson(jarr, GET_INFO_TT.getType());
		return normalizeStream(true, result).toImmutableList();
	}

	private StreamEx<GetInfoResult> normalizeStream(boolean enableAllCarriers, Iterable<? extends GetInfoResult> ible) {
		StreamEx<GetInfoResult> stream = Utils.Lots.stream(ible).map(v -> v);
		stream = stream.nonNull();
		// has index
		stream = stream.filter(v -> v.getIndex() != null && v.getIndex() >= 0);
		// has proxy
		stream = stream.filter(v -> v.streamProxies().iterator().hasNext());
		// disabled carries
		final Set<String> disabledCarriers;
		if (enableAllCarriers)
			disabledCarriers = Set.of();
		else
			disabledCarriers = Utils.Lots.stream(Configs.get(ProxidizeConfig.class).disabledCarriers())
					.mapPartial(Utils.Strings::trimToNullOptional).toSet();
		if (!disabledCarriers.isEmpty())
			stream = stream.filter(proxyInfo -> {
				var carrier = Utils.Strings.trimToNull(proxyInfo.getCarrier());
				if (carrier != null && Utils.Lots.stream(disabledCarriers).anyMatch(carrier::equalsIgnoreCase))
					return false;
				return true;
			});

		{// sort and remove dupe imeis
			Comparator<GetInfoResult> sorter;
			{
				sorter = Utils.Lots.comparatorNoOp();
				sorter = sorter.thenComparing(v -> v.getIndex());
				sorter = sorter.thenComparing(v -> {
					if (Utils.Strings.isBlank(v.getImei()))
						return 1;
					return 0;
				});
			}
			stream = stream.sorted(sorter);
			stream = stream.distinct(v -> v.getIndex());
		}
		return stream;
	}

	public static void main(String[] args) throws InterruptedException, IOException {
		for (int i = 0;; i++) {
			if (i > 0)
				Thread.sleep(Duration.ofSeconds(3).toMillis());
			var stream = GetInfoResultFunction.INSTANCE.get();
			System.out.println(stream.count());
			var carriers = GetInfoResultFunction.INSTANCE.get().map(v -> v.getCarrier()).distinct().sorted().toList();
			System.out.println(carriers);
			var indexes = GetInfoResultFunction.INSTANCE.get().map(v -> v.getIndex()).sorted().toList();
			System.out.println(indexes);
			var imeis = GetInfoResultFunction.INSTANCE.get().map(v -> v.getImei()).nonNull().sorted().toList();
			System.out.println(imeis);
		}
	}
}
