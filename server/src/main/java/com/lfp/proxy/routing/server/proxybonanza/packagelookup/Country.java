
package com.lfp.proxy.routing.server.proxybonanza.packagelookup;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Country {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("isocode")
    @Expose
    private String isocode;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("flag_image")
    @Expose
    private String flagImage;
    @SerializedName("continent")
    @Expose
    private String continent;
    @SerializedName("eunion")
    @Expose
    private Boolean eunion;
    @SerializedName("vat_rate")
    @Expose
    private Object vatRate;

    /**
     * 
     * @return
     *     The id
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The isocode
     */
    public String getIsocode() {
        return isocode;
    }

    /**
     * 
     * @param isocode
     *     The isocode
     */
    public void setIsocode(String isocode) {
        this.isocode = isocode;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The flagImage
     */
    public String getFlagImage() {
        return flagImage;
    }

    /**
     * 
     * @param flagImage
     *     The flag_image
     */
    public void setFlagImage(String flagImage) {
        this.flagImage = flagImage;
    }

    /**
     * 
     * @return
     *     The continent
     */
    public String getContinent() {
        return continent;
    }

    /**
     * 
     * @param continent
     *     The continent
     */
    public void setContinent(String continent) {
        this.continent = continent;
    }

    /**
     * 
     * @return
     *     The eunion
     */
    public Boolean getEunion() {
        return eunion;
    }

    /**
     * 
     * @param eunion
     *     The eunion
     */
    public void setEunion(Boolean eunion) {
        this.eunion = eunion;
    }

    /**
     * 
     * @return
     *     The vatRate
     */
    public Object getVatRate() {
        return vatRate;
    }

    /**
     * 
     * @param vatRate
     *     The vat_rate
     */
    public void setVatRate(Object vatRate) {
        this.vatRate = vatRate;
    }

}
