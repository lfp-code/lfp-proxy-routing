package test;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.serial.Serials;
import com.lfp.proxy.routing.client.ProxyRoutingClient;
import com.lfp.proxy.routing.service.ProxyAttributes;
import com.lfp.proxy.routing.service.ProxyAttributes.ServerType;
import com.neovisionaries.i18n.CountryCode;

public class LocationTest {

	public static void main(String[] args) throws IOException {
		var pab = ProxyAttributes.builder();
		pab.countryCode(CountryCode.US);
		pab.serverTypes(ServerType.MOBILE);
		Set<String> ips = new HashSet<>();
		for (int i = 0; i < 25; i++) {
			var proxy = ProxyRoutingClient.get().lookupProxyBlocking(pab.build());
			if (proxy != null) {
				ips.add(proxy.getPublicIPAddress().get());
				var info = IPs.getIPAddressInfo(proxy.getPublicIPAddress().get());
				System.out.println(Serials.Gsons.getPretty().toJson(info.get()));
			}
			System.out.println(Serials.Gsons.getPretty().toJson(proxy));

		}
		System.out.println(ips.size());
	}
}
