package com.lfp.proxy.routing.server.smartproxy.config;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;

public interface SmartProxyConfig extends Config {

	String proxyUsername();

	String proxyPassword();

	String apiUsername();

	String apiPassword();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.builder().withPrependClassNames(false).withJsonOutput(true).build());
	}
}
