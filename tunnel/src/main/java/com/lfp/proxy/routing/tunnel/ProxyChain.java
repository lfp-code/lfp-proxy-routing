package com.lfp.proxy.routing.tunnel;

import java.io.File;
import java.net.InetSocketAddress;
import java.security.interfaces.RSAKey;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.threadly.concurrent.future.ListenableFuture;

import com.google.gson.Gson;
import com.lfp.code.bridge.nodejs.NodeJSEvalService;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.impl.config.ProxyRoutingImplConfig;
import com.lfp.proxy.routing.tunnel.config.ProxyRoutingTunnelConfig;

public class ProxyChain extends NodeJSEvalService {

	public static final ProxyChain INSTANCE = new ProxyChain();

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	protected ProxyChain() {
		super("jose", "proxy-chain", "cached", "bs58");
	}

	public ListenableFuture<Void> start(InetSocketAddress address) {
		var gson = Serials.Gsons.get();
		ProxyRoutingTunnelConfig cfg = Configs.get(ProxyRoutingTunnelConfig.class);
		Map<String, Object> tokens = new HashMap<>();
		{
			tokens.put("PORT", address.getPort());
			tokens.put("VERBOSE_LOGGING", MachineConfig.isDeveloper());
			tokens.put("ALLOW_EMPTY_AUTHENTICATION", MachineConfig.isDeveloper());
			tokens.put("TUNNEL_PRIVATE_KEY_JSON", toJson(gson, cfg.tunnelPrivateKey()));
			tokens.put("SERVER_PUBLIC_KEY_JSON",
					toJson(gson, Configs.get(ProxyRoutingImplConfig.class).serverPublicKey()));
		}
		String template;
		if (MachineConfig.isDeveloper())
			template = Throws.unchecked(() -> Utils.Files.readFileToString(
					new File("src/main/resources/com/lfp/proxy/routing/tunnel/ProxyChain.template.js")));
		else
			template = Utils.Resources.getResourceAsString("ProxyChain.template.js").get();
		var code = Utils.Strings.templateApply(template, tokens);
		logger.info("proxy chain scheduled:{}", address);
		var future = Threads.Pools.centralPool().submit(() -> {
			logger.info("proxy chain starting:{}", address);
			var result = this.eval(code, Void.class).block();
			return result;
		});
		Threads.Futures.callback(future, (v, t) -> {
			if (Utils.Exceptions.isCancelException(t))
				return;
			logger.error("proxy chain ended:{}", address);
		});
		return future;
	}

	private static String toJson(Gson gson, RSAKey rsaKey) {
		Objects.requireNonNull(rsaKey);
		return gson.toJson(rsaKey);
	}

	public static void main(String[] args) throws InterruptedException {
		var pc = new ProxyChain();
		pc.start(new InetSocketAddress("localhost", 3000));
		Thread.currentThread().join();
	}
}
