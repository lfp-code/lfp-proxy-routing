package com.lfp.proxy.routing.lookup;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

import com.lfp.joe.net.dns.DNSs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Hashable;
import com.lfp.proxy.routing.service.ProxyExt;

import org.jsoup.helper.Validate;

import at.favre.lib.bytes.Bytes;

public abstract class ProxySupplier implements Supplier<ProxyExt>, Hashable {

	public static ProxySupplier create(Proxy.Type type, String hostname, String username, String password,
			int portStart, int portEnd, String publicIPAddress) {
		Objects.requireNonNull(type);
		Validate.isTrue(Utils.Strings.isNotBlank(hostname));
		Validate.isTrue(portStart > 0);
		Validate.isTrue(portEnd > 0);
		Validate.isTrue(portEnd >= portStart);
		Bytes hash = Utils.Crypto.hashMD5(type, hostname, username, password, portStart, portEnd);
		return new ProxySupplier(hash) {

			@Override
			public ProxyExt get() {
				int port;
				if (portStart == portEnd)
					port = portStart;
				else
					port = Utils.Crypto.getRandomInclusive(portStart, portEnd);
				var proxyExtB = ProxyExt.builder();
				proxyExtB.type(type);
				proxyExtB.hostname(hostname);
				proxyExtB.port(port);
				proxyExtB.username(username);
				proxyExtB.password(password);
				proxyExtB.publicIPAddress(publicIPAddress);
				return proxyExtB.build();
			}
		};
	}

	public static ProxySupplier create(ProxyExt proxy) {
		Objects.requireNonNull(proxy);
		Proxy hashProxy = proxy;
		Optional<String> ipOp = DNSs.parseIPAddressFromWildcardDNSHostname(proxy.getHostname());
		if (ipOp.isPresent())
			hashProxy = hashProxy.toBuilder().hostname(ipOp.get()).build();
		return new ProxySupplier(hashProxy.hash()) {

			@Override
			public ProxyExt get() {
				return proxy;
			}

		};
	}

	private final Bytes hash;

	public ProxySupplier(Bytes hash) {
		this.hash = Objects.requireNonNull(hash);
	}

	@Override
	public Bytes hash() {
		return hash;
	}

	@Override
	public int hashCode() {
		return Objects.hash(uuid());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProxySupplier other = (ProxySupplier) obj;
		return Objects.equals(uuid(), other.uuid());
	}

}
