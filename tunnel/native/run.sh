set -e

source app.env

ulimit -n 1048576
sysctl -p

curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash -
apt update
apt install -y nodejs
PACKAGES=()
PACKAGES+=('default-jre')
PACKAGES+=('glances')
PACKAGES+=('nodejs')
for package in "${PACKAGES[@]}"
do
    dpkg -s "$package" >/dev/null 2>&1 && {
        echo "$package is installed."
    } || {
        apt install -y $package
    }
done

rm -f $app_jar_name
curl "$app_jar_url" -o ./$app_jar_name

java -jar \
--add-opens java.base/jdk.internal.loader=ALL-UNNAMED \
--add-opens java.base/jdk.internal.misc=ALL-UNNAMED \
-Dcom.lfp.joe.vault.VaultConfig_address=$VaultConfig_address \
-Dcom.lfp.joe.vault.VaultConfig_username=$VaultConfig_username \
-Dcom.lfp.joe.vault.VaultConfig_password=$VaultConfig_password \
-Dcom.lfp.proxy.routing.tunnel.config.ProxyRoutingTunnelConfig_proxyExecutable=/usr/bin/proxy \
-Xms1024m \
-Xmx4096m \
app*.jar