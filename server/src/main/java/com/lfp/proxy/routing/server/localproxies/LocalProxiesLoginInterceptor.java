package com.lfp.proxy.routing.server.localproxies;

import java.io.IOException;
import java.net.URI;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.net.dns.TLDs;
import com.lfp.joe.net.html.Jsoups;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.utils.Utils;

import de.xn__ho_hia.storage_unit.StorageUnits;
import okhttp3.Interceptor;
import okhttp3.Response;

public class LocalProxiesLoginInterceptor implements Interceptor {
	private static final String DOMAIN_NAME = "localproxies.com";
	private final String username;
	private final String password;

	public LocalProxiesLoginInterceptor(String username, String password) {
		this.username = Validate.notBlank(username);
		this.password = Validate.notBlank(password);
	}

	@Override
	public Response intercept(Chain chain) throws IOException {
		var request = chain.request();
		var response = chain.proceed(request);
		if (!isLocalProxiesURI(response.request().url().uri()))
			return response;
		if (!Ok.Calls.contentTypeMatches(response, null, "html", null))
			return response;
		var formData = Ok.Calls.peekDocument(response, StorageUnits.megabyte(1).longValue(), doc -> {
			var form = Jsoups.select(doc, "form#am-login-form").findFirst().orElse(null);
			if (form == null)
				return null;
			return Jsoups.parseFormData(form);
		});
		if (formData == null)
			return response;
		Ok.Calls.close(response);
		formData.set("amember_login", this.username);
		formData.set("amember_pass", this.password);
		var loginResponse = chain.proceed(Ok.Calls.toRequest(formData));
		return loginResponse;
	}

	private boolean isLocalProxiesURI(URI uri) {
		if (uri == null)
			return false;
		if (!Utils.Strings.containsIgnoreCase(uri.getHost(), DOMAIN_NAME))
			return false;
		var domainName = TLDs.parseDomainName(uri);
		return Utils.Strings.equalsIgnoreCase(domainName, DOMAIN_NAME);
	}

}
