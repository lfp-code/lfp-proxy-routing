const jose = require('jose')
	const ProxyChain = require('proxy-chain');
const cached = require('cached');
const crypto = require('crypto')
	const assert = require('assert');
const bs58 = require('bs58')

function isBlank(value) {
	if (typeof value === 'undefined')
		return true;
	if (value)
		return false;
	return true;
}

function equalsIgnoreCase(v1, v2) {
	if (!v1 && !v2)
		return true;
	return (v1 + '').toUpperCase() == (v2 + '').toUpperCase()
}

function toJson(obj) {
	var cache = [];
	var json = JSON.stringify(obj, (key, value) => {
			if (typeof value === 'object' && value !== null) {
				if (cache.includes(value))
					return
					cache.push(value);
			}
			return value;
		});
	cache = null;
	return json;
}

const REQUEST_AUTHENTICATION_RESPONSE = {
	requestAuthentication: true
};
const PORT = Number('{{{PORT}}}')
	const VERBOSE_LOGGING = equalsIgnoreCase(true, '{{{VERBOSE_LOGGING}}}')
	const ALLOW_EMPTY_AUTHENTICATION = equalsIgnoreCase(true, '{{{ALLOW_EMPTY_AUTHENTICATION}}}')
	const TUNNEL_PRIVATE_KEY = jose.importJWK(JSON.parse('{{{TUNNEL_PRIVATE_KEY_JSON}}}'), 'RSA-OAEP-256')
	const SERVER_PUBLIC_KEY = jose.importJWK(JSON.parse('{{{SERVER_PUBLIC_KEY_JSON}}}'), 'RSA-OAEP-256')
	const UPSTREAM_CACHE_TTL_SECONDS = isBlank('{{{UPSTREAM_CACHE_TTL_SECONDS}}}') ? 5 : Number('{{{UPSTREAM_CACHE_TTL_SECONDS}}}');
const UPSTREAM_CACHE = cached('UPSTREAM_CACHE', {
		backend: 'memory',
		defaults: {
			freshFor: UPSTREAM_CACHE_TTL_SECONDS
		}
	});

async function getUpstream(token) {
	let hash = crypto.createHash('md5').update(token).digest('hex')
		return UPSTREAM_CACHE.getOrElse(hash, function () {
			if (VERBOSE_LOGGING)
				console.log(`cache miss. hash:${hash} token:${token}`);
			return getUpstreamFresh(token).catch((error) => {
				if (VERBOSE_LOGGING)
					console.error(`token authorization failed. hash:${hash} token:${token}`, error);
				return REQUEST_AUTHENTICATION_RESPONSE
			});
		});
}

async function getUpstreamFresh(token) {
	var jweString = ''
		for (var part of token.split('-')) {
			if (jweString.length > 0)
				jweString += '.';
			const bytes = bs58.decode(part);
			var decodedBase64 = Buffer.from(bytes).toString('base64');
			var decodedBase64Url = decodedBase64.replace('+', '-').replace('/', '_').replace(/=+$/, '');
			jweString += decodedBase64Url;
		}
		if (VERBOSE_LOGGING)
			console.log(`decoding jwe:${jweString}`);
		const {
		plaintext
	} = await jose.compactDecrypt(jweString, (await TUNNEL_PRIVATE_KEY))
		const {
		payload
	} = await jose.compactVerify(plaintext, (await SERVER_PUBLIC_KEY))
		var payloadData = JSON.parse(payload)
		if (VERBOSE_LOGGING)
			console.log('payloadData', payloadData);
		var expiration = payloadData.exp
		if (expiration != null) {
			var ttl = (expiration * 1000) - new Date().getTime();
			assert(ttl >= 0, 'expired JWT')
		}
		var upstreamProxyUrl = `${payloadData.hostname}:${payloadData.port}`
		var username = payloadData.username;
	var password = payloadData.password;
	if (!isBlank(username) || !isBlank(password)) {
		username = !username ? '' : username;
		password = !password ? '' : password;
		upstreamProxyUrl = `${username}:${password}@${upstreamProxyUrl}`;
	}
	var type = payloadData.type;
	type = !type ? '' : type;
	type = type.replace('_', '');
	upstreamProxyUrl = `${type.toLowerCase()}://${upstreamProxyUrl}`;
	if (VERBOSE_LOGGING)
		console.log('upstreamProxyUrl', upstreamProxyUrl);
	return {
		upstreamProxyUrl: upstreamProxyUrl
	};
}

const proxyChain = new ProxyChain.Server({
		// Port where the server will listen. By default 8000.
		port: PORT,

		// Enables verbose logging
		verbose: VERBOSE_LOGGING,

		// Custom function to authenticate proxy requests and provide the URL to chained upstream proxy.
		// It must return an object (or promise resolving to the object) with the following form:
		// { requestAuthentication: Boolean, upstreamProxyUrl: String }
		// If the function is not defined or is null, the server runs in simple mode.
		// Note that the function takes a single argument with the following properties:
		// * request      - An instance of http.IncomingMessage class with information about the client request
		//                  (which is either HTTP CONNECT for SSL protocol, or other HTTP request)
		// * username     - Username parsed from the Proxy-Authorization header. Might be empty string.
		// * password     - Password parsed from the Proxy-Authorization header. Might be empty string.
		// * hostname     - Hostname of the target server
		// * port         - Port of the target server
		// * isHttp       - If true, this is a HTTP request, otherwise it's a HTTP CONNECT tunnel for SSL
		//                  or other protocols
		// * connectionId - Unique ID of the HTTP connection. It can be used to obtain traffic statistics.
		prepareRequestFunction: ({
			request,
			username,
			password,
			hostname,
			port,
			isHttp,
			connectionId
		}) => {
			if (VERBOSE_LOGGING)
				console.log('request', toJson(request));
			var token = ''
				if (!!username)
					token += username;
				if (!!password)
					token += password;
				token = token.trim();
			if (!ALLOW_EMPTY_AUTHENTICATION && isBlank(token))
				return REQUEST_AUTHENTICATION_RESPONSE;
			if (isBlank(token))
				return {};
			return getUpstream(token);
		},
	});

// Emitted when HTTP connection is closed
proxyChain.on('connectionClosed', (connectionId) => {
	if (VERBOSE_LOGGING)
		console.log(`Connection ${connectionId} closed`);
});

// Emitted when HTTP request fails
proxyChain.on('requestFailed', (request, error) => {
	console.error(`Request ${request.url} failed`, error);
});

return new Promise((resolve, reject) => {
	proxyChain.listen().then(() => {
		console.log(`Proxy server is running on port ${proxyChain.port}`);
		setInterval(() => {
			console.log(`Stats: ${JSON.stringify(proxyChain.stats)}`);
		}, 30000);
	}).catch(reject);
	proxyChain.server.on('close', resolve)
});
