package test;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;

import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.lookup.ProxyRegistration;
import com.lfp.proxy.routing.lookup.proxidize.ProxidizeLookupService;

public class ProxidizeTest {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws IOException {
		var proxyRegistrations = ProxidizeLookupService.INSTANCE.lookup().toList();
		Collections.shuffle(proxyRegistrations);
		Proxy proxy = Utils.Lots.stream(proxyRegistrations).map(ProxyRegistration::getProxy)
				.filter(v -> Proxy.Type.HTTP.equals(v.getType())).findFirst().orElse(null);
		Objects.requireNonNull(proxy);
		System.out.println(Serials.Gsons.getPretty().toJson(proxy));
		var client = HttpClients.builder(proxy).build();
		Date startedAt = new Date();
		for (int i = 0;; i++) {
			if (i > 0)
				Threads.sleepUnchecked(Duration.ofSeconds(1));
			try {
				var request = HttpRequests.request().uri(URI.create("http://icanhazip.com/")).build();
				var body = client.send(request, BodyHandlers.ofString()).body();
				body = Utils.Strings.trimToNull(body);
				System.out.println(String.format("%s - %s - %s", i,
						Duration.ofMillis(System.currentTimeMillis() - startedAt.getTime()).toSeconds(), body));
			} catch (Exception e) {
				logger.warn("proxy error:{}", Serials.Gsons.get().toJson(proxy), e);
			}

		}
	}

}
