package com.lfp.proxy.routing.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.ReadWriteAccessor;

public class Registry<X> {
	private final ReadWriteAccessor<List<X>> readWriteAccessor = new ReadWriteAccessor<>(new ArrayList<>());

	public boolean addIf(Predicate<X> valuePredicate, X value) {
		Objects.requireNonNull(value);
		Objects.requireNonNull(valuePredicate);
		return readWriteAccessor.writeAccess(list -> {
			for (X check : list)
				if (!valuePredicate.test(check))
					return false;
			return list.add(value);
		});
	}

	public boolean add(X value) {
		return addIf(v -> {
			if (Objects.equals(v, value))
				return false;
			return true;
		}, value);
	}

	public List<X> removeIf(Predicate<X> valuePredicate) {
		Objects.requireNonNull(valuePredicate);
		return readWriteAccessor.writeAccess(list -> {
			List<X> result = new ArrayList<>();
			list.removeIf(v -> {
				if (!valuePredicate.test(v))
					return false;
				result.add(v);
				return true;
			});
			return Collections.unmodifiableList(result);
		});
	}

	public Optional<X> nextIf(Predicate<X> valuePredicate) {
		Objects.requireNonNull(valuePredicate);
		return readWriteAccessor.writeAccess(list -> {
			var removeAt = -1;
			for (int i = 0; removeAt < 0 && i < list.size(); i++)
				if (valuePredicate.test(list.get(i)))
					removeAt = i;
			if (removeAt < 0)
				return Optional.empty();
			var result = list.remove(removeAt);
			list.add(result);
			return Optional.of(result);
		});

	}

	public Optional<X> next() {
		return nextIf(v -> true);
	}

	public Optional<X> getIf(Predicate<X> valuePredicate) {
		return readWriteAccessor.readAccess(list -> {
			for (X check : list)
				if (valuePredicate.test(check))
					return Optional.of(check);
			return Optional.empty();
		});

	}

	public List<X> getAll() {
		return getAll(v -> true);
	}

	public List<X> getAll(Predicate<X> valuePredicate) {
		return readWriteAccessor.readAccess(list -> {
			return Utils.Lots.stream(list).filter(valuePredicate).toImmutableList();
		});
	}

}
