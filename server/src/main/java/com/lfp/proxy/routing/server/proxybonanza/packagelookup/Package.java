
package com.lfp.proxy.routing.server.proxybonanza.packagelookup;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Package {

	@SerializedName("parent_id")
	@Expose
	private Long parentId;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("bandwidth")
	@Expose
	private Long bandwidth;
	@SerializedName("howmany_ips")
	@Expose
	private Long howmanyIps;
	@SerializedName("package_type")
	@Expose
	private String packageType;
	@SerializedName("created")
	@Expose
	private Object created;
	@SerializedName("modified")
	@Expose
	private Object modified;

	/**
	 * 
	 * @return The parentId
	 */
	public Long getParentId() {
		return parentId;
	}

	/**
	 * 
	 * @param parentId The parent_id
	 */
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	/**
	 * 
	 * @return The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name The name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return The bandwidth
	 */
	public Long getBandwidth() {
		return bandwidth;
	}

	/**
	 * 
	 * @param bandwidth The bandwidth
	 */
	public void setBandwidth(Long bandwidth) {
		this.bandwidth = bandwidth;
	}

	/**
	 * 
	 * @return The howmanyIps
	 */
	public Long getHowmanyIps() {
		return howmanyIps;
	}

	/**
	 * 
	 * @param howmanyIps The howmany_ips
	 */
	public void setHowmanyIps(Long howmanyIps) {
		this.howmanyIps = howmanyIps;
	}

	/**
	 * 
	 * @return The packageType
	 */
	public String getPackageType() {
		return packageType;
	}

	/**
	 * 
	 * @param packageType The package_type
	 */
	public void setPackageType(String packageType) {
		this.packageType = packageType;
	}

	/**
	 * 
	 * @return The created
	 */
	public Object getCreated() {
		return created;
	}

	/**
	 * 
	 * @param created The created
	 */
	public void setCreated(Object created) {
		this.created = created;
	}

	/**
	 * 
	 * @return The modified
	 */
	public Object getModified() {
		return modified;
	}

	/**
	 * 
	 * @param modified The modified
	 */
	public void setModified(Object modified) {
		this.modified = modified;
	}

}
