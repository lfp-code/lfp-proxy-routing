
package com.lfp.proxy.routing.lookup.airproxy;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class ListResponse {

    @SerializedName("proxies")
    @Expose
    private List<ProxyResult> proxies = null;
    @SerializedName("count")
    @Expose
    private Long count;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public List<ProxyResult> getProxies() {
        return proxies;
    }

    public void setProxies(List<ProxyResult> proxies) {
        this.proxies = proxies;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

}
