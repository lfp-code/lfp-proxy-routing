package com.lfp.proxy.routing.lookup;

import java.io.IOException;

import com.lfp.proxy.routing.service.ProxyAttributes;

import one.util.streamex.StreamEx;

public interface LookupService {

	// multiple proxies should be considered "synonyms"
	StreamEx<ProxyRegistration> lookup() throws IOException;

	default boolean skipValidation() {
		return false;
	}

	public static interface IPAuthorization extends LookupService {

		void authorizeIPs(String... ipAddresses) throws IOException;
	}

}
