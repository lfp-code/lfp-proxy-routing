package test;

import java.io.IOException;
import java.time.Duration;
import java.util.Collections;

import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.threads.Threads;
import com.lfp.proxy.routing.lookup.proxidize.ProxidizeLookupService;

public class RecoverTest {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws IOException {
		var prs = ProxidizeLookupService.INSTANCE.lookup().toList();
		Collections.shuffle(prs);
		var proxy = prs.get(0).getProxy();
		var proxySummary = String.format("%s:%s", proxy.getHostname(), proxy.getPort());
		var client = Ok.Clients.get(prs.get(0).getProxy());
		for (int i = 0;; i++) {
			if (i > 0)
				Threads.sleepUnchecked(Duration.ofSeconds(1));
			try {
				logger.info("success:{} proxy:{} ipAddress:{}", i, proxySummary, Ok.Clients.getIPAddress(client));
			} catch (Exception e) {
				logger.warn("failure:{} proxy:{}", i, proxySummary, e);
			}
		}
	}

}
