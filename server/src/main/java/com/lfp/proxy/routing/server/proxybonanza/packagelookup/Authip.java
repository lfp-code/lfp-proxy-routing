
package com.lfp.proxy.routing.server.proxybonanza.packagelookup;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Authip {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("ip")
    @Expose
    private String ip;
    @SerializedName("userpackage_id")
    @Expose
    private Long userpackageId;

    /**
     * 
     * @return
     *     The id
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * 
     * @param ip
     *     The ip
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * 
     * @return
     *     The userpackageId
     */
    public Long getUserpackageId() {
        return userpackageId;
    }

    /**
     * 
     * @param userpackageId
     *     The userpackage_id
     */
    public void setUserpackageId(Long userpackageId) {
        this.userpackageId = userpackageId;
    }

}
