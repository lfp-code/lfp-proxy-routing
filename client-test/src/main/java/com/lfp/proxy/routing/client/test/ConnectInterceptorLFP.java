package com.lfp.proxy.routing.client.test;

import java.io.IOException;
import java.util.Objects;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.internal.connection.ConnectInterceptor;

public class ConnectInterceptorLFP implements Interceptor {

	private final ConnectInterceptor connectInterceptor;

	public ConnectInterceptorLFP(ConnectInterceptor connectInterceptor) {
		this.connectInterceptor = Objects.requireNonNull(connectInterceptor);
	}

	@Override
	public Response intercept(Chain chain) throws IOException {
		return connectInterceptor.intercept(chain);
	}

}
