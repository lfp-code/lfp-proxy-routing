package test;

import java.io.IOException;
import java.util.Map;

import com.lfp.joe.serial.Serials;
import com.lfp.proxy.routing.client.ProxyRoutingClient;
import com.lfp.proxy.routing.service.ProxyAttributes;
import com.lfp.proxy.routing.service.ProxyAttributes.ServerType;

import com.neovisionaries.i18n.CountryCode;

public class PrintProxy {
	public static void main(String[] args) throws IOException {
		var pab = ProxyAttributes.builder();
		pab.countryCode(CountryCode.DE);
		var pa = pab.build();
		for (int i = 0; i < 1; i++) {
			var proxy = ProxyRoutingClient.get().lookupProxyBlocking(pa);
			System.out.println(Serials.Gsons.getPretty().toJson(proxy));
			// System.out.println(Serials.Gsons.getPretty().toJson(Map.of("http",
			// proxy.toURI(), "https", proxy.toURI())));
			// try (var rctx = Ok.Calls.execute(Ok.Clients.get(proxy),
			// "https://api.ipify.org")) {
			// System.out.println(rctx.parseText().getParsedBody());
			// }
		}
	}
}
