package test;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse.BodyHandlers;

import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.serial.Serials;
import com.lfp.proxy.routing.client.ProxyRoutingClient;

public class ConnectTest {

	public static void main(String[] args) throws IOException, InterruptedException {
		var proxy = ProxyRoutingClient.get().lookupProxyBlocking();
		System.out.println(Serials.Gsons.getPretty().toJson(proxy));
		var client = HttpClients.newClient(proxy);
		var resp = client.send(HttpRequest.newBuilder().uri(URI.create("https://api.ipify.org")).build(),
				BodyHandlers.ofString());
		System.out.println(resp.body());
	}
}
