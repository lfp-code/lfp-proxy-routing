package com.lfp.proxy.routing.impl.config;

import java.security.interfaces.RSAPublicKey;
import java.time.Duration;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.properties.converter.DurationConverter;
import com.lfp.joe.properties.converter.JsonConverter;

public interface ProxyRoutingImplConfig extends Config {

	@DefaultValue("1 day")
	@ConverterClass(DurationConverter.class)
	Duration ssdbProxyCacheTTL();

	@ConverterClass(JsonConverter.class)
	RSAPublicKey serverPublicKey();

	@ConverterClass(JsonConverter.class)
	RSAPublicKey tunnelPublicKey();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.json());
	}
}
