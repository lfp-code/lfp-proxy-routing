package com.lfp.proxy.routing.server.proxybonanza.authips;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Datum {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("userpackage_id")
    @Expose
    private Long userpackageId;
    @SerializedName("ip")
    @Expose
    private String ip;

    /**
     * 
     * @return
     *     The id
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The userpackageId
     */
    public Long getUserpackageId() {
        return userpackageId;
    }

    /**
     * 
     * @param userpackageId
     *     The userpackage_id
     */
    public void setUserpackageId(Long userpackageId) {
        this.userpackageId = userpackageId;
    }

    /**
     * 
     * @return
     *     The ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * 
     * @param ip
     *     The ip
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

}
