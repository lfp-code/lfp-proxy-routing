package com.lfp.proxy.routing.tunnel;

import java.time.Duration;
import java.util.Objects;

import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.net.proxy.Proxy.Type;
import com.lfp.joe.threads.ImmutableFutures.ScheduleWhileRequest;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.impl.TunnelAddress;
import com.lfp.proxy.routing.tunnel.config.ProxyRoutingTunnelConfig;

import reactor.core.publisher.FluxSink;

public class TunnelAddressPublisher {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final Duration RUN_INTERVAL = Duration.ofSeconds(10);
	private Type proxyType;
	private int proxyPort;
	private final FluxSink<TunnelAddress> fluxSink;
	private String currentIPAddress;

	public TunnelAddressPublisher(Proxy.Type proxyType, int proxyPort, FluxSink<TunnelAddress> fluxSink) {
		super();
		this.proxyType = Objects.requireNonNull(proxyType);
		this.proxyPort = proxyPort;
		this.fluxSink = Objects.requireNonNull(fluxSink);

	}

	public ListenableFuture<Void> start() {
		var future = ScheduleWhileRequest.builder(RUN_INTERVAL, () -> {
			try {
				run();
				return true;
			} catch (Throwable t0) {
				logger.warn("error during tunnel address publish", t0);
				try {
					fluxSink.error(t0);
				} catch (Throwable t1) {
					logger.warn("error during error publish", t1);
				}
			}
			return false;
		}, loop -> loop).build().schedule();
		Threads.Futures.logFailureError(future, true, logger, "error during tunnel address schedule");
		Threads.Futures.callback(future, this.fluxSink::complete);
		return future.map(v -> null);
	}

	private void run() {
		ProxyRoutingTunnelConfig proxyRoutingTunnelConfig = Configs.get(ProxyRoutingTunnelConfig.class);
		String ipAddress = IPs
				.getIPAddress(Duration.ofMillis(proxyRoutingTunnelConfig.machineIPAddressCacheDurationMillis()));
		if (Utils.Strings.equals(currentIPAddress, ipAddress)) {
			if (MachineConfig.isDeveloper())
				logger.info("skipping tunnel address update");
			return;
		}
		this.currentIPAddress = ipAddress;
		var proxy = Proxy.builder().type(proxyType).hostname(ipAddress).port(proxyPort).build();
		TunnelAddress tunnelAddress = TunnelAddress.builder().proxy(proxy).machineIPAddress(ipAddress).build();
		this.fluxSink.next(tunnelAddress);
	}
}
