package test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogTest {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final Logger logger = LogManager.getLogger(THIS_CLASS);

	public static void main(String[] args) {
		logger.info("hi\nwow\ncool");
		System.err.println("done");
	}

}
