package com.lfp.proxy.routing.server.hydraproxy.config;

import java.util.List;
import java.util.Map;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.properties.converter.JsonConverter;

public interface HydraProxyConfig extends Config {

	@ConverterClass(JsonConverter.class)
	Map<String, List<String>> mappings();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.json());
	}
}
