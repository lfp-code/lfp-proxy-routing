set -e

host=$1
s3_key=$2
s3_secret=$3

if [ -z "$host" ]
then
	echo "host is empty"
	exit 1
fi

if [ -z "$s3_key" ]
then
	echo "s3_key is empty"
	exit 1
fi

if [ -z "$s3_secret" ]
then
	echo "s3_secret is empty"
	exit 1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR


upload(){

	file=$1
	resource=$2
	content_type="application/octet-stream"
	date=`date -R`
	_signature="PUT\n\n${content_type}\n${date}\n${resource}"
	signature=`echo -en ${_signature} | openssl sha1 -hmac ${s3_secret} -binary | base64`

	curl -v -X PUT -T "${file}" \
			  -H "Host: $host" \
			  -H "Date: ${date}" \
			  -H "Content-Type: ${content_type}" \
			  -H "Authorization: AWS ${s3_key}:${signature}" \
			  https://$host${resource}

}

bucket="releases"
upload "native/run.sh" "/${bucket}/lfp-proxy-routing/tunnel/run.sh"

mvn clean package
mv target/app*-final-jar-with-dependencies.jar target/app.jar
upload "target/app.jar" "/${bucket}/lfp-proxy-routing/tunnel/app.jar"

