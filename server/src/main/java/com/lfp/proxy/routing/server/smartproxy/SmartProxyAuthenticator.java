package com.lfp.proxy.routing.server.smartproxy;

import java.io.IOException;
import java.time.Duration;
import java.util.Optional;
import java.util.function.Supplier;

import org.jsoup.helper.Validate;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.gson.JsonElement;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.server.smartproxy.config.SmartProxyConfig;

import at.favre.lib.bytes.Bytes;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public enum SmartProxyAuthenticator implements Supplier<String> {
	INSTANCE;

	private final LoadingCache<Optional<Void>, String> tokenCache = Caffeine.newBuilder().initialCapacity(1)
			.expireAfterWrite(Duration.ofMinutes(5)).build(nil -> loadTokenFresh());

	private String loadTokenFresh() throws IOException {
		SmartProxyConfig cfg = Configs.get(SmartProxyConfig.class);
		String headerVal = "Basic "
				+ Bytes.from(String.format("%s:%s", cfg.apiUsername(), cfg.apiPassword())).encodeBase64();
		OkHttpClient client = Ok.Clients.get();
		JsonElement je = Ok.Calls.execute(client,
				new Request.Builder().url("https://api.smartproxy.com/v1/auth").header(Headers.AUTHORIZATION, headerVal)
						.post(RequestBody.create(MediaType.parse("text/plain"), Utils.Bits.emptyByteArray())).build())
				.validateStatusCode(2).parseJson().getParsedBody();
		String token = Serials.Gsons.tryGetAsString(je, "token").orElse(null);
		Validate.isTrue(Utils.Strings.isNotBlank(token), "token not found");
		return token;
	}

	@Override
	public String get() {
		return tokenCache.get(Optional.empty());
	}

	public static void main(String[] args) {
		System.out.println(SmartProxyAuthenticator.INSTANCE.get());
	}
}
