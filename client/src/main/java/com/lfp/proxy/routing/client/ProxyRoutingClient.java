package com.lfp.proxy.routing.client;

import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.net.proxy.Proxy.Type;
import com.lfp.joe.serial.Serials;
import com.lfp.proxy.routing.service.ProxyAttributes;
import com.lfp.proxy.routing.service.ProxyExt;
import com.lfp.proxy.routing.service.RoutingService;
import com.lfp.rsocket.client.RSocketClients;
import com.neovisionaries.i18n.CountryCode;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class ProxyRoutingClient implements RoutingService {

	private static final LoadingCache<Optional<Void>, List<ProxyAttributes>> ATTRIBUTES_CACHE = Caffeine.newBuilder()
			.refreshAfterWrite(Duration.ofSeconds(15)).build(v -> {
				return Collections.unmodifiableList(
						RSocketClients.get(RoutingService.class).availableProxyAttributes().collectList().block());
			});

	public static ProxyRoutingClient get() {
		return Instances.get(ProxyRoutingClient.class, ProxyRoutingClient::new);
	}

	protected ProxyRoutingClient() {}

	@Override
	public Flux<ProxyAttributes> availableProxyAttributes() {
		return Flux.fromIterable(ATTRIBUTES_CACHE.get(Optional.empty()));
	}

	@Override
	public Mono<ProxyExt> lookupProxy(Type type, ProxyAttributes proxyAttributes) {
		return RSocketClients.get(RoutingService.class).lookupProxy(type, proxyAttributes).cache();
	}

	public ProxyExt lookupProxyBlocking() {
		return lookupProxyBlocking(null, null);
	}

	public ProxyExt lookupProxyBlocking(Proxy.Type type) {
		return lookupProxyBlocking(type, null);
	}

	public ProxyExt lookupProxyBlocking(ProxyAttributes proxyAttributes) {
		return lookupProxyBlocking(null, proxyAttributes);
	}

	public ProxyExt lookupProxyBlocking(Proxy.Type type, ProxyAttributes proxyAttributes) {
		return lookupProxy(type, proxyAttributes).block();
	}

	public static void main(String[] args) {
		var pab = ProxyAttributes.builder();
		pab.countryCode(CountryCode.US);
		for (int i = 0; i < 10; i++) {
			Proxy p = ProxyRoutingClient.get().lookupProxyBlocking(Proxy.Type.HTTP, pab.build());
			System.out.println(Serials.Gsons.getPretty().toJson(p));
		}
	}

}
