package com.lfp.proxy.routing.server.localproxies;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Pattern;

import org.apache.commons.lang3.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.cookie.CookieStoreLFP;
import com.lfp.joe.net.cookie.Cookies;
import com.lfp.joe.net.html.Jsoups;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.okhttp.cookie.CookieJars;
import com.lfp.joe.okhttp.interceptor.UserAgentInterceptor;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.lookup.LookupService;
import com.lfp.proxy.routing.lookup.ProxyRegistration;
import com.lfp.proxy.routing.lookup.ProxySupplier;
import com.lfp.proxy.routing.server.PublicIPAddressAppender;
import com.lfp.proxy.routing.server.localproxies.config.LocalProxiesConfig;
import com.lfp.proxy.routing.service.ProxyAttributes;
import com.lfp.proxy.routing.service.ProxyAttributes.RotationType;
import com.lfp.proxy.routing.service.ProxyAttributes.ServerType;
import com.neovisionaries.i18n.CountryCode;

import io.mikael.urlbuilder.UrlBuilder;
import okhttp3.OkHttpClient;
import one.util.streamex.StreamEx;

public enum LocalProxiesLookupService implements LookupService, LookupService.IPAuthorization {
	INSTANCE;

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	private final LoadingCache<Nada, Optional<LocalProxiesAccountInfo>> accountInfoCache = Caches
			.newCaffeineBuilder(-1, Duration.ofMinutes(1), null).build(nil -> {
				return Optional.ofNullable(lookupLocalProxiesAccountInfoFresh());
			});
	private final CookieStoreLFP cookieStore = Cookies.createCookieStore();

	@Override
	public void authorizeIPs(String... ipAddresses) throws IOException {
		var lookupLocalProxiesAccountInfo = lookupLocalProxiesAccountInfo();
		if (lookupLocalProxiesAccountInfo == null)
			return;
		try (var stream = Utils.Lots.stream(ipAddresses).nonNull().distinct()) {
			for (var ip : stream) {
				var uri = lookupLocalProxiesAccountInfo.getIpAddressAuthFunction().apply(ip);
				if (uri == null)
					continue;
				try (var rctx = Ok.Calls.execute(Ok.Clients.get(), uri).validateSuccess().parseJson()) {
					var je = rctx.getParsedBody();
					var resultStr = Serials.Gsons.tryGetAsString(je, "result").orElse(null);
					var result = Utils.Strings.equalsIgnoreCase(resultStr, Boolean.TRUE.toString());
					if (result)
						continue;
					var details = Serials.Gsons.tryGetAsString(je, "details").orElse(null);
					if (Utils.Strings.containsIgnoreCase(details, "Duplicate entry"))
						continue;
					throw new IOException(je.toString());
				}
			}
		}
	}

	@Override
	public StreamEx<ProxyRegistration> lookup() throws IOException {
		var accountInfo = lookupLocalProxiesAccountInfo();
		var proxyStream = Utils.Lots.stream(accountInfo == null ? null : accountInfo.getProxies());
		return proxyStream.map(v -> toProxyRegistration(v));
	}

	private LocalProxiesAccountInfo lookupLocalProxiesAccountInfo() {
		return accountInfoCache.get(Nada.get()).orElse(null);
	}

	private LocalProxiesAccountInfo lookupLocalProxiesAccountInfoFresh() throws IOException {
		var cfg = Configs.get(LocalProxiesConfig.class);
		if (Utils.Strings.isBlank(cfg.username()) || Utils.Strings.isBlank(cfg.password()))
			return LocalProxiesAccountInfo.builder().ipAddressAuthFunction(v -> null).build();
		OkHttpClient client = Ok.Clients.get().newBuilder().addInterceptor(UserAgentInterceptor.getBrowserDefault())
				.addInterceptor(new LocalProxiesLoginInterceptor(cfg.username(), cfg.password()))
				.cookieJar(CookieJars.create(cookieStore)).build();
		try (var rctx = Ok.Calls.execute(client, "https://amember.localproxies.com/proxy-ports").validateSuccess()
				.parseHtml()) {
			return parseLocalProxiesAccountInfo(rctx.getParsedBody());
		}
	}

	private ProxyRegistration toProxyRegistration(Proxy proxy) {
		var proxyAttributesB = ProxyAttributes.builder();
		proxyAttributesB.serverTypes(ServerType.RESIDENTIAL);
		proxyAttributesB.rotationType(RotationType.RANDOM);
		proxyAttributesB.countryCode(CountryCode.US);
		var proxyRegistrationB = ProxyRegistration.builder();
		proxyRegistrationB.lookupServiceType(this.getClass());
		proxyRegistrationB.proxyAttributes(proxyAttributesB.build());
		var proxyExt = PublicIPAddressAppender.INSTANCE.apply(proxy);
		proxyRegistrationB.proxySupplier(ProxySupplier.create(proxyExt));
		proxyRegistrationB.disableTesting(true);
		return proxyRegistrationB.build();
	}

	private static LocalProxiesAccountInfo parseLocalProxiesAccountInfo(Document doc) {
		List<Proxy> proxies;
		{
			var stream = Jsoups.select(doc, ".proxy_list_sample").map(Element::text);
			stream = stream.filter(v -> {
				return Pattern.compile("curl\\s").matcher(v).find();
			});
			stream = stream.filter(v -> {
				return Pattern.compile("--proxy\\s").matcher(v).find();
			});
			stream = stream.filter(v -> {
				return Pattern.compile("http:\\/\\/").matcher(v).find();
			});
			var uriStream = stream.map(v -> {
				var uri = URIs.extract(v).findFirst().orElse(null);
				return uri;
			});
			uriStream = uriStream.nonNull();
			uriStream = uriStream.distinct();
			proxies = uriStream.map(v -> Proxy.builder(v).build()).toList();
		}
		Function<String, URI> ipAddressAuthFunction;
		{
			String pathToken = "/add_ip_whitelist.php";
			var stream = Jsoups.select(doc, ".auth div").map(Element::text);
			stream = stream.filter(v -> Utils.Strings.containsIgnoreCase(v, pathToken));
			var uriStreams = stream.map(v -> {
				return URIs.extract(v).filter(vv -> Utils.Strings.containsIgnoreCase(vv.getPath(), pathToken));
			});
			var uriStream = Utils.Lots.flatMap(uriStreams);
			uriStream = uriStream.nonNull();
			uriStream = uriStream.distinct();
			var uris = uriStream.limit(2).toList();
			if (uris.isEmpty())
				return null;
			Validate.isTrue(uris.size() == 1);
			var urlb = UrlBuilder.fromUri(uris.get(0));
			ipAddressAuthFunction = ip -> {
				if (!IPs.isValidIpAddress(ip))
					return null;
				return urlb.addParameter("ip_address", ip).toUri();
			};
		}
		return LocalProxiesAccountInfo.builder().proxies(proxies).ipAddressAuthFunction(ipAddressAuthFunction).build();
	}

	public static void main(String[] args) throws IOException {
		Date quitAt = new Date(System.currentTimeMillis() + Duration.ofSeconds(20).toMillis());
		for (int i = 0; i < 20; i++) {
			if (i > 0)
				Threads.sleepUnchecked(Duration.ofSeconds(1));
			System.out.println(LocalProxiesLookupService.INSTANCE.lookup().count());
		}
	}

}
