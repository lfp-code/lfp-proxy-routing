package test;

import java.util.List;

import com.lfp.joe.location.Location;
import com.lfp.joe.net.http.ServiceConfig;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.proxy.OkHttpClientProxyTester;
import com.lfp.joe.serial.Serials;
import com.lfp.proxy.routing.service.ProxyAttributes;
import com.lfp.proxy.routing.service.RoutingService;
import com.neovisionaries.i18n.CountryCode;

public class ClientTest {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	@SuppressWarnings("unused")
	public static void main(String[] args) throws InterruptedException {
		ServiceConfig cc = false ? RoutingService.clientConfig() : null;
		RoutingService client = null;
		if (false)
			while (true) {
				try {
					List<ProxyAttributes> locs = client.availableProxyAttributes().collectList().block();
					System.out.println(Serials.Gsons.get().toJson(locs));
				} catch (Exception e) {
					logger.warn("error during lookup:" + e.getMessage());
				}
				Thread.sleep(1_000);
			}
		if (true)
			while (true) {
				var pa = ProxyAttributes.builder();
				pa.countryCode(CountryCode.US);
				Proxy proxy = client.lookupProxy(Proxy.Type.SOCKS_5, pa.build()).block();
				System.out.println(Serials.Gsons.get().toJson(proxy));
				try {
					OkHttpClientProxyTester.getDefault().validate(proxy);
					System.out.println("passed");
				} catch (Exception e) {
					logger.error("failed:" + proxy.getHostname() + " - " + e.getMessage(), e);
				}
				// Thread.sleep(500);
			}
	}

}
