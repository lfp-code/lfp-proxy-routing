package com.lfp.proxy.routing.server.hydraproxy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.lookup.LookupService;
import com.lfp.proxy.routing.lookup.ProxyRegistration;
import com.lfp.proxy.routing.lookup.ProxySupplier;
import com.lfp.proxy.routing.server.hydraproxy.config.HydraProxyConfig;
import com.lfp.proxy.routing.service.ProxyAttributes;
import com.lfp.proxy.routing.service.ProxyAttributes.RotationType;
import com.lfp.proxy.routing.service.ProxyAttributes.ServerType;
import com.lfp.proxy.routing.service.ProxyExt;
import com.neovisionaries.i18n.CountryCode;

import one.util.streamex.StreamEx;

public enum HydraProxyLookupService implements LookupService {
	INSTANCE;

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	@Override
	public StreamEx<ProxyRegistration> lookup() throws IOException {
		var cfg = Configs.get(HydraProxyConfig.class);
		var mappings = cfg.mappings();
		if (mappings == null)
			return StreamEx.empty();
		var listStream = Utils.Lots.stream(mappings).map(ent -> {
			return toProxyRegistrations(ent.getKey(), ent.getValue());
		});
		return Utils.Lots.flatMapIterables(listStream).nonNull().distinct();
	}

	private List<ProxyRegistration> toProxyRegistrations(String tunnelIPAddress, List<String> proxyHostPorts) {
		if (!IPs.isValidIpAddress(tunnelIPAddress))
			return null;
		var uriStream = Utils.Lots.stream(proxyHostPorts).filter(Utils.Strings::isNotBlank).map(v -> {
			if (!Utils.Strings.containsIgnoreCase(v, "://"))
				v = "http://" + v;
			return URIs.parse(v).orElse(null);
		});
		uriStream = uriStream.nonNull();
		var proxies = uriStream.map(v -> Proxy.builder(v).build()).toList();
		List<ProxyRegistration> result = new ArrayList<>();
		for (var proxy : proxies) {
			var proxyAttributesB = ProxyAttributes.builder();
			proxyAttributesB.serverTypes(ServerType.MOBILE);
			proxyAttributesB.rotationType(RotationType.RANDOM);
			proxyAttributesB.countryCode(CountryCode.US);
			var prb = ProxyRegistration.builder();
			prb.lookupServiceType(this.getClass());
			prb.disableTesting(true);
			prb.proxyAttributes(proxyAttributesB.build());
			prb.proxySupplier(ProxySupplier.create(ProxyExt.build(proxy, null)));
			prb.restrictTunnelIPAddress(tunnelIPAddress);
			prb.lookupPublicIPAddressWithTunnel(true);
			result.add(prb.build());
		}
		return result;

	}

	public static void main(String[] args) throws IOException {
		System.out.println(Serials.Gsons.getPretty().toJson(HydraProxyLookupService.INSTANCE.lookup().toList()));
	}

}
