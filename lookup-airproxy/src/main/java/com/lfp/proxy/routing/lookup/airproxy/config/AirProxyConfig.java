package com.lfp.proxy.routing.lookup.airproxy.config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;

import org.aeonbits.owner.Config;

public interface AirProxyConfig extends Config {

	String key();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.json());
	}
}
