package com.lfp.proxy.routing.lookup.proxidize.api;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.proxy.routing.lookup.proxidize.config.ProxidizeConfig;

import io.mikael.urlbuilder.UrlBuilder;
import okhttp3.Request;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public enum APIFunction {
	INSTANCE;

	public <X> X get(Iterable<String> pathParts, Map<String, ? extends Iterable<? extends Object>> queryParams,
			ThrowingFunction<InputStream, X, IOException> mapper) throws IOException {
		Objects.requireNonNull(mapper);
		var request = createApiRequest(pathParts, queryParams);
		try (var rctx = Ok.Calls.execute(Ok.Clients.get(), request).validateStatusCode(2)) {
			return mapper.apply(rctx.getResponse().body().byteStream());
		}
	}

	private static Request createApiRequest(Iterable<String> pathParts,
			Map<String, ? extends Iterable<? extends Object>> queryParams) {
		var cfg = Configs.get(ProxidizeConfig.class);
		URI uri;
		{
			var urlb = UrlBuilder.fromUri(Objects.requireNonNull(cfg.uri()));
			var path = URIs.normalizePath(urlb.path);
			var pathAppendJoined = Utils.Lots.stream(pathParts).filter(Utils.Strings::isNotBlank).joining("/");
			if (!pathAppendJoined.isEmpty()) {
				if (!Utils.Strings.endsWithIgnoreCase(path, "/"))
					path += "/";
				path += pathAppendJoined;
			}
			urlb = urlb.withPath(path);
			if (queryParams != null) {
				for (var ent : queryParams.entrySet()) {
					var name = Optional.ofNullable(ent).map(Entry::getKey).orElse(null);
					if (name == null)
						continue;
					StreamEx<String> valueStream = Utils.Lots.stream(ent.getValue())
							.map(vv -> vv == null ? null : vv.toString());
					for (var value : valueStream)
						urlb = urlb.addParameter(name, value);
				}

			}
			uri = urlb.toUri();
		}
		var apiToken = Validate.notBlank(cfg.apiToken());
		var rb = new Request.Builder();
		rb = rb.url(uri.toString());
		rb = rb.header(Headers.AUTHORIZATION, String.format("Token %s", apiToken));
		return rb.build();
	}
}
