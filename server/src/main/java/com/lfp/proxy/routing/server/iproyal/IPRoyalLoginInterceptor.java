package com.lfp.proxy.routing.server.iproyal;
import java.io.OutputStream;

import java.io.IOException;
import java.net.URI;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.html.Jsoups;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.okhttp.cookie.CookieJars;
import com.lfp.joe.okhttp.interceptor.BrowserHeaderInterceptor;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.server.iproyal.config.IPRoyalConfig;

import org.jsoup.nodes.Document;

import io.mikael.urlbuilder.UrlBuilder;
import okhttp3.Interceptor;
import okhttp3.Response;

public class IPRoyalLoginInterceptor implements Interceptor {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	@Override
	public Response intercept(Chain chain) throws IOException {
		var request = chain.request();
		var response = chain.proceed(request);
		if (!isLoginResponse(response))
			return response;
		var client = Ok.Clients.getClient(chain.call()).orElse(null);
		var cookieStore = Ok.Clients.getCookieStore(client).orElse(null);
		if (cookieStore == null)
			return response;
		return login(chain, response);
	}

	private Response login(Chain chain, Response initialResponse) throws IOException {
		URI requestURI;
		Document doc;
		try (initialResponse; var is = initialResponse.body().byteStream()) {
			requestURI = initialResponse.request().url().uri();
			var headers = initialResponse.headers();
			doc = Jsoups.parse(is, headers == null ? null : headers.toMultimap(), requestURI);
		}
		var form = Jsoups.select(doc, "form [name=\"password\"]").map(v -> {
			while (v != null) {
				if ("form".equalsIgnoreCase(v.tagName()))
					return v;
				v = v.parent();
			}
			return null;
		}).nonNull().findFirst().get();
		var formData = Jsoups.parseFormData(form);
		var cfg = Configs.get(IPRoyalConfig.class);
		formData.set("email", cfg.email());
		formData.set("password", cfg.password());
		var rb = Ok.Calls.toRequest(formData).newBuilder();
		{
			var requestUrlb = UrlBuilder.fromUri(requestURI).withPath(URIs.normalizePath(requestURI))
					.withQuery((String) null);
			rb.header(Headers.ORIGIN, Utils.Strings.stripEnd(requestUrlb.withPath("/").toString(), "/"));
			rb.header(Headers.REFERER, Utils.Strings.stripEnd(requestUrlb.toString(), "/"));
		}
		var origRequest = Utils.Lots.last(Ok.Calls.streamRequests(initialResponse)).get();
		{// login and check if redirected
			var resp = chain.proceed(rb.build());
			if (!StatusCodes.isSuccess(resp.code(), 2)) {
				resp.close();
				throw Ok.Calls.createException(resp, "login failed - invalid status code");
			}
			if (resp.request().url().equals(origRequest.url()))
				return resp;
			try (resp) {
				Utils.Bits.copy(resp.body().byteStream(), OutputStream.nullOutputStream());
			}
		}
		// force re navigate
		var resp = chain.proceed(origRequest);
		if (!isLoginResponse(resp)) {
			resp.close();
			throw Ok.Calls.createException(resp, "login failed - login form presented");
		}
		return resp;
	}

	private boolean isLoginResponse(Response response) {
		if (!isLoginURI(response.request().url().uri()))
			return false;
		var origRequest = Utils.Lots.last(Ok.Calls.streamRequests(response)).get();
		if (isLoginURI(origRequest.url().uri()))
			return false;
		return true;
	}

	private boolean isLoginURI(URI uri) {
		if (uri == null)
			return false;
		if (!"dashboard.iproyal.com".equalsIgnoreCase(uri.getHost()))
			return false;
		if (!URIs.pathPrefixMatch(uri.getPath(), "/login"))
			return false;
		return true;
	}

	public static void main(String[] args) throws IOException {
		var proxy = Proxy.builder(URI.create("http://localhost:8888")).build();
		var cookieFile = Utils.Files.tempFile(THIS_CLASS, "v0");
		var client = Ok.Clients.get(proxy).newBuilder().cookieJar(CookieJars.create(cookieFile))
				.addInterceptor(BrowserHeaderInterceptor.getBrowserDefault())
				.addInterceptor(new IPRoyalLoginInterceptor()).build();
		try (var rctx = Ok.Calls.execute(client, "https://dashboard.iproyal.com/products/4g-mobile-proxies")
				.validateSuccess().parseHtml()) {
			System.out.println(rctx.getParsedBody());
		}
	}
}