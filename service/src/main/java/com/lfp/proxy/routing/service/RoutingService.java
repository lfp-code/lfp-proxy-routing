package com.lfp.proxy.routing.service;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.proxy.routing.service.config.ProxyRoutingServiceConfig;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface RoutingService {

	static ProxyRoutingServiceConfig clientConfig() {
		return Configs.get(ProxyRoutingServiceConfig.class);
	}

	default Mono<ProxyExt> lookupProxy() {
		return lookupProxy(null, null);
	}

	default Mono<ProxyExt> lookupProxy(ProxyAttributes proxyAttributes) {
		return lookupProxy(null, proxyAttributes);
	}

	default Mono<ProxyExt> lookupProxy(Proxy.Type type) {
		return lookupProxy(type, null);
	}

	Mono<ProxyExt> lookupProxy(Proxy.Type type, ProxyAttributes proxyAttributes);

	Flux<ProxyAttributes> availableProxyAttributes();

}
