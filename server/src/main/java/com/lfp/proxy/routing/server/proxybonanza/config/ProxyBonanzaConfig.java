package com.lfp.proxy.routing.server.proxybonanza.config;

import java.util.List;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;

public interface ProxyBonanzaConfig extends Config {

	public String apiKey();

	public List<Long> disabledPackages();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.builder().withPrependClassNames(false).withJsonOutput(true).build());
	}
}
