package com.lfp.proxy.routing.service.config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.rsocket.service.RsocketServiceConfig;

public interface ProxyRoutingServiceConfig extends RsocketServiceConfig {

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.builder().withPrependClassNames(true).withJsonOutput(false)
				.withSkipPopulated(false).build());
	}
}
