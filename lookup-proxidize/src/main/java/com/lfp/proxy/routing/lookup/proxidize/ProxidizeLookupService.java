package com.lfp.proxy.routing.lookup.proxidize;

import java.io.IOException;
import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.location.Location;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.lookup.LookupService;
import com.lfp.proxy.routing.lookup.ProxyRegistration;
import com.lfp.proxy.routing.lookup.ProxySupplier;
import com.lfp.proxy.routing.lookup.proxidize.api.GetInfoResult;
import com.lfp.proxy.routing.lookup.proxidize.api.GetInfoResultFunction;
import com.lfp.proxy.routing.lookup.proxidize.config.ProxidizeConfig;
import com.lfp.proxy.routing.lookup.proxidize.rotation.IPRotationService;
import com.lfp.proxy.routing.service.ProxyAttributes;
import com.lfp.proxy.routing.service.ProxyAttributes.RotationType;
import com.lfp.proxy.routing.service.ProxyAttributes.ServerType;
import com.lfp.proxy.routing.service.ProxyExt;
import com.neovisionaries.i18n.CountryCode;

import one.util.streamex.StreamEx;

public enum ProxidizeLookupService implements LookupService {
	INSTANCE;

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private final boolean IP_ROTATATION_SERVICE_DISABLED_FORCE = MachineConfig.isDeveloper() && false;
	protected final Optional<IPRotationService> ipRotationService;

	private ProxidizeLookupService() {
		var cfg = Configs.get(ProxidizeConfig.class);
		var ipRotationServiceType = cfg.ipRotationServiceType();
		if (ipRotationServiceType != null && !IP_ROTATATION_SERVICE_DISABLED_FORCE)
			ipRotationService = Optional.of(Utils.Types.newInstanceUnchecked(ipRotationServiceType));
		else
			ipRotationService = Optional.empty();
	}

	@Override
	public StreamEx<ProxyRegistration> lookup() throws IOException {
		var proxyRegistrationStream = GetInfoResultFunction.INSTANCE.get().map(v -> toProxyRegistrations(v))
				.chain(Streams.flatMap());
		proxyRegistrationStream = proxyRegistrationStream.nonNull();
		return proxyRegistrationStream;
	}

	private StreamEx<ProxyRegistration> toProxyRegistrations(GetInfoResult proxyResult) {
		StreamEx<ProxyRegistration> stream = StreamEx.empty();
		try (var estream = proxyResult.streamProxies()) {
			for (var proxy : estream)
				stream = stream.append(toProxyRegistration(proxyResult, proxy));
		}
		stream = stream.nonNull();
		return stream;
	}

	private ProxyRegistration toProxyRegistration(GetInfoResult proxyResult, Proxy proxy) {
		var cfg = Configs.get(ProxidizeConfig.class);
		Location location;
		{
			location = cfg.location();
			if (location == null)
				location = Location.builder(CountryCode.US).build();
		}
		ProxyAttributes proxyAttributes;
		{
			var blder = ProxyAttributes.builder(location);
			blder.serverTypes(ServerType.MOBILE);
			blder.rotationType(RotationType.RANDOM);
			proxyAttributes = blder.build();
		}
		ProxyRegistration proxyRegistration;
		{
			var blder = ProxyRegistration.builder();
			blder.lookupServiceType(this.getClass());
			blder.proxyAttributes(proxyAttributes);
			blder.proxySupplier(new ProxySupplier(proxy.hash()) {

				@Override
				public ProxyExt get() {
					return ProxyExt.build(proxy, proxyResult.getPublicIp());
				}

			});
			proxyRegistration = blder.build();
		}
		return proxyRegistration;
	}

	public static void main(String[] args) throws IOException {
		for (int i = 0;; i++) {
			if (i > 0)
				Threads.sleepUnchecked(Duration.ofSeconds(5));
			var count = new AtomicLong();
			ProxidizeLookupService.INSTANCE.lookup().forEach(v -> {
				var proxy = v.getProxy();
				System.out.println(Serials.Gsons.getPretty().toJson(proxy));
				System.out.println(proxy.getPublicIPAddress());
				count.incrementAndGet();
			});
			System.err.println(count.get());
		}
	}

}
