package com.lfp.proxy.routing.impl;

import java.io.IOException;
import java.security.PrivateKey;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.core.function.FNV;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Base58;
import com.lfp.proxy.routing.impl.config.ProxyRoutingImplConfig;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JOSEObject;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.crypto.RSADecrypter;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.jwt.SignedJWT;

import at.favre.lib.bytes.Bytes;

public class ProxyDecoder extends ProxyCodec.Abs<Iterable<String>, Proxy> {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 0;

	public ProxyDecoder(PrivateKey tunnelPrivateKey) {
		this(new RSAKey.Builder(Configs.get(ProxyRoutingImplConfig.class).tunnelPublicKey())
				.privateKey(Objects.requireNonNull(tunnelPrivateKey))
				.build(), new RSAKey.Builder(Configs.get(ProxyRoutingImplConfig.class).serverPublicKey()).build());
	}

	public ProxyDecoder(RSAKey tunnelKey, RSAKey serverPublicKey) {
		super(tunnelKey, serverPublicKey);
	}

	public Proxy apply(Proxy proxy) throws IOException {
		return apply(proxy == null ? null
				: Arrays.asList(proxy.getUsername().orElse(null), proxy.getPassword().orElse(null)));
	}

	public Proxy apply(String... tokenParts) throws IOException {
		return apply(Utils.Lots.stream(tokenParts));
	}

	@Override
	public Proxy applyInternal(Iterable<String> tokenParts) throws IOException {
		var tokenBytes = toTokenBytes(tokenParts);
		if (tokenBytes.isEmpty())
			return null;
		try {
			return decode(tokenBytes);
		} catch (ParseException | JOSEException e) {
			throw Utils.Exceptions.as(e, IOException.class);
		}
	}

	@Override
	protected Long getCacheKey(Iterable<String> input) {
		var tokenBytes = toTokenBytes(input);
		if (tokenBytes.isEmpty())
			return null;
		var bytesStream = Utils.Lots.stream(tokenBytes);
		bytesStream = bytesStream.append(Utils.Bits.from(THIS_CLASS.getName()));
		bytesStream = bytesStream.append(Utils.Bits.from(VERSION));
		return FNV.hash64(bytesStream.map(v -> v.array()));
	}

	private Proxy decode(List<Bytes> tokenBytes) throws IOException, ParseException, JOSEException {
		var proxy = fromJWE(tokenBytes);
		if (proxy != null)
			return proxy;
		proxy = fromSSDB(tokenBytes);
		if (proxy != null)
			return proxy;
		return null;
	}

	private Proxy fromSSDB(List<Bytes> tokenBytes) throws JOSEException {
		if (tokenBytes.size() != 3)
			return null;
		var signatureBytes = tokenBytes.get(0);
		var expiresAtBytes = tokenBytes.get(1);
		var cacheKeyBytes = tokenBytes.get(2);
		var verified = Utils.Crypto.verifySignature(getSignature(), this.getPublicKey().toPublicKey(), signatureBytes,
				expiresAtBytes.append(cacheKeyBytes));
		if (!verified)
			return null;
		Date expiresAt = CompactDateCodec.INSTANCE.decodeDate(expiresAtBytes);
		if (expiresAt != null && expiresAt.before(new Date()))
			return null;
		var proxy = getSSDBClient().get(cacheKeyBytes.encodeHex());
		return proxy;
	}

	private Proxy fromJWE(Iterable<Bytes> tokenBytes) throws JOSEException, ParseException {
		var jweObject = tryParseJWEObject(tokenBytes).orElse(null);
		if (jweObject == null)
			return null;
		// Decrypt with private key
		jweObject.decrypt(new RSADecrypter(this.getSigningKey()));
		// Extract payload
		SignedJWT signedJWT = jweObject.getPayload().toSignedJWT();
		Objects.requireNonNull(signedJWT, "payload not a signed JWT");
		// Check the signature
		Validate.isTrue(signedJWT.verify(new RSASSAVerifier(this.getPublicKey())));
		var expirationTime = signedJWT.getJWTClaimsSet().getExpirationTime();
		Validate.isTrue(expirationTime == null || expirationTime.after(new Date()), "expired JWT");
		var map = signedJWT.getJWTClaimsSet().toJSONObject();
		var proxy = Serials.Gsons.get().fromJson(Serials.Gsons.get().toJson(map), Proxy.class);
		return Objects.requireNonNull(proxy);
	}

	private static List<Bytes> toTokenBytes(Iterable<String> tokenParts) {
		var token = Utils.Lots.stream(tokenParts).filter(Utils.Strings::isNotBlank).joining();
		if (Utils.Strings.isBlank(token))
			return List.of();
		var tokenBytes = new ArrayList<Bytes>();
		for (var part : Utils.Strings.split(token, '-')) {
			if (Utils.Strings.isBlank(part))
				continue;
			var base58Decoded = Utils.Functions.catching(() -> Base58.decodeToBytes(part), t -> null);
			if (base58Decoded == null)
				return List.of();
			if (base58Decoded.length() == 0)
				continue;
			tokenBytes.add(base58Decoded);
		}
		return List.copyOf(tokenBytes);
	}

	private static Optional<JWEObject> tryParseJWEObject(Iterable<Bytes> tokenBytes) {
		StringBuilder jweSb = new StringBuilder();
		for (var bytes : tokenBytes) {
			var base64Encoded = Utils.Functions.catching(() -> bytes.encodeBase64(true, false), t -> null);
			if (Utils.Strings.isBlank(base64Encoded))
				return Optional.empty();
			if (jweSb.length() > 0)
				jweSb.append(".");
			jweSb.append(base64Encoded);
		}
		var result = Utils.Functions.catching(() -> tryParseJWEObjectBase64(jweSb.toString()), t -> null);
		return Optional.ofNullable(result);
	}

	private static JWEObject tryParseJWEObjectBase64(String jweString) throws ParseException {
		if (Utils.Strings.isBlank(jweString))
			return null;
		var dotCount = 0;
		for (int i = 0; dotCount < 4 && i < jweString.length(); i++) {
			if (Objects.equals('.', jweString.charAt(i)))
				dotCount++;
		}
		// We must have 2 (JWS) or 4 dots (JWE)
		if (dotCount != 2 && dotCount != 4)
			return null;
		Base64URL[] parts = JOSEObject.split(jweString);
		if (parts.length != 5)
			return null;
		return new JWEObject(parts[0], parts[1], parts[2], parts[3], parts[4]);
	}

}
