package com.lfp.proxy.routing.lookup.proxidize.api;

import java.net.InetSocketAddress;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.lookup.proxidize.config.ProxidizeConfig;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

@Generated("jsonschema2pojo")
public class GetInfoResult {

	@SerializedName("ID")
	@Expose
	private Long id;
	@SerializedName("Index")
	@Expose
	private Integer index;
	@SerializedName("Model")
	@Expose
	private String model;
	@SerializedName("Imei")
	@Expose
	private String imei;
	@SerializedName("Public_ip")
	@Expose
	private String publicIp;
	@SerializedName("Phone_number")
	@Expose
	private String phoneNumber;
	@SerializedName("Carrier")
	@Expose
	private String carrier;
	@SerializedName("USB_ID")
	@Expose
	private String usbId;
	@SerializedName("Dev_name")
	@Expose
	private String devName;
	@SerializedName("Internal_ip")
	@Expose
	private String internalIp;
	@SerializedName("Gateway")
	@Expose
	private String gateway;
	@SerializedName("Proxy_ip")
	@Expose
	private String proxyIp;
	@SerializedName("Driver")
	@Expose
	private String driver;
	@SerializedName("TTY")
	@Expose
	private String tty;
	@SerializedName("Time_added")
	@Expose
	private String timeAdded;
	@SerializedName("rotation_interval")
	@Expose
	private String rotationInterval;
	@SerializedName("Port")
	@Expose
	private Long port;
	@SerializedName("Proxy")
	@Expose
	private String proxy;
	@SerializedName("HTTP")
	@Expose
	private String http;
	@SerializedName("SOCKS")
	@Expose
	private String socks;
	@SerializedName("Download")
	@Expose
	private String download;
	@SerializedName("Upload")
	@Expose
	private String upload;
	@SerializedName("Workmode")
	@Expose
	private String workmode;
	@SerializedName("Status")
	@Expose
	private String status;

	private boolean disableHostModification;

	public EntryStream<Proxy.Type, InetSocketAddress> streamAddresses() {
		EntryStream<Proxy.Type, String> typeToAddressString = EntryStream.of(Proxy.Type.HTTP, this.getProxy(),
				Proxy.Type.HTTP, this.getHttp(), Proxy.Type.SOCKS_5, this.getSocks());
		EntryStream<Proxy.Type, InetSocketAddress> typeToAddress = typeToAddressString.mapValues(v -> {
			int index = Utils.Strings.indexOfIgnoreCase(v, ":");
			if (index < 0)
				return null;
			var hostname = v.substring(0, index);
			if (Utils.Strings.isBlank(hostname))
				return null;
			var port = Utils.Strings.parseNumber(v.substring(index + 1)).map(Number::intValue).orElse(null);
			if (port == null)
				return null;
			return InetSocketAddress.createUnresolved(hostname, port);
		});
		typeToAddress = typeToAddress.nonNullValues();
		typeToAddress = typeToAddress.distinct();
		return typeToAddress;
	}

	public StreamEx<Proxy> streamProxies(Proxy.Type... typeFilters) {
		var typeFilterSet = Utils.Lots.stream(typeFilters).nonNull().toSet();
		var stream = streamAddresses().map(ent -> {
			var type = ent.getKey();
			if (!typeFilterSet.isEmpty() && !typeFilterSet.contains(type))
				return null;
			var inetSocketAddress = ent.getValue();
			var cfg = Configs.get(ProxidizeConfig.class);
			Proxy proxy;
			{
				var blder = Proxy.builder();
				blder.type(type);
				if (this.isDisableHostModification())
					blder.hostname(inetSocketAddress.getHostString());
				else
					blder.hostname(cfg.uri().getHost());
				blder.port(inetSocketAddress.getPort());
				blder.username(cfg.proxyUsername());
				blder.password(cfg.proxyPassword());
				proxy = blder.build();
			}
			return proxy;
		});
		stream = stream.nonNull();
		return stream;

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getPublicIp() {
		return publicIp;
	}

	public void setPublicIp(String publicIp) {
		this.publicIp = publicIp;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getUsbId() {
		return usbId;
	}

	public void setUsbId(String usbId) {
		this.usbId = usbId;
	}

	public String getDevName() {
		return devName;
	}

	public void setDevName(String devName) {
		this.devName = devName;
	}

	public String getInternalIp() {
		return internalIp;
	}

	public void setInternalIp(String internalIp) {
		this.internalIp = internalIp;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getProxyIp() {
		return proxyIp;
	}

	public void setProxyIp(String proxyIp) {
		this.proxyIp = proxyIp;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getTty() {
		return tty;
	}

	public void setTty(String tty) {
		this.tty = tty;
	}

	public String getTimeAdded() {
		return timeAdded;
	}

	public void setTimeAdded(String timeAdded) {
		this.timeAdded = timeAdded;
	}

	public String getRotationInterval() {
		return rotationInterval;
	}

	public void setRotationInterval(String rotationInterval) {
		this.rotationInterval = rotationInterval;
	}

	public Long getPort() {
		return port;
	}

	public void setPort(Long port) {
		this.port = port;
	}

	public String getHttp() {
		return http;
	}

	public void setHttp(String http) {
		this.http = http;
	}

	public String getSocks() {
		return socks;
	}

	public void setSocks(String socks) {
		this.socks = socks;
	}

	public String getDownload() {
		return download;
	}

	public void setDownload(String download) {
		this.download = download;
	}

	public String getUpload() {
		return upload;
	}

	public void setUpload(String upload) {
		this.upload = upload;
	}

	public String getWorkmode() {
		return workmode;
	}

	public void setWorkmode(String workmode) {
		this.workmode = workmode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProxy() {
		return proxy;
	}

	public void setProxy(String proxy) {
		this.proxy = proxy;
	}

	public boolean isDisableHostModification() {
		return disableHostModification;
	}

	public void setDisableHostModification(boolean disableHostModification) {
		this.disableHostModification = disableHostModification;
	}

}