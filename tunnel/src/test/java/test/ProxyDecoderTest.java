package test;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;

import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.client.ProxyRoutingClient;
import com.lfp.proxy.routing.tunnel.TunnelProxyDecoder;

public class ProxyDecoderTest {

	public static void main(String[] args) throws IOException {
		String json = "{\n" + "  \"publicIPAddress\": \"194.53.143.0\",\n" + "  \"type\": \"HTTP\",\n"
				+ "  \"hostname\": \"e47xbkcj5ooow5ox.104.248.54.162.nip.io\",\n" + "  \"port\": 30000,\n"
				+ "  \"username\": \"2Wzkq8uKQteyqq8Z7CWa1j4CE8sXJ25ejdbP7na3nfuXiJny8soA7LgtWT2X4pphCWHDE-68j6kZmRN353UUqWvRmr1fGb8kAeos1hfsyTL8jrBNnP8wRkmmGjH4DDz8uEiC1EbXP3SbXg7GYshUwvHHr8Vxep98ZsZVmcvU9ipvVw6BHYj96P6rpQbvrcWQfMzUMJHpqQYCg2rpNS6h2MsUusVGn6XhdKM7ZNhPiQ3LU6xiMByV54qWpz7VRJhuN1D2MuTuG3ZjdMBFjaCnGgnWeYVXM19Uq4aaMr6XZqiHSBG9sfyqFFYb4VKhF72Jct4jDDeAo3qy55EoEZcwKZidtXtCyQBWaQjAvEP3QTVaLr9akno9hqCU4WVNde5QAdgpF1KZEgMAPxWWxc7P52eiyof83tLkUyVn-2RJStRATGU7sNX3NF-2GCc2UzMWDeDs1G8egKMFS5oCY4x9Nx7Drs7u4CvhhctA9M8ccGScWkt8FPXkQbKR8BAcBwk2fvSZWsdNMDadx8D3xtzKWuUX6wQ1h4xWnR1g27wDc3vm7NYF4XSyZM5NWjqSgSqymuWo2Xyp4w8fbRH3YzC1GYPq6hD1NeV\",\n"
				+ "  \"password\": \"MVbrM6igxNmnfJcivhdTtDBhmRGRXxHJhfUfNNzBcbRdokHHbfbWjmSvyBzARWq7shjqUvNK14uM8biuEeWtHPcqHSGEfABu2baAeeG3Vno6v7D3BcJCTD6ZL4BRaUDkMdBbgp6Bu2BmWsY5VPQW8mXy2Co6bb3FduUvEdgfs8eHLbMifvzEkhLeA4QqpK7TnteCNWhz9WqbzdVM6qCaW3YFzjjL2YG3UYUcc8Fg4gCAYidb1BfUfyufUMjMKVbH4mVtPiMQt8LkMu4RskPWipQCEDfoB3ELi7iAAKRwtvZMgduAT6LddXig5wAHV2DChhumrHk2Pj8xfwVFZyY5G3zkqWNo2Tc4F6WvAAdrAK6eR8qNUFzbj6tEkVyS86JuyN3jFoB52vJw1xDEqM626svq9wQnuz4nPUwTP3P7mVNEnkfbRwMuUioxQEVLLESKXFg1oy7poXyxQQbxehGRcaYg71hbgdpisdG26Kap8P5y64kfoKrGUpH3GRuqUXNLzHZ8woYYSXqByCQeaXrYTLMahsKvqbEyoNe8qWSB2jG38nk3PWeo7MoJbDMbyCQfYSZtGJQV-GDa967TLjxLzjBnjWLs72o\"\n"
				+ "}\n" + "";
		// json = "";
		for (int i = 0; i < 10; i++) {
			Proxy proxy;
			if (Utils.Strings.isBlank(json))
				proxy = ProxyRoutingClient.get().lookupProxyBlocking(Proxy.Type.SOCKS_5);
			else
				proxy = Serials.Gsons.get().fromJson(json, Proxy.class);
			var proxyDecrypted = TunnelProxyDecoder.INSTANCE
					.apply(Arrays.asList(proxy.getUsername().orElse(null), proxy.getPassword().orElse(null)));
			System.out.println(Serials.Gsons.getPretty().toJson(Utils.Lots.entry(proxy, proxyDecrypted)));
			for (var testProxy : Arrays.asList(proxyDecrypted, proxy)) {
				try (var rctx = Ok.Calls.execute(Ok.Clients.get(testProxy), URI.create("http://api.ipify.org/"))) {
					System.out.println(rctx.parseText().getParsedBody());
				}
				try (var rctx = Ok.Calls.execute(Ok.Clients.get(testProxy), URI.create("https://api.ipify.org/"))) {
					System.out.println(rctx.parseText().getParsedBody());
				}
			}
		}
	}

}
